AMDiS
=====
The *Adaptive Multi-Dimensional Simulation Toolbox* (AMDiS) is implemented as a
discretization module on top of the Dune framework.

Documentation
=============
See the online documentation [amdis.readthedocs.io](https://amdis.readthedocs.io) that is
generated from the Markdown files in the `docs/` directory.

Mirror
======
This repositories is mirrored to https://gitlab.mn.tu-dresden.de/amdis/amdis-core.

Installation
============
A detailed explanation how to get started with AMDiS and how to install all dependencies and
the library is given in [docs/getting-started.md](docs/getting-started.md). Here a simplified
installation instruction is provided for users already familiar with Dune.

We provide a *cmake*-based configuration and use the `dunecontrol` build system of the Dune
framework. Simply run

```
dunecontrol --current all
```

The `dunecontrol` script searches for the required
(and suggested) dune modules this library depends on. These include:
- [dune-common](https://gitlab.dune-project.org/core/dune-common)
- [dune-geometry](https://gitlab.dune-project.org/core/dune-geometry)
- [dune-grid](https://gitlab.dune-project.org/core/dune-grid)
- [dune-localfunctions](https://gitlab.dune-project.org/core/dune-localfunctions)
- [dune-istl](https://gitlab.dune-project.org/core/dune-istl)
- [dune-typetree](https://gitlab.dune-project.org/staging/dune-typetree)
- [dune-functions](https://gitlab.dune-project.org/staging/dune-functions)

(See the file `dune.module` for an up-to-date list of dependencies). The dune modules
can be obtained from https://gitlab.dune-project.org and need to be found in a
subdirectory of `DUNE_CONTROL_PATH`. See also https://dune-project.org/installation/installation-buildsrc/
for details about the installation of dune modules. You can use the script

```
bin/install_all_dune_modules.sh --prefix DOWNLOAD_DIR
```

to clone all dune repositories into `DOWNLOAD_DIR` and provide a simple way using `dunecontrol`
to install all of them at once.

Additionally the following optional libraries can be used:
- [MTL4](https://gitlab.math.tu-dresden.de/spraetor/mtl4) (use this fork to get up-to-date changes)
- [Eigen3](http://eigen.tuxfamily.org) >= 3.3
- [SuiteSparse](http://faculty.cse.tamu.edu/davis/suitesparse.html)
- [PETSc](https://petsc.org)
- libalberta >= 3.0 (For Alberta-Grids)

And a compiler that supports the C++17 standard, e.g. g++ >= 9.0 and clang >= 10.0, and cmake >= 3.16.

By default, the `dune-istl` linear-algebra backend is used. To choose one of `ISTL`, `MTL`,
`PETSC`, or `EIGEN`, you can specify the cmake parameter `-DBACKEND=[ISTL,MTL,PETSC,EIGEN]`.

If, for example, your MTL4 installation is not found by default, you have to specify the path,
where the file `MTLConfig.cmake` is found, here called `MTL_ROOT`. Then simply use
`dunecontrol` to configure and `cmake` to build:

```
CMAKE_FLAGS="-DBACKEND=MTL -DMTL_ROOT:PATH=[MTL_ROOT]" dunecontrol --current configure
cmake --build build-cmake
```

How to Cite AMDiS
=================
If you use AMDiS as part of your research and in publications, we kindly ask you to cite the publications, AMDiS is based on, and the source repository, including the release version that you are using, e.g.
```
@article{VeyVoigt2007AMDiS,
  author    = {Simon Vey and Axel Voigt},
  journal   = {Computing and Visualization in Science},
  title     = {{AMDiS}: {A}daptive {M}ulti{Di}mensional {S}imulations},
  year      = {2007},
  pages     = {57--67},
  volume    = {10},
  doi       = {10.1007/s00791-006-0048-3},
}
@article{WitkowskiEtAl2015SoftwareConcepts,
  author    = {Thomas Witkowski and Siqi Ling and Simon Praetorius and Axel Voigt},
  journal   = {Advances in Computational Mathematics},
  title     = {Software concepts and numerical algorithms for a scalable adaptive
               parallel finite element method},
  year      = {2015},
  number    = {6},
  pages     = {1145--1177},
  volume    = {41},
  doi       = {10.1007/s10444-015-9405-4},
}
@misc{AMDiS:2.10,
  key    = {AMDiS},
  title  = {Adaptive Multi-Dimensional Simulations},
  note   = {v2.10},
  howpublished = {\url{https://gitlab.com/amdis/amdis}},
}
```

Since it is a Dune module, you are asked to cite an appropriate Dune publication, see [dune-project.org/about/publications](https://dune-project.org/about/publications/), e.g,
```
@article{Dune:2021,
  title   = {The \textsc{Dune} framework: Basic concepts and recent developments},
  author  = {Peter Bastian and Markus Blatt and Andreas Dedner and Nils-Arne Dreier
             and Christian Engwer and Ren{\'e} Fritze and Carsten Gr{\"a}ser
             and Christoph Gr{\"u}ninger and Dominic Kempf and Robert Kl{\"o}fkorn
             and Mario Ohlberger and Oliver Sander},
  volume  = {81},
  journal = {Computers and Mathematics with Applications},
  year    = {2021},
  pages   = {75--112},
  doi     = {10.1016/j.camwa.2020.06.007}
}
```

Please note that other Dune modules might require citation of further papers, such as [dune-alugrid](https://gitlab.dune-project.org/extensions/dune-alugrid), , [dune-curvedgrid](https://gitlab.dune-project.org/extensions/dune-curvedgrid), or [dune-foamgrid](https://gitlab.dune-project.org/extensions/dune-foamgrid). See their corresponding README for how to cite their publications, or the [dune-project.org/about/publications](https://dune-project.org/about/publications/) page for an overview of some modules.