// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <iostream>

#include <amdis/AMDiS.hpp>
#include <amdis/Integrate.hpp>
#include <amdis/ProblemStat.hpp>
#include "Tests.hpp"

#include <dune/grid/onedgrid.hh>

using namespace AMDiS;

template <class IS, class Expr, class GV>
auto integrate_on_boundary(BoundarySubset<IS> const& boundary, Expr&& expr, GV const& gridView, int localFctOrder = -1)
{
  auto gridFct = makeGridFunction(FWD(expr), gridView);
  auto localFct = localFunction(gridFct);

  using LF = TYPEOF(localFct);
  auto quadOrder = [localFctOrder](auto const& lf) {
    DUNE_UNUSED_PARAMETER(localFctOrder);
    if constexpr(Concepts::Polynomial<LF>)
      return order(lf);
    else
      return localFctOrder;
  };

  test_exit(Concepts::Polynomial<LF> || localFctOrder >= 0, "Polynomial degree of expression can not be deduced. You need to provide an explicit value for the quadrature degree or a quadrature rule in `integrate_on_boundary()`.");

  using Range = typename decltype(localFct)::Range;
  Range result(0);

  using Rules = Dune::QuadratureRules<typename GV::ctype, GV::dimension-1>;
  for (auto const& element : elements(gridView, Dune::Partitions::interior)) {
    if (!element.hasBoundaryIntersections())
      continue;

    localFct.bind(element);
    for (auto const& is : intersections(gridView, element)) {
      if (!boundary(is))
        continue;

      auto geo = is.geometry();
      auto geoInInside = is.geometryInInside();
      auto const& quad = Rules::rule(is.type(), quadOrder(localFct));

      for (auto const qp : quad)
        result += localFct(geoInInside.global(qp.position())) * geo.integrationElement(qp.position()) * qp.weight();
    }
    localFct.unbind();
  }

  return gridView.comm().sum(result);
}

template <class Grid, class Basis>
void test1(Grid& grid, Basis const& basis)
{
  Parameters::set("prob1->solver","direct");
  ProblemStat prob1{"prob1", grid, basis};
  prob1.initialize(INIT_ALL);

  Parameters::set("prob2->solver","direct");
  ProblemStat prob2{"prob2", grid, basis};
  prob2.initialize(INIT_ALL);


  prob1.addMatrixOperator(sot(1.0), 0, 0);
  prob1.addMatrixOperator(zot(1.0), 0, 1);
  prob1.addMatrixOperator(zot(1.0), 1, 0);
  prob1.addMatrixOperator(zot(1.0), 1, 1);
  prob1.addVectorOperator(zot(1.0), 0);
  prob1.addDirichletBC(BoundaryType{1}, 0, 0, 0.0);

  prob2.addMatrixOperator(zot(1.0), 0, 0);
  prob2.addMatrixOperator(sot(1.0), 0, 1);
  prob2.addMatrixOperator(zot(1.0), 1, 0);
  prob2.addMatrixOperator(zot(1.0), 1, 1);
  prob2.addVectorOperator(zot(1.0), 0);
  prob2.addDirichletBC(BoundaryType{1}, 0, 1, 0.0);

  AdaptInfo adaptInfo{"adapt"};
  prob1.assemble(adaptInfo);
  prob1.solve(adaptInfo);

  prob2.assemble(adaptInfo);
  prob2.solve(adaptInfo);

  // check that the solutions are equivalent
  double err1 = std::sqrt(integrate(pow<2>(prob1.solution(0) - prob2.solution(1)), prob1.gridView()));
  AMDIS_TEST(err1 < 1.e-8);

  auto boundary = BoundarySubset{prob1.boundaryManager(), 1};

  // check that solution has correct boundary values
  double err2 = std::sqrt(integrate_on_boundary(boundary, pow<2>(prob1.solution(0)), prob1.gridView()));
  AMDIS_TEST(err2 < 1.e-8);

  double err3 = std::sqrt(integrate_on_boundary(boundary, pow<2>(prob2.solution(1)), prob1.gridView()));
  AMDIS_TEST(err3 < 1.e-8);
}

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  Dune::OneDGrid grid(10,0.0,1.0);

  using namespace Dune::Functions::BasisFactory;
  test1(grid,power<2>(lagrange<1>()));

  return report_errors();
}
