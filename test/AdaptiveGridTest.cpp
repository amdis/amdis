#include <config.h>

#include <dune/common/filledarray.hh>
#include <dune/grid/test/gridcheck.hh>
#include <amdis/AdaptiveGrid.hpp>

#if HAVE_DUNE_UGGRID
  #include <dune/grid/uggrid.hh>
#else
  #include <dune/grid/yaspgrid.hh>
#endif

#include "Tests.hpp"

using namespace AMDiS;

// test IdentityGrid for given dimension
template <class HostGrid>
void testGrid(HostGrid& grid)
{
  grid.globalRefine(1);

  AdaptiveGrid<HostGrid> adaptiveGrid(grid);
  adaptiveGrid.globalRefine(1);

  gridcheck(adaptiveGrid);
}


int main(int argc, char** argv)
{
  Environment env(argc, argv);

#if HAVE_DUNE_UGGRID
  std::cout << std::endl << "UGGrid<2,Simplex>..." << std::endl;
  {
    using HostGrid = Dune::UGGrid<2>;
    auto hostGridPtr = Dune::StructuredGridFactory<HostGrid>::createSimplexGrid({0.0,0.0}, {1.0,1.0}, {8u,8u});
    testGrid(*hostGridPtr);
  }

  std::cout << std::endl << "UGGrid<2,Cube>..." << std::endl;
  {
    using HostGrid = Dune::UGGrid<2>;
    auto hostGridPtr = Dune::StructuredGridFactory<HostGrid>::createCubeGrid({0.0,0.0}, {1.0,1.0}, {8u,8u});
    testGrid(*hostGridPtr);
  }

  std::cout << std::endl << "UGGrid<3,Simplex>..." << std::endl;
  {
    using HostGrid = Dune::UGGrid<3>;
    auto hostGridPtr = Dune::StructuredGridFactory<HostGrid>::createSimplexGrid({0.0,0.0,0.0}, {1.0,1.0,1.0}, {4u,4u,4u});
    testGrid(*hostGridPtr);
  }

  std::cout << std::endl << "UGGrid<3,Cube>..." << std::endl;
  {
    using HostGrid = Dune::UGGrid<3>;
    auto hostGridPtr = Dune::StructuredGridFactory<HostGrid>::createCubeGrid({0.0,0.0,0.0}, {1.0,1.0,1.0}, {4u,4u,4u});
    testGrid(*hostGridPtr);
  }
#else
  std::cout << "YaspGrid<2>..." << std::endl;
  {
    using HostGrid = Dune::YaspGrid<2>;
    HostGrid hostGrid({1.0,1.0}, {8,8});
    testGrid(hostGrid);
  }

  std::cout << "YaspGrid<3>..." << std::endl;
  {
    using HostGrid = Dune::YaspGrid<3>;
    HostGrid hostGrid({1.0,1.0,1.0}, {4,4,4});
    testGrid(hostGrid);
  }
#endif

  return report_errors();
}
