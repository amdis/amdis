#include <algorithm>
#include <functional>
#include <string>
#include <vector>

#include <amdis/AMDiS.hpp>
#include <amdis/common/parallel/Collective.hpp>
#include <amdis/common/parallel/Communicator.hpp>
#include <amdis/common/parallel/RequestOperations.hpp>
#include "Tests.hpp"

namespace AMDiS { namespace Impl {
  template <class Op>
  void test_allreduce(Op op, int init)
  {
    int s = Environment::mpiSize();
    int r = Environment::mpiRank()+1;
    int result = 0;

    std::vector<int> ranks(s);
    std::iota(ranks.begin(), ranks.end(), 1);

    // in, out
    Mpi::all_reduce(Environment::comm(), r, result, op);
    // inout
    Mpi::all_reduce(Environment::comm(), r, op);

    AMDIS_TEST_EQ(r, result);
    AMDIS_TEST_EQ(r, std::accumulate(ranks.begin(), ranks.end(), init, op));
  }
}}

using namespace AMDiS;


// ---------------------------------------------------------------------------------------
// Test of collective MPI operations
void test0()
{
  Impl::test_allreduce(Operation::Plus{}, 0);
  Impl::test_allreduce(std::plus<>{}, 0);

  Impl::test_allreduce(Operation::Multiplies{}, 1);
  Impl::test_allreduce(std::multiplies<>{}, 1);

  Impl::test_allreduce(Operation::Min{}, 99999);
  Impl::test_allreduce(Operation::Max{}, -99999);

  int s = Environment::mpiSize();
  int r = Environment::mpiRank();
  std::vector<int> ranks(s, 0);

  Mpi::all_gather(Environment::comm(), r, ranks);
  for (int i = 0; i < s; ++i)
    AMDIS_TEST_EQ(ranks[i], i);

  ranks.resize(2*s);
  std::fill(ranks.begin(), ranks.end(), 0);
  Mpi::all_gather(Environment::comm(), std::array<int,2>{r,r}, ranks);
  for (int i = 0; i < s; ++i) {
    AMDIS_TEST_EQ(ranks[2*i], i);
    AMDIS_TEST_EQ(ranks[2*i+1], i);
  }

  auto ranks2 = Mpi::all_gather(Environment::comm(), r);
  AMDIS_TEST_EQ(ranks2.size(), s);
  for (int i = 0; i < s; ++i)
    AMDIS_TEST_EQ(ranks2[i], i);

  auto ranks3 = Mpi::all_gather(Environment::comm(), std::array<int,2>{r,r});
  AMDIS_TEST_EQ(ranks3.size(), 2*s);
  for (int i = 0; i < s; ++i) {
    AMDIS_TEST_EQ(ranks3[2*i], i);
    AMDIS_TEST_EQ(ranks3[2*i+1], i);
  }
}

// ---------------------------------------------------------------------------------------
// Test of Point-to-Point communication (blocking)
void test1()
{
  Mpi::Communicator world(Environment::comm());

  AMDIS_TEST_EQ(world.rank(), Environment::mpiRank());
  AMDIS_TEST_EQ(world.size(), Environment::mpiSize());

  // direct send/recv
  int data = 0;
  if (world.rank() == 0) {
    data = world.size();
    for (int i = 1; i < world.size(); ++i)
      world.send(data, i, Mpi::Tag{10});
  } else {
    world.recv(data, 0, Mpi::Tag{10});
  }
  AMDIS_TEST_EQ(data, world.size());


  // send/recv of pointers
  data = 0;
  if (world.rank() == 0) {
    data = world.size();
    for (int i = 1; i < world.size(); ++i)
      world.send(&data, 1u, i, Mpi::Tag{11});
  } else {
    world.recv(&data, 1u, 0, Mpi::Tag{11});
  }
  AMDIS_TEST_EQ(data, world.size());


  // send/recv of c-arrays
  int data2[] = {0};
  if (world.rank() == 0) {
    data2[0] = world.size();
    for (int i = 1; i < world.size(); ++i)
      world.send(data2, i, Mpi::Tag{12});
  } else {
    world.recv(data2, 0, Mpi::Tag{12});
  }
  AMDIS_TEST_EQ(data2[0], world.size());


  // send/recv of std::arrays
  std::array<int,1> data3{0};
  if (world.rank() == 0) {
    data3[0] = world.size();
    for (int i = 1; i < world.size(); ++i)
      world.send(data3, i, Mpi::Tag{13});
  } else {
    world.recv(data3, 0, Mpi::Tag{13});
  }
  AMDIS_TEST_EQ(data3[0], world.size());


  // send/recv of std::vector
  std::vector<int> data4;
  if (world.rank() == 0) {
    data4.push_back(world.size());
    for (int i = 1; i < world.size(); ++i)
      world.send(data4, i, Mpi::Tag{14});
  } else {
    world.recv(data4, 0, Mpi::Tag{14});
  }
  AMDIS_TEST_EQ(data4.size(), 1);
  AMDIS_TEST_EQ(data4[0], world.size());


  // send/recv of std::string
  std::string data5 = "";
  if (world.rank() == 0) {
    data5 = "world";
    for (int i = 1; i < world.size(); ++i)
      world.send(data5, i, Mpi::Tag{15});
  } else {
    world.recv(data5, 0, Mpi::Tag{15});
  }
  AMDIS_TEST_EQ(data5, std::string("world"));
}


// ---------------------------------------------------------------------------------------
// Test of Point-to-Point communication (non-blocking)
void test2()
{
  Mpi::Communicator world(Environment::comm());

  // direct send/recv
  int data = 0;
  if (world.rank() == 0) {
    data = world.size();
    std::vector<Mpi::Request> requests;
    for (int i = 1; i < world.size(); ++i)
      requests.emplace_back( world.isend(data, i, Mpi::Tag{20}) );
    Mpi::wait_all(requests.begin(), requests.end());
  } else {
    Mpi::Request request( world.irecv(data, 0, Mpi::Tag{20}) );
    MPI_Status status = request.wait();
    AMDIS_TEST(status.MPI_SOURCE == 0);
    AMDIS_TEST(status.MPI_TAG == 20);
  }
  AMDIS_TEST_EQ(data, world.size());


  // send/recv of pointers
  data = 0;
  if (world.rank() == 0) {
    data = world.size();
    std::vector<Mpi::Request> requests;
    for (int i = 1; i < world.size(); ++i)
      requests.push_back( world.isend(&data, 1u, i, Mpi::Tag{21}) );
    Mpi::wait_all(requests.begin(), requests.end());
  } else {
    Mpi::Request request = world.irecv(&data, 1u, 0, Mpi::Tag{21});
    MPI_Status status = request.wait();
    AMDIS_TEST(status.MPI_SOURCE == 0);
    AMDIS_TEST(status.MPI_TAG == 21);
  }
  AMDIS_TEST_EQ(data, world.size());

#if 1
  // send/recv of c-arrays
  int data2[] = {0};
  if (world.rank() == 0) {
    data2[0] = world.size();
    std::vector<Mpi::Request> requests(world.size()-1);
    for (int i = 1; i < world.size(); ++i)
      requests[i-1] = world.isend(data2, 1, i, Mpi::Tag{22});
    Mpi::wait_all(requests.begin(), requests.end());
  } else {
    auto request = world.irecv(data2, 0, Mpi::Tag{22});
    MPI_Status status = request.wait();
    AMDIS_TEST(status.MPI_SOURCE == 0);
    AMDIS_TEST(status.MPI_TAG == 22);
  }
  AMDIS_TEST_EQ(data2[0], world.size());
#endif

#if 1
  // send/recv of std::arrays
  std::array<int,1> data3{0};
  if (world.rank() == 0) {
    data3[0] = world.size();
    std::vector<Mpi::Request> requests;
    for (int i = 1; i < world.size(); ++i)
      requests.emplace_back( world.isend(data3, i, Mpi::Tag{23}) );
    Mpi::wait_all(requests.begin(), requests.end());
  } else {
    Mpi::Request request{ world.irecv(data3, 0, Mpi::Tag{23}) };
    MPI_Status status = request.wait();
    AMDIS_TEST(status.MPI_SOURCE == 0);
    AMDIS_TEST(status.MPI_TAG == 23);
  }
  AMDIS_TEST_EQ(data3[0], world.size());
#endif

  // send/recv of std::vector
  std::vector<int> data4;
  if (world.rank() == 0) {
    data4.push_back(world.size());
    std::vector<Mpi::Request> requests;
    for (int i = 1; i < world.size(); ++i)
      requests.emplace_back( world.isend(data4, i, Mpi::Tag{24}) );
    Mpi::test_any(requests.begin(), requests.end());
    Mpi::wait_all(requests.begin(), requests.end());
  } else {
    Mpi::Request request{ world.irecv(data4, 0, Mpi::Tag{24}) };
    MPI_Status status = request.wait();
    AMDIS_TEST(status.MPI_SOURCE == 0);
    AMDIS_TEST(status.MPI_TAG == 24);
  }
  AMDIS_TEST_EQ(data4.size(), 1);
  AMDIS_TEST_EQ(data4[0], world.size());


  // send/recv of std::string
  std::string data5 = "";
  if (world.rank() == 0) {
    data5 = "world";
    std::vector<Mpi::Request> requests;
    for (int i = 1; i < world.size(); ++i)
      requests.emplace_back( world.isend(data5, i, Mpi::Tag{25}) );
    Mpi::wait_all(requests.begin(), requests.end());
  } else {
    Mpi::Request request{ world.irecv(data5, 0, Mpi::Tag{25}) };
    MPI_Status status = request.wait();
    AMDIS_TEST(status.MPI_SOURCE == 0);
    AMDIS_TEST(status.MPI_TAG == 25);
  }
  AMDIS_TEST_EQ(data5, std::string("world"));
}


int main(int argc, char** argv)
{
  Environment env(argc, argv);
  test0();
  test1();
  test2();

  return report_errors();
}
