#include <config.h>

#include <dune/grid/yaspgrid.hh>

#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>

#include <amdis/Environment.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/typetree/Concepts.hpp>
#include <amdis/typetree/TreePath.hpp>

using namespace AMDiS;

int main (int argc, char** argv)
{
  Environment env(argc, argv);

  Dune::YaspGrid<2> grid({1.0,1.0}, {1,1});

  using namespace Dune::Functions::BasisFactory;
  auto basis = makeBasis(grid.leafGridView(),
    composite(
      power<2>(lagrange<2>()),
      lagrange<1>()
    ));

  using Tree = TYPEOF(basis.localView().tree());

  // index access of composite node
  static_assert(not Concepts::ValidTreePath<Tree,int>);
  static_assert(not Concepts::ValidTreePath<Tree,std::size_t>);
  static_assert(    Concepts::ValidTreePath<Tree,std::integral_constant<int,0>>);
  static_assert(    Concepts::ValidTreePath<Tree,std::integral_constant<std::size_t,0>>);

  static_assert(not Concepts::ValidTreePath<Tree,std::integral_constant<int,2>>);
  static_assert(not Concepts::ValidTreePath<Tree,std::integral_constant<std::size_t,2>>);

  using namespace Dune::Indices;

  auto tp0 = makeTreePath(0);
  auto tp1 = makeTreePath(_0);
  static_assert(not Concepts::ValidTreePath<Tree,TYPEOF(tp0)>);
  static_assert(    Concepts::ValidTreePath<Tree,TYPEOF(tp1)>);

  auto tp2 = makeTreePath(_0,0);
  auto tp3 = makeTreePath(_0,_0);
  auto tp4 = makeTreePath(_1,0);
  static_assert(    Concepts::ValidTreePath<Tree,TYPEOF(tp2)>);
  static_assert(    Concepts::ValidTreePath<Tree,TYPEOF(tp3)>);
  static_assert(not Concepts::ValidTreePath<Tree,TYPEOF(tp4)>);

  auto tp5 = makeTreePath(_0,0,0);
  auto tp6 = makeTreePath(_0,0,_0);
  static_assert(not Concepts::ValidTreePath<Tree,TYPEOF(tp5)>);
  static_assert(not Concepts::ValidTreePath<Tree,TYPEOF(tp6)>);
}
