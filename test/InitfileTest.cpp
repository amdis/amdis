#include <amdis/AMDiS.hpp>
#include <amdis/Initfile.hpp>
#include "Tests.hpp"

int main(int argc, char** argv)
{
  using namespace AMDiS;
  using namespace Dune::Functions::BasisFactory;

  AMDiS::Environment env(argc, argv);

  std::string fn = "/tmp/initfile.dat";
  std::string fn2 = "/tmp/initfile2.dat";
  {
    std::ofstream f_out(fn, std::ios_base::out);
    f_out << "int: 4\n";                                            // 4.0
    f_out << "scalar: 2.0\n";                                       // 2.0
    f_out << "scalar->expr: 2.0*3.0\n";                             // 6.0
    f_out << "scalar->expr->var: 2.0*${scalar}\n";                  // 4.0
    f_out << "vector: 2.0 3.0\n";                                   // 2.0 3.0
    f_out << "vector->expr: 2.0*3.0 3.0*4.0\n";                     // 6.0 12.0
    f_out << "vector->expr->var: 2.0*${scalar} 3.0*${scalar}\n";    // 4.0 6.0
    f_out << "#include <" << fn2 << ">\n";

    std::ofstream f2_out(fn2, std::ios_base::out);
    f2_out << "scalar2: 7.0\n";
  }

  Initfile::init(fn);

  // test reading scalar values
  AMDIS_TEST_EQ(Initfile::get<int>("int").value(), 4);
  AMDIS_TEST_EQ(Initfile::get<double>("scalar").value(), 2.0);
  AMDIS_TEST_EQ(Initfile::get<double>("scalar->expr").value(), 6.0);
  AMDIS_TEST_EQ(Initfile::get<double>("scalar->expr->var").value(), 4.0);

  // test parsing vectors
  AMDIS_TEST((Initfile::get<std::array<double,2>>("vector").value() == std::array{2.0,3.0}));
  AMDIS_TEST((Initfile::get<std::array<double,2>>("vector->expr").value() == std::array{6.0,12.0}));
  AMDIS_TEST((Initfile::get<std::array<double,2>>("vector->expr->var").value() == std::array{4.0,6.0}));

  AMDIS_TEST((Initfile::get<Dune::FieldVector<double,2>>("vector").value() == Dune::FieldVector<double,2>{2.0,3.0}));
  AMDIS_TEST((Initfile::get<Dune::FieldVector<double,2>>("vector->expr").value() == Dune::FieldVector<double,2>{6.0,12.0}));
  AMDIS_TEST((Initfile::get<Dune::FieldVector<double,2>>("vector->expr->var").value() == Dune::FieldVector<double,2>{4.0,6.0}));

  AMDIS_TEST((Initfile::get<std::vector<double>>("vector").value() == std::vector{2.0,3.0}));
  AMDIS_TEST((Initfile::get<std::vector<double>>("vector->expr").value() == std::vector{6.0,12.0}));
  AMDIS_TEST((Initfile::get<std::vector<double>>("vector->expr->var").value() == std::vector{4.0,6.0}));

  // test of include statements
  AMDIS_TEST_EQ(Initfile::get<double>("scalar2").value(), 7.0);

  return report_errors();
}
