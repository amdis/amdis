// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#include <iostream>

#include <amdis/AMDiS.hpp>
#include <amdis/LinearAlgebra.hpp>
#include "Tests.hpp"

#include <dune/grid/onedgrid.hh>

using namespace AMDiS;

template <class Basis>
void test1(Basis const& basis)
{
  using Matrix = MatrixFacade<double, BackendTraits::template Matrix<Basis,Basis>::template Impl>;
  using Vector = VectorFacade<double, BackendTraits::template Vector<Basis>::template Impl>;

  using MI = typename Basis::MultiIndex;

  using Pattern = typename BackendTraits::template Matrix<Basis,Basis>::SparsityPattern;
  Pattern pattern{basis, basis};

  Vector vectorFacade{basis};
  vectorFacade.init(basis,true);

  Matrix matrixFacade{basis, basis};
  matrixFacade.init(pattern);
  {
    auto localView = basis.localView();
    for (auto const& e : entitySet(basis))
    {
      localView.bind(e);
      FlatMatrix<double> localMat(localView.size(), localView.size());
      FlatVector<double> localVec(localView.size());
      for (std::size_t i = 0; i < localView.size(); ++i) {
        localMat[i][i] = 2.0;
        localVec[i] = 2.0;
      }
      matrixFacade.scatter(localView, localView, localMat);
      vectorFacade.scatter(localView, localVec, Assigner::assign{});
    }
  }
  matrixFacade.finish();

  Vector resultFacade{basis};
  resultFacade.init(basis,true);

  auto& matrix = matrixFacade.impl().matrix();
  auto& vector = vectorFacade.impl().vector();
  auto& result = resultFacade.impl().vector();

  // test 1: set the whole row to 0
  matrixFacade.zeroRows(std::vector{MI{0}}, false);

#if AMDIS_BACKEND == AMDIS_BACKEND_ISTL
  {
    result = 0.0;
    matrix.mv(vector, result);
    AMDIS_TEST_EQ(result[0], 0.0);
    AMDIS_TEST_NE(result[1], 0.0);
  }
#endif

  // test 2: set a unit row
  matrixFacade.zeroRows(std::vector{MI{0}}, true);

#if AMDIS_BACKEND == AMDIS_BACKEND_ISTL
  {
    result = 0.0;
    matrix.mv(vector, result);
    AMDIS_TEST_EQ(result[0], vector[0]);
    AMDIS_TEST_NE(result[1], 0.0);
  }
#endif

  // test 3: zero rows and columns
  matrixFacade.zeroRowsColumns(std::vector{MI{0}}, false);
  matrixFacade.zeroRowsColumns(std::vector{MI{0}}, true);
  matrixFacade.zeroRowsColumns(std::vector{MI{0}}, false, vectorFacade, resultFacade);
  matrixFacade.zeroRowsColumns(std::vector{MI{0}}, true, vectorFacade, resultFacade);
}

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  Dune::OneDGrid grid(10,0.0,1.0);
  auto gridView = grid.leafGridView();

  using namespace Dune::Functions::BasisFactory;
  GlobalBasis basis1{gridView, lagrange<1>()};
  // GlobalBasis basis2{gridView, lagrange<2>()};

  test1(basis1);
  // test2(basis1, basis2);

  return report_errors();
}
