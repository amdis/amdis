#include <array>
#include <vector>

#include <dune/common/reservedvector.hh>
#include <dune/functions/common/multiindex.hh>

#include <amdis/common/FieldMatVec.hpp>
#include <amdis/common/Concepts.hpp>

using namespace AMDiS;

int main()
{
  using V = Dune::FieldVector<double,2>;

  // Concepts::Callable
  auto f = [](V const&) { return 0.0; };
  auto b = [](V const&) { return true; };

  static_assert( Concepts::Callable<decltype(f),V>, "" );
  static_assert( Concepts::Functor<decltype(f),double(V)>, "" );
  static_assert( Concepts::Functor<decltype(b),bool(V)>, "" );
  static_assert( Concepts::Predicate<decltype(b),V>, "" );

  // Concepts::MultiIndex
  static_assert( Concepts::MultiIndex<Dune::ReservedVector<std::size_t,2>>, "" );
  static_assert( Concepts::MultiIndex<Dune::Functions::StaticMultiIndex<std::size_t,1>>, "" );
  static_assert( Concepts::MultiIndex<std::array<std::size_t,2>>, "" );
  static_assert( Concepts::MultiIndex<std::vector<std::size_t>>, "" );
}
