#include <amdis/AMDiS.hpp>
#include <amdis/ContextGeometry.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/algorithm/ForEach.hpp>
#include <amdis/localoperators/ConvectionDiffusionOperator.hpp>
#include <amdis/localoperators/StokesOperator.hpp>

using namespace AMDiS;

using T = double;

template <class LOp>
using IsOperatorList = decltype(std::declval<LOp>().empty());

// Test the functionality of a local operator
template <class LOp, class LocalView, class MatVec, class... Nodes>
void test_local_operator(LOp&& lop, LocalView const& localView, MatVec& matVec,
                         Nodes const&... nodes)
{
  auto const& gridView = localView.globalBasis().gridView();
  auto const& element = localView.element();
  auto geometry = element.geometry();

  [[maybe_unused]] auto context = [&]{
    if constexpr(Dune::Std::is_detected_v<IsOperatorList,LOp>)
      return GlobalContext<TYPEOF(gridView)>{gridView, element, geometry};
    else
      return ContextGeometry<TYPEOF(element)>{element, element, geometry};
  }();

  lop.bind(element);
  if constexpr(!Dune::Std::is_detected_v<IsOperatorList,LOp>)
    lop.assemble(context, nodes..., matVec);
  lop.unbind();
}


// Test the functionality of an operator
template <class Op, class LocalView, class... Args>
void test_operator(Op&& op, LocalView const& localView, Args&&... args)
{
  op.update(localView.globalBasis().gridView());

  auto lop = localOperator(op);
  test_local_operator(lop, localView, FWD(args)...);
}


// Test a hierarchic list of operators
template <class OpList, class LocalView, class Mat>
void test_mat_assembler(OpList& opList, LocalView const& localView, Mat& mat)
{
  Recursive::forEach(opList, [&](auto& op) {
    op.update(localView.globalBasis().gridView());
    auto lop = localAssembler(op);
    test_local_operator(lop, localView, mat, localView.treeCache(), localView.treeCache());
  });

  auto lopList = localAssembler(opList);
  Recursive::forEach(lopList, [&](auto& lop) {
    test_local_operator(lop, localView, mat, localView.treeCache(), localView.treeCache());
  });
}

template <class OpList, class LocalView, class Vec>
void test_vec_assembler(OpList& opList, LocalView const& localView, Vec& vec)
{
  Recursive::forEach(opList, [&](auto& op) {
    op.update(localView.globalBasis().gridView());
    auto lop = localAssembler(op);
    test_local_operator(lop, localView, vec, localView.treeCache());
  });

  auto lopList = localAssembler(opList);
  Recursive::forEach(lopList, [&](auto& lop) {
    test_local_operator(lop, localView, vec, localView.treeCache());
  });
}


// Test the copy and move constructed operators
template <class Op, class Test>
void test_construction(Op const& op, Test test)
{
  // copy construction
  Op op2(op);
  test(op2);

  auto op3 = op2;
  test(op3);

  // move construction
  Op op4(std::move(op3));
  test(op4);

  auto op5 = std::move(op4);
  test(op5);
}


// Test a vector operator
template <class Op, class Basis, class Path>
void test(Op& op, Basis const& basis, Path path)
{
  using GridView = typename Basis::GridView;
  GridView gridView = basis.gridView();

  auto localView = basis.localView();
  auto element = *gridView.template begin<0>();
  localView.bind(element);

  auto const& tree = localView.treeCache();
  auto const& node = Dune::TypeTree::child(tree, path);

  using ElementVector = Dune::DynamicVector<T>;
  ElementVector vec(localView.size());

  using Element = typename GridView::template Codim<0>::Entity;
  auto a_op = makeOperator<Element>(op, gridView);
  test_operator(a_op, localView, vec, node);
  test_construction(a_op, [&](auto& op)
  {
    test_operator(op, localView, vec, node);
  });

  using Traits = Impl::OperatorTraits<GridView,Element,ElementVector>;

  // Construct a type-erasure Operator from a concrete operator
  using Node = TYPEOF(node);
  using OpBase = Operator<Traits,Node>;
  OpBase b_op(a_op);
  test_operator(b_op, localView, vec, node);
  test_construction(b_op, [&](auto& op)
  {
    test_operator(op, localView, vec, node);
  });

  VectorAssembler<Basis,ElementVector> opList;
  opList.push(tag::element_operator<Element>{}, OperatorAdapter{a_op,path});
  test_vec_assembler(opList, localView, vec);

  OpBase c_op(std::move(a_op));
  test_operator(c_op, localView, vec, node);
}


// Test a matrix operator
template <class Op, class Basis, class Row, class Col>
void test(Op& op, Basis const& basis, Row row, Col col)
{
  using GridView = typename Basis::GridView;
  GridView gridView = basis.gridView();

  auto localView = basis.localView();
  auto element = *gridView.template begin<0>();
  localView.bind(element);

  auto const& tree = localView.treeCache();
  auto const& rowNode = Dune::TypeTree::child(tree, row);
  auto const& colNode = Dune::TypeTree::child(tree, col);

  using ElementMatrix = Dune::DynamicMatrix<T>;
  ElementMatrix mat(localView.size(), localView.size());

  using Element = typename GridView::template Codim<0>::Entity;
  auto a_op = makeOperator<Element>(op, gridView);
  test_operator(a_op, localView, mat, rowNode, colNode);
  test_construction(a_op, [&](auto& op)
  {
    test_operator(op, localView, mat, rowNode, colNode);
  });

  using Traits = Impl::OperatorTraits<GridView,Element,ElementMatrix>;

  // Construct a type-erasure Operator from a concrete operator
  using RowNode = TYPEOF(rowNode);
  using ColNode = TYPEOF(colNode);
  using OpBase = Operator<Traits,RowNode,ColNode>;
  OpBase b_op(a_op);
  test_operator(b_op, localView, mat, rowNode, colNode);
  test_construction(b_op, [&](auto& op)
  {
    test_operator(op, localView, mat, rowNode, colNode);
  });

  MatrixAssembler<Basis,Basis,ElementMatrix> opList;
  opList.push(tag::element_operator<Element>{}, OperatorAdapter{a_op,row,col});
  test_mat_assembler(opList, localView, mat);
}

template <class Problem>
void test_problem_operators(Problem& prob)
{
  auto localView = prob.globalBasis()->localView();
  auto element = *prob.gridView().template begin<0>();
  localView.bind(element);

  FlatVector<T> elementVec(localView.size());
  FlatMatrix<T> elementMat(localView.size(), localView.size());

  { // test problem operators
    test_vec_assembler(prob.rhsVector()->assembler(), localView, elementVec);
    test_mat_assembler(prob.systemMatrix()->assembler(), localView, elementMat);

    test_construction(prob.rhsVector()->assembler(), [&](auto& op) {
      test_vec_assembler(op, localView, elementVec);
    });
    test_construction(prob.systemMatrix()->assembler(), [&](auto& op) {
      test_mat_assembler(op, localView, elementMat);
    });
  }
}


int main(int argc, char** argv)
{
  Environment env(argc, argv);

  using namespace Dune::Indices;

  // use T as coordinate type
  using HostGrid = Dune::YaspGrid<2, Dune::EquidistantCoordinates<T,2>>;
  HostGrid hostGrid({T(1), T(1)}, {8,8});
  AdaptiveGrid<HostGrid> grid(hostGrid);

  // use T as range type for basis
  using namespace Dune::Functions::BasisFactory;
  GlobalBasis basis{grid.leafGridView(), composite(power<2>(lagrange<2,T>(), flatLexicographic()), lagrange<1,T>(), flatLexicographic())};
  using Basis = decltype(basis);

  // use T as coefficient type
  using Param = DefaultProblemTraits<Basis, T>;


  ProblemStat<Param> prob("ellipt", grid, basis);
  prob.initialize(INIT_ALL);

  auto _v = makeTreePath(_0);
  auto _p = makeTreePath(_1);

  FieldVector<T,1> scalar = 1.0;
  FieldVector<T,2> vec; vec = 1.0;
  FieldMatrix<T,2,2> mat; mat = 1.0;

  auto op0a = sot(scalar);
  auto op0b = zot(scalar);
  test(op0a, *prob.globalBasis(), _p, _p);
  prob.addMatrixOperator(op0a, _p, _p);
  test(op0b, *prob.globalBasis(), _p);
  prob.addVectorOperator(op0b, _p);

  auto op1 = makeOperator(tag::divtestvec{}, scalar);
  test(op1, *prob.globalBasis(), _v);
  prob.addVectorOperator(op1, _v);

  auto op2 = makeOperator(tag::divtestvec_trial{}, scalar);
  test(op2, *prob.globalBasis(), _v, _p);
  prob.addMatrixOperator(op2, _v, _p);

  auto op3 = makeOperator(tag::gradtest{}, vec);
  test(op3, *prob.globalBasis(), _p);
  prob.addVectorOperator(op3, _p);

  auto op4 = makeOperator(tag::gradtest_trial{}, vec);
  test(op4, *prob.globalBasis(), _p, _p);
  prob.addMatrixOperator(op4, _p, _p);

  auto op5a = makeOperator(tag::gradtest_trialvec{}, scalar);
  // auto op5b = makeOperator(tag::gradtest_trialvec{}, mat); // TODO: needs to be implemented
  test(op5a, *prob.globalBasis(), _p, _v);
  prob.addMatrixOperator(op5a, _p, _v);

  auto op6 = makeOperator(tag::partialtest{0}, scalar);
  test(op6, *prob.globalBasis(), _p);
  prob.addVectorOperator(op6, _p);

  auto op7 = makeOperator(tag::partialtest_trial{0}, scalar);
  test(op7, *prob.globalBasis(), _p, _p);
  prob.addMatrixOperator(op7, _p, _p);

  auto op8 = makeOperator(tag::test_divtrialvec{}, scalar);
  test(op8, *prob.globalBasis(), _p, _v);
  prob.addMatrixOperator(op8, _p, _v);

  auto op9 = makeOperator(tag::test_gradtrial{}, vec);
  test(op9, *prob.globalBasis(), _p, _p);
  prob.addMatrixOperator(op9, _p, _p);

  auto op10 = makeOperator(tag::test_partialtrial{0}, scalar);
  test(op10, *prob.globalBasis(), _p, _p);
  prob.addMatrixOperator(op10, _p, _p);

  auto op11a = makeOperator(tag::testvec_gradtrial{}, scalar);
  // auto op11b = makeOperator(tag::testvec_gradtrial{}, mat); // TODO: needs to be implemented
  test(op11a, *prob.globalBasis(), _v, _p);
  prob.addMatrixOperator(op11a, _v, _p);

  auto op12 = makeOperator(tag::divtestvec_divtrialvec{}, scalar);
  test(op12, *prob.globalBasis(), _v, _v);
  prob.addMatrixOperator(op12, _v, _v);

  auto op13a = makeOperator(tag::gradtest_gradtrial{}, scalar);
  test(op13a, *prob.globalBasis(), _p, _p);
  prob.addMatrixOperator(op13a, _p, _p);

  auto op13b = makeOperator(tag::gradtest_gradtrial{}, mat);
  test(op13b, *prob.globalBasis(), _p, _p);
  prob.addMatrixOperator(op13b, _p, _p);

  auto op14 = makeOperator(tag::partialtest_partialtrial{0,0}, scalar);
  test(op14, *prob.globalBasis(), _p, _p);
  prob.addMatrixOperator(op14, _p, _p);

  auto op15 = makeOperator(tag::test{}, scalar);
  test(op15, *prob.globalBasis(), _p);
  prob.addVectorOperator(op15, _p);

  auto op16 = makeOperator(tag::test_trial{}, scalar);
  test(op16, *prob.globalBasis(), _p, _p);
  prob.addMatrixOperator(op16, _p, _p);

  auto op17 = makeOperator(tag::test_trialvec{}, vec);
  test(op17, *prob.globalBasis(), _p, _v);
  prob.addMatrixOperator(op17, _p, _v);

  auto op18 = makeOperator(tag::testvec{}, vec);
  test(op18, *prob.globalBasis(), _v);
  prob.addVectorOperator(op18, _v);

  auto op19 = makeOperator(tag::testvec_trial{}, vec);
  test(op19, *prob.globalBasis(), _v, _p);
  prob.addMatrixOperator(op19, _v, _p);

  auto op20a = makeOperator(tag::testvec_trialvec{}, scalar);
  test(op20a, *prob.globalBasis(), _v, _v);
  prob.addMatrixOperator(op20a, _v, _v);

  auto op20b = makeOperator(tag::testvec_trialvec{}, mat);
  test(op20b, *prob.globalBasis(), _v, _v);
  prob.addMatrixOperator(op20b, _v, _v);

  // some special operators

  auto opStokes = makeOperator(tag::stokes{}, scalar);
  test(opStokes, *prob.globalBasis(), makeTreePath(), makeTreePath());
  prob.addMatrixOperator(opStokes);
  prob.addMatrixOperator(opStokes, {}, {});

  auto opCDa = convectionDiffusion(scalar, scalar, scalar, scalar);
  test(opCDa, *prob.globalBasis(), _p, _p);
  test(opCDa, *prob.globalBasis(), _p);
  prob.addMatrixOperator(opCDa, _p, _p);
  prob.addVectorOperator(opCDa, _p);

  auto opCDb = convectionDiffusion(mat, vec, scalar, scalar, std::false_type{});
  test(opCDb, *prob.globalBasis(), _p, _p);
  test(opCDb, *prob.globalBasis(), _p);
  prob.addMatrixOperator(opCDb, _p, _p);
  prob.addVectorOperator(opCDb, _p);

  test_problem_operators(prob);

  auto prob2(prob);
  test_problem_operators(prob2);

  return 0;
}
