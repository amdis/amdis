#include <config.h>
#include <amdis/Environment.hpp>
#include <amdis/algorithm/Map.hpp>
#include <amdis/algorithm/ForEach.hpp>

#include "Tests.hpp"

using namespace AMDiS;

template <class Container>
void test(Container& container)
{
  // initialize container
  int i = 0;
  Recursive::forEach(container, [&](auto& c) { c = i++; });

  // sum up all the elements recursively
  int sum = 0;
  Recursive::forEach(container, [&](auto const& c) { sum += c; });

  // create a new container with scaled elements
  auto scaled = Recursive::map([&](auto const& c) { return 2*c; }, container);

  // sum up agains
  int sum2 = 0;
  Recursive::forEach(scaled, [&](auto const& c) { sum2 += c; });

  AMDIS_TEST_EQ(2*sum, sum2);
}

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  std::vector<std::array<std::tuple<int,int,int>,2>> container1(2);
  test(container1);

  std::tuple<std::array<std::vector<int>,2>> container2;
  std::get<0>(container2)[0].resize(2);
  std::get<0>(container2)[1].resize(2);
  test(container2);

  return report_errors();
}
