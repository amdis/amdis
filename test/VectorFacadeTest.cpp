#include <amdis/AMDiS.hpp>
#include <amdis/DOFVector.hpp>

#include <dune/common/test/testsuite.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

using namespace AMDiS;

#define CHECK(test, ...) \
  test.check(__VA_ARGS__, #__VA_ARGS__)

int main (int argc, char** argv)
{
  Environment env(argc, argv);

  using namespace Dune::Functions::BasisFactory;
  Dune::YaspGrid<2> grid({1.0,1.0}, {6,6});
  auto dv = DOFVector{grid.leafGridView(),lagrange<1>()};
  auto& basis = dv.basis();

  // extract the pure vector facade
  auto& v = dv.coefficients();

  Dune::TestSuite test;

  // check the size of the vector
  CHECK(test, v.localSize() <= v.globalSize());
  if (grid.comm().size() == 1)
    CHECK(test, v.localSize() == basis.dimension());

  // check that the resize has no effect
  auto ls = v.localSize();
  auto gs = v.globalSize();
  v.resize(basis);
  CHECK(test, v.localSize() == ls);
  CHECK(test, v.globalSize() == gs);

  // check that resize with zeroing out gives components with value 0
  v.resizeZero(basis);
  auto lv = basis.localView();
  lv.bind(*basis.gridView().begin<0>());
  CHECK(test, v.get(lv.index(0)) == 0.0);
  CHECK(test, v.state() == VectorState::synchronized);

  v.init(basis, true);
  CHECK(test, v.state() == VectorState::synchronized);
  v.finish();

  // create a copy
  auto v2 = v;

  v = 0.0;
  v2 = 1.0;
  CHECK(test, v.get(lv.index(0)) == 0.0);
  CHECK(test, v2.get(lv.index(0)) == 1.0);

  // call linear algebra operations

  v.axpy(2.0, v2); // v += 2.0 * v2
  CHECK(test, v.get(lv.index(0)) == 2.0);

  v.aypx(1.5, v2); // v = 1.0 * v + v2
  CHECK(test, v.get(lv.index(0)) == 4.0);

  v += v2;
  CHECK(test, v.get(lv.index(0)) == 5.0);

  v -= v2;
  CHECK(test, v.get(lv.index(0)) == 4.0);

  return test.exit();
}