#include <amdis/AMDiS.hpp>
#include <amdis/ProblemStat.hpp>

#include <dune/grid/uggrid.hh>

#include "Tests.hpp"

using namespace AMDiS;

static const int d = 2; // problem dimension

using Grid  = Dune::UGGrid<d>;
using Basis = LagrangeBasis<Grid, 1>;
using Problem = ProblemStat<Basis>;
using DomainType = typename Dune::FieldVector<double,d>;

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  DomainType lowerLeft; lowerLeft = 0.0;    // lower left grid corner
  DomainType upperRight; upperRight = 1.0;  // upper right grid corner
  std::array<unsigned int, d> s; s.fill(1); // number of elements on each axis

  std::shared_ptr<Grid> grid =
    Dune::StructuredGridFactory<Grid>::createCubeGrid(lowerLeft, upperRight, s);

  Problem prob("test", *grid);
  prob.initialize(INIT_ALL);

  AdaptInfo adaptInfo("adapt");

  // refine the grid close to the points (0, 0) and (1, 1)
  auto markerFunc = [](auto const& x) -> double
  {
    static Dune::FieldVector<double, d> x0{0.0, 0.0};
    static Dune::FieldVector<double, d> x1{1.0, 1.0};
    return { 1.0 / (0.1 + std::min(distance(x0, x), distance(x1, x))) };
  };

  GridFunctionMarker marker("mymarker", prob.grid(), markerFunc);

  prob.addMarker(marker);

  for (int i = 0; i < 10; ++i) {
    prob.markElements(adaptInfo);
    if (!prob.adaptGrid(adaptInfo))
      break;
  }

  AMDIS_TEST_EQ(prob.solutionVector()->localSize(), prob.globalBasis()->dimension());
  prob.solution().interpolate(markerFunc);
  prob.writeFiles(adaptInfo);

  prob.removeMarker(marker.name());

  prob.addMarker(marker);
  prob.removeMarker(marker);

  return report_errors();
}
