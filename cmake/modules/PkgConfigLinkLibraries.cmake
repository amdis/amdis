# PkgConfigLinkLibraries.cmake
#
# Scan the LDFLAGS returned by pkg-config for library directories and
# libraries and figure out the absolute paths of that libraries in the
# given directories.
#
# This macro is extracted from FindPkgConfig.cmake
#
# This will define the following macro
#
#    pkg_config_link_libraries(PREFIX, OUTPUT)
#
# .. cmake_macro:: pkg_config_link_libraries
#
#    .. cmake_brief::
#
#       Creates a list ob absolute path for link libraries
#
#    .. cmake_param:: PREFIX
#       :single:
#
#       The module prefix (including _STATIC for static libraries)
#
#    .. cmake_param:: OUTPUT
#       :single:
#
#       The name of the output list of absolute paths
#
#
# Author: Simon Praetorius <simon.praetorius@tu-dresden.de>

macro(pkg_config_link_libraries _prefix _output)
  unset(_search_paths)
  foreach (flag IN LISTS ${_prefix}_LDFLAGS)
    if (flag MATCHES "^-L(.*)")
      list(APPEND _search_paths ${CMAKE_MATCH_1})
      continue()
    endif()
    if (flag MATCHES "^-l(.*)")
      set(_pkg_search "${CMAKE_MATCH_1}")
    else()
      continue()
    endif()

    if(_search_paths)
      # Firstly search in -L paths
      find_library(pkgcfg_lib_${_prefix}_${_pkg_search}
                    NAMES ${_pkg_search}
                    HINTS ${_search_paths} NO_DEFAULT_PATH)
    endif()
    find_library(pkgcfg_lib_${_prefix}_${_pkg_search}
                NAMES ${_pkg_search}
                ${_find_opts})
    if(pkgcfg_lib_${_prefix}_${_pkg_search})
      list(APPEND ${_output} "${pkgcfg_lib_${_prefix}_${_pkg_search}}")
    endif()
  endforeach()
endmacro(pkg_config_link_libraries)