# Module that provides tools for adding executable with all required flags
#
# .. cmake_function:: add_amdis_executable
#
#    .. cmake_brief::
#
#       Adds an executable using the AMDiS library
#
#    .. cmake_param:: NAME
#       :single:
#
#       The name of the test that should be added. If an executable
#       is also added (by specifying SOURCES), the executable is also
#       named accordingly. If omitted, the name will be deduced from
#       the (single) sources parameter or from the given target. Note
#       that this requires you to take care, that you only use a target
#       or source file for but one such test.
#
#    .. cmake_param:: SOURCES
#       :multi:
#
#       The source files that this test depends on. These are the
#       sources that will be passed to :ref:`add_executable`.
#
#       You *must* specify either :code:`SOURCES` or :code:`TARGET`.
#
#    .. cmake_param:: DIM
#       :single:
#
#       Specify the grid dimension.
#
#    .. cmake_param:: DOW
#       :single:
#
#       Specify the world dimension. If not specified it is assumed to be equal to DIM
#
#    .. cmake_param:: ALU_GRID
#       :option:
#
#       Enables ALUGrid with specified grid dimension and world dimension.
#
#    .. cmake_param:: ALBERTA_GRID
#       :option:
#
#       Enables AlbertaGrid with specified grid dimension and world dimension.
#
#    This file defines the a function for adding executables to cmake and setting all required
#    parameters for dependent modules.
#

function(add_amdis_executable)
  include(CMakeParseArguments)
  set(OPTIONS ALU_GRID ALBERTA_GRID)
  set(SINGLEARGS NAME DIM DOW)
  set(MULTIARGS SOURCES)
  cmake_parse_arguments(ADDEXE "${OPTIONS}" "${SINGLEARGS}" "${MULTIARGS}" ${ARGN})

  # Check whether the parser produced any errors
  if(ADDTEST_UNPARSED_ARGUMENTS)
    message(WARNING "Unrecognized arguments ('${ADDTEST_UNPARSED_ARGUMENTS}') for add_amdis_executable!")
  endif()

  # Check input for validity and apply defaults
  if(NOT ADDEXE_SOURCES)
    message(FATAL_ERROR "You need to specify the SOURCES option for add_amdis_executable!")
  endif()
  if(NOT ADDEXE_NAME)
    # try deducing the executable name form the source name
    if(ADDEXE_SOURCES)
      # deducing a name is only possible with a single source argument
      list(LENGTH ADDEXE_SOURCES len)
      if(NOT len STREQUAL "1")
        message(FATAL_ERROR "Cannot deduce executable name from multiple sources!")
      endif()
      # strip file extension
      get_filename_component(ADDEXE_NAME ${ADDEXE_SOURCES} NAME_WE)
    endif()
  endif()

  # Add the executable
  add_executable(${ADDEXE_NAME} ${ADDEXE_SOURCES})
  # add all flags to the target!
  add_dune_all_flags(${ADDEXE_NAME})
  target_link_dune_default_libraries(${ADDEXE_NAME})
  dune_target_link_libraries(${ADDEXE_NAME} amdis)

  if(ADDEXE_DIM)
    set(GRIDDIM ${ADDEXE_DIM})
  endif(ADDEXE_DIM)

  if(ADDEXE_DOW)
    set(WORLDDIM ${ADDEXE_DOW})
  else(ADDEXE_DOW)
    set(WORLDDIM ${ADDEXE_DIM})
  endif(ADDEXE_DOW)

  # set dimension flag
  if(GRIDDIM)
    target_compile_definitions(${ADDEXE_NAME} PRIVATE "GRIDDIM=${GRIDDIM}")
  endif(GRIDDIM)
  if(WORLDDIM)
    target_compile_definitions(${ADDEXE_NAME} PRIVATE "WORLDDIM=${WORLDDIM}")
  endif(WORLDDIM)

  # add flags for AlbertaGrid
  if(ADDEXE_ALBERTA_GRID)
    if (NOT ALBERTA_FOUND)
      message(WARNING "Alberta package required in order to set ALBERTA_GRID flag.")
    endif ()
    if(NOT ADDEXE_DIM)
      message(FATAL_ERROR "You need to specify dimension DIM for ALBERTA_GRID!")
    endif()
    if(NOT ADDEXE_DOW)
      message(WARNING "dimensionworld DOW not specified for ALBERTA_GRID. Setting DOW=DIM.")
    endif()

    add_dune_alberta_flags(GRIDDIM ${GRIDDIM} WORLDDIM ${WORLDDIM} ${ADDEXE_NAME})
  endif(ADDEXE_ALBERTA_GRID)

  # add flags for ALUGrid
  if(ADDEXE_ALU_GRID)
    if (NOT dune-alugrid_FOUND)
      message(WARNING "dune-alugrid package required in order to set ALU_GRID flag.")
    endif ()
  endif(ADDEXE_ALU_GRID)
endfunction(add_amdis_executable)
