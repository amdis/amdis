include(AmdisCXXFeatures)
include(AddAmdisExecutable)

# some optimization of the compile times
find_program(CCACHE_PROGRAM ccache)
if (ENABLE_CCACHE AND CCACHE_PROGRAM)
  set(CMAKE_CXX_COMPILER_LAUNCHER "${CCACHE_PROGRAM}")
  if("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    add_compile_options("-fdiagnostics-color")
  endif()
endif ()

if (NOT BACKEND)
  set(BACKEND "ISTL" CACHE STRING "LinearAlgebra backend. One of MTL, EIGEN, PETSC, ISTL")
  set_property(CACHE BACKEND PROPERTY STRINGS "MTL" "EIGEN" "PETSC" "ISTL")
endif (NOT BACKEND)

if (BACKEND STREQUAL "ISTL")
  set(AMDIS_BACKEND 1)
  if (NOT dune-istl_FOUND)
    message(FATAL_ERROR "Need dune-istl for backend ISTL. Either install the dune-istl module, or change the BACKEND!")
  endif ()

elseif (BACKEND STREQUAL "MTL" OR BACKEND STREQUAL "MTL4")
  find_package(MTL REQUIRED)
  set(AMDIS_HAS_MTL TRUE)
  set(AMDIS_BACKEND 2)

  find_package(HYPRE)
  set(AMDIS_HAS_HYPRE ${HYPRE_FOUND})

elseif (BACKEND STREQUAL "EIGEN" OR BACKEND STREQUAL "EIGEN3")
  find_package(Eigen3 REQUIRED 3.3.5)
  set(AMDIS_HAS_EIGEN TRUE)
  set(AMDIS_BACKEND 3)
  message(STATUS "  Found Eigen3, version: ${Eigen3_VERSION}")

elseif (BACKEND STREQUAL "PETSC")
  find_package(PETSc REQUIRED)
  set(AMDIS_HAS_PETSC TRUE)
  set(AMDIS_BACKEND 4)

else ()
  message(FATAL_ERROR "BACKEND must be one of MTL, EIGEN, PETSC, ISTL, ISTL_BLOCKED")
endif ()
