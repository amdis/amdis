# FindPETSc.cmake
#
# Finds the PETSc library
#
# This will define the following variables
#
#    PETSc_FOUND
#    PETSc_VERSION
#
# and the following imported targets
#
#    PETSc::PETSc
#
# Author: Simon Praetorius <simon.praetorius@tu-dresden.de>

find_package(PkgConfig)

if (NOT PkgConfig_FOUND)
  message(FATAL_ERROR "Can not find PkgConfig!")
endif()

mark_as_advanced(PETSc_FOUND PETSc_VERSION PETSC_PKG_CONFIG)

find_path(PETSC_PKG_CONFIG "PETSc.pc"
  HINTS
    ${PETSC_DIR}
    ${PETSC_ROOT}
    ENV PETSC_DIR
    ENV PETSC_ROOT
    ENV PKG_CONFIG_PATH
  PATHS
    /etc/alternatives
    /usr/lib/petsc
    /usr/lib/petsc/linux-gnu-cxx-opt
    /usr/lib/petsc/linux-gnu-c-opt
  PATH_SUFFIXES lib/pkgconfig/
)

if (PETSC_PKG_CONFIG)
  set(ENV{PKG_CONFIG_PATH} "${PETSC_PKG_CONFIG}:$ENV{PKG_CONFIG_PATH}")
endif (PETSC_PKG_CONFIG)

if (PETSc_FIND_VERSION)
  pkg_check_modules(PETSC PETSc>=${PETSc_FIND_VERSION})
else ()
  pkg_check_modules(PETSC PETSc)
endif ()

if (PETSC_STATIC_FOUND)
  set(PETSc_VERSION "${PETSC_STATIC_VERSION}")
elseif (PETSC_FOUND)
  set(PETSc_VERSION "${PETSC_VERSION}")
endif ()

if ((PETSC_STATIC_FOUND OR PETSC_FOUND) AND NOT TARGET PETSc::PETSc)
  add_library(PETSc::PETSc INTERFACE IMPORTED GLOBAL)
  if (PETSC_INCLUDE_DIRS)
    set_property(TARGET PETSc::PETSc PROPERTY INTERFACE_INCLUDE_DIRECTORIES "${PETSC_INCLUDE_DIRS}")
  endif ()
  include(PkgConfigLinkLibraries)
  if (PETSC_LINK_LIBRARIES)
    set_property(TARGET PETSc::PETSc PROPERTY INTERFACE_LINK_LIBRARIES "${PETSC_LINK_LIBRARIES}")
  else ()
    # extract the absolute paths of link libraries from the LDFLAGS
    pkg_config_link_libraries(PETSC _libs)
    set_property(TARGET PETSc::PETSc PROPERTY INTERFACE_LINK_LIBRARIES "${_libs}")
    unset(_libs)
  endif ()
  if (PETSC_STATIC_LINK_LIBRARIES)
    set_property(TARGET PETSc::PETSc PROPERTY INTERFACE_LINK_LIBRARIES "${PETSC_STATIC_LINK_LIBRARIES}")
  else ()
    # extract the absolute paths of link libraries from the LDFLAGS
    pkg_config_link_libraries(PETSC_STATIC _static_libs)
    set_property(TARGET PETSc::PETSc PROPERTY INTERFACE_LINK_LIBRARIES "${_static_libs}")
    unset(_static_libs)
  endif ()
  if (PETSC_LDFLAGS_OTHER)
    set_property(TARGET PETSc::PETSc PROPERTY INTERFACE_LINK_OPTIONS "${PETSC_LDFLAGS_OTHER}")
  endif ()
  if (PETSC_STATIC_LDFLAGS_OTHER)
    set_property(TARGET PETSc::PETSc PROPERTY INTERFACE_LINK_OPTIONS "${PETSC_STATIC_LDFLAGS_OTHER}")
  endif ()
  if (PETSC_CFLAGS_OTHER)
    set_property(TARGET PETSc::PETSc PROPERTY INTERFACE_COMPILE_OPTIONS "${PETSC_CFLAGS_OTHER}")
  endif ()
  # workaround for PETSc macros redefining MPI functions
  set_property(TARGET PETSc::PETSc PROPERTY INTERFACE_COMPILE_DEFINITIONS "PETSC_HAVE_BROKEN_RECURSIVE_MACRO=1")
endif ()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(PETSc
  REQUIRED_VARS PETSc_VERSION
  VERSION_VAR PETSc_VERSION
)

# text for feature summary
set_package_properties("PETSc" PROPERTIES
  DESCRIPTION "Portable, Extensible Toolkit for Scientific Computation")