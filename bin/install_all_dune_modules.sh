#!/usr/bin/env bash

USAGE_HELP="usage: $0 --prefix DUNE_BASE_DIR [--branch DUNE_BRANCH] [--ssh]"

PREFIX="-"
BRANCH="releases/2.8"
NUM_PROCS=$(nproc --all)
USE_SSH="0"

while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    --prefix)
    PREFIX="$2"
    shift # past argument
    shift # past value
    ;;
    -b|--branch)
    BRANCH="$2"
    shift # past argument
    shift # past value
    ;;
    -n|--np)
    NUM_PROCS="$2"
    shift # past argument
    shift # past value
    ;;
    --ssh)
    USE_SSH="1"
    shift
    ;;
    *)    # unknown option
    echo $USAGE_HELP
    exit 1
    shift # past argument
    ;;
esac
done

if [[ "$PREFIX" == "-" ]]; then
  echo $USAGE_HELP
  exit 1
fi


mkdir -p ${PREFIX}
cd ${PREFIX}

BASE_URL="https://gitlab.dune-project.org"
if [[ "${USE_SSH}" -eq "1" ]]; then
  BASE_URL="ssh://git@gitlab.dune-project.org:22022"
fi

CORE_MODULES=("dune-common" "dune-geometry" "dune-grid" "dune-istl" "dune-localfunctions")
STAGING_MODULES=("dune-functions" "dune-typetree" "dune-uggrid")
EXTENSIONS_MODULES=("dune-alugrid" "dune-foamgrid" "dune-vtk")

# download all modules by git
for MOD in ${CORE_MODULES[*]}; do
  git clone ${BASE_URL}/core/${MOD}.git
  (cd $MOD && git checkout -q ${BRANCH})
done

for MOD in ${STAGING_MODULES[*]}; do
  git clone ${BASE_URL}/staging/${MOD}.git
  (cd $MOD && git checkout -q ${BRANCH})
done

for MOD in ${EXTENSIONS_MODULES[*]}; do
  git clone ${BASE_URL}/extensions/${MOD}.git
  (cd $MOD && git checkout -q ${BRANCH})
done


# create options files
cat << EOF >> ${PREFIX}/release.opts
CMAKE_FLAGS="-DCMAKE_BUILD_TYPE=Release -DCMAKE_CXX_FLAGS=\"-march=native\""
BUILDDIR="build-release"
MAKE_FLAGS="-j${NUM_PROCS}"
EOF

cat << EOF >> ${PREFIX}/debug.opts
CMAKE_FLAGS="-DCMAKE_BUILD_TYPE=Debug -DCMAKE_CXX_FLAGS_DEBUG=\"-g -Og\" -DCMAKE_CXX_FLAGS=\"-march=native -DAMDIS_INFO_LEVEL=2 -DDUNE_ISTL_WITH_CHECKING=1 -DDUNE_CHECK_BOUNDS=1 -DDUNE_FMatrix_WITH_CHECKING=1\""
BUILDDIR="build-debug"
MAKE_FLAGS="-j${NUM_PROCS}"
EOF

echo ""
echo "=============================================================="
echo "You can add the following lines to you .bashrc file           "
echo "  export DUNE_CONTROL_PATH=${PREFIX}                          "
echo "  export PATH=${PREFIX}/dune-common/bin:\${PATH}              "
echo "                                                              "
echo "In order to build all modules, run inside the ${PREFIX} dir   "
echo "  duncontrol --opts=${PREFIX}/release.opts all                "
echo "=============================================================="
