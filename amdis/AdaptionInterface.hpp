#pragma once

#include <amdis/common/ConceptsBase.hpp>

namespace AMDiS
{
  /**
   * \addtogroup Concepts
   * @{
   **/

  namespace Concepts
  {
    namespace Definition
    {
      struct InterpolateData
      {
        template <class Data>
        auto require(Data const& data) -> decltype(
            const_cast<Data&>(data).preAdapt(true),
            const_cast<Data&>(data).postAdapt(true)
          );
      };

      struct UpdateData
      {
        template <class Basis>
        auto require(Basis const& basis) -> decltype(
            const_cast<Basis&>(basis).update(basis.gridView())
          );
      };

    } // end namespace Definition

    template <class Data>
    constexpr bool InterpolateData = models<Definition::InterpolateData(Data)>;

    template <class Data>
    using InterpolateData_t = models_t<Definition::InterpolateData(Data)>;


    template <class Basis>
    constexpr bool UpdateData = models<Definition::UpdateData(Basis)>;

    template <class Basis>
    using UpdateData_t = models_t<Definition::UpdateData(Basis)>;

  } // end namespace Concepts

  /// @}


  /**
   * \addtogroup Adaption
   * @{
   **/

  /// \brief Interface for transfer between grid changes
  class AdaptionInterface
  {
  public:
    virtual ~AdaptionInterface() = default;

    /// Prepare the grid and the data for the adaption
    virtual bool preAdapt() = 0;

    /// Do the grid adaption
    virtual bool adapt() = 0;

    // Perform data adaption to the new grid
    virtual void postAdapt() = 0;
  };

  /// @}

} // end namespace AMDiS
