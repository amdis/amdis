#pragma once

#include <utility>

#include <amdis/ContextGeometry.hpp>
#include <amdis/GridFunctionOperator.hpp>
#include <amdis/LocalOperator.hpp>
#include <amdis/OperatorAdapter.hpp>
#include <amdis/functions/LocalViewPair.hpp>
#include <amdis/typetree/Traversal.hpp>

namespace AMDiS {

template <class RB, class CB, class T, class Traits>
  template <class ContextTag, class Expr, class RowTreePath, class ColTreePath>
void BiLinearForm<RB,CB,T,Traits>::
addOperator(ContextTag contextTag, Expr const& expr,
            RowTreePath row, ColTreePath col)
{
  static_assert( Concepts::PreTreePath<RowTreePath>,
      "row must be a valid treepath, or an integer/index-constant");
  static_assert( Concepts::PreTreePath<ColTreePath>,
      "col must be a valid treepath, or an integer/index-constant");

  using LocalContext = typename Impl::ContextTagType<ContextTag>::type;
  auto op = makeOperator<LocalContext>(expr, this->rowBasis().gridView());
  if constexpr(std::is_same_v<RowTreePath,RootTreePath> && std::is_same_v<ColTreePath,RootTreePath>)
    assembler_.push(contextTag, std::move(op));
  else
    assembler_.push(contextTag, OperatorAdapter{std::move(op),makeTreePath(row),makeTreePath(col)});
  updatePattern_ = true;
}


template <class RB, class CB, class T, class Traits>
  template <class RowLocalView, class ColLocalView,
    REQUIRES_(Concepts::LocalView<RowLocalView>),
    REQUIRES_(Concepts::LocalView<ColLocalView>)>
void BiLinearForm<RB,CB,T,Traits>::
assemble(RowLocalView const& rowLocalView, ColLocalView const& colLocalView)
{
  assert(rowLocalView.bound());
  assert(colLocalView.bound());

  elementMatrix_.resize(rowLocalView.size(), colLocalView.size());
  elementMatrix_ = 0;

  auto const& gv = this->rowBasis().gridView();
  auto const& element = rowLocalView.element();
  GlobalContext<TYPEOF(gv)> context{gv, element, element.geometry()};

  auto matOp = localAssembler(assembler_);
  matOp.bind(element);
  matOp.assemble(context, rowLocalView.treeCache(), colLocalView.treeCache(), elementMatrix_);
  matOp.unbind();

  this->scatter(rowLocalView, colLocalView, elementMatrix_);
}


template <class RB, class CB, class T, class Traits>
void BiLinearForm<RB,CB,T,Traits>::
assemble()
{
  LocalViewPair<RB,CB> localViews{this->rowBasis(), this->colBasis()};

  this->init();
  for (auto const& element : entitySet(this->rowBasis())) {
    localViews.bind(element);
    this->assemble(localViews.row(), localViews.col());
    localViews.unbind();
  }
  this->finish();
}


} // end namespace AMDiS
