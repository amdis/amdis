#include <config.h>

#include "InitfileParser.hpp"

#include <amdis/Output.hpp>
#include <amdis/common/Filesystem.hpp>
#include <amdis/common/String.hpp>

namespace AMDiS {

void InitfileParser::readInitfile(std::string const& fn, Dune::ParameterTree& pt, bool overwrite)
{
  test_exit(filesystem::exists(fn), "Init-file '{}' cannot be opened for reading", fn);

  // read file if its not parsed already
  auto ins = includeList().insert(fn);
  if (ins.second) {
    std::ifstream in(fn.c_str());
    readInitfile(in, pt, overwrite);
  }
}


void InitfileParser::readInitfile(std::istream& in, Dune::ParameterTree& pt, bool overwrite)
{
  std::string swap;
  std::getline(in, swap);
  while (in.good() || swap.size() > 0) {
    std::string whitespaces = " \t\r\f\n";
    std::string delimiter = "\r\n";
    std::string sw(swap);
    std::size_t pos0 = sw.find_first_not_of(whitespaces);

    if (pos0 != std::string::npos
        && sw[pos0] != '%'
        && sw[pos0] != '#'
        && sw[pos0] != 0)
    {
      // parse line and extract map: tag->value
      std::size_t pos = sw.find(':');
      test_exit(pos != std::string::npos, "Cannot find the delimiter ':' in line '{}'", sw);

      std::string name = sw.substr(0, pos);
      std::string value = sw.substr(pos + 1, sw.length() - (pos + 1));

      // remove everything after the %
      pos = value.find('%');
      if (pos != std::string::npos)
        value = value.substr(0, pos);

      // add parameter to map after variable replacement
      std::string paramName = replaceVariable(pt, trim(name));
      std::string paramValue = replaceVariable(pt, trim(value));
      paramValue = replaceExpression(pt, paramValue);

      // add parameter to parametertree
      if (overwrite || ! pt.hasKey(paramName))
        pt[paramName] = paramValue;
    }
    else if (pos0 != std::string::npos &&
             sw[pos0] == '#'  &&
             std::size_t(sw.find("#include")) == pos0)
    {
      // include file by '#include "filename"' or '#include <filename>'
      std::size_t pos = sw.find_first_not_of(whitespaces, pos0 + std::string("#include").size() + 1);
      std::size_t epos = 0;
      std::string fn =  "";
      std::map<char,char> closing = { {'<','>'}, {'\"', '\"'} };

      switch (char c = swap[pos++]) {
        case '<':
        case '\"':
          delimiter += closing[c];
          epos = sw.find_first_of(delimiter, pos);
          fn = sw.substr(pos, epos - pos);

          test_exit(sw[epos]==closing[c], "Filename in #include not terminated by {}.", closing[c]);
          break;
        default:
          error_exit("No filename given for #include");
      }

      readInitfile(fn, pt, overwrite);
    }

    swap.clear();
    std::getline(in, swap);
  }
}


std::string InitfileParser::replaceVariable(Dune::ParameterTree const& pt, std::string const& input)
{
  std::string whitespaces = " \t\r\f";
  std::string allowedChars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_";
  std::string inputSwap = input;
  std::size_t posVar = inputSwap.find_first_of('$');
  while (posVar != std::string::npos) {
    std::size_t posVarBegin, posVarEnd;
    if (inputSwap[posVar+1] == '{') {						// ${var_name}
      posVarEnd = inputSwap.find_first_of('}',posVar + 2);
      posVarBegin = posVar + 1;
    } else if (inputSwap[posVar+1] != '(' && inputSwap[posVar+1] != '[') { // $var_name
      posVarEnd = inputSwap.find_first_not_of(allowedChars, posVar + 1);
      posVarBegin = posVar;
    } else {
      posVar = inputSwap.find_first_of('$',posVar+1);
      continue;
    }

    std::string varName = inputSwap.substr(posVarBegin + 1 , posVarEnd - posVarBegin - 1);

    // if varname is found in parameter list then replace variable by value
    // otherwise throw tagNotFound exception
    test_exit(pt.hasKey(varName),
      "Required tag '{}' for variable substitution not found", varName);

    std::string varParam = pt[varName];

    std::string replaceName = inputSwap.substr(posVar , posVarEnd - posVar + (posVarBegin - posVar));
    inputSwap.replace(inputSwap.find(replaceName), replaceName.length(), varParam);

    posVar = inputSwap.find_first_of('$',posVarBegin);
  }

  return inputSwap;
}


std::string InitfileParser::replaceExpression(Dune::ParameterTree const& /*pt*/, std::string const& input)
{
#if 0
  std::string whitespaces = " \t\r\f";
  std::string inputSwap = input;
  std::size_t posVar = inputSwap.find("$(");
  while (posVar != std::string::npos) {
    size_t posVarBegin, posVarEnd;
    posVarEnd = inputSwap.find_first_of(')',posVar + 2);
    posVarBegin = posVar + 1;
    std::string varName = inputSwap.substr(posVarBegin + 1 , posVarEnd - posVarBegin - 1);

    double value = 0.0;
    detail::convert(varName, value); // string -> double (using muparser)
    detail::convert(value, varName); // double -> string

    std::string replaceName = inputSwap.substr(posVar , posVarEnd - posVar + (posVarBegin - posVar));
    inputSwap.replace(inputSwap.find(replaceName), replaceName.length(), varName);

    posVar = inputSwap.find("$(",posVarBegin);
  }

  return inputSwap;
#else

  // currently no evaluation implemented.
  return input;
#endif
}

} // end namespace AMDiS
