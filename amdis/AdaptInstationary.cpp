#include <config.h>

#include "AdaptInstationary.hpp"

// AMDiS includes
#include <amdis/AdaptInfo.hpp>
#include <amdis/Flag.hpp>
#include <amdis/Initfile.hpp>
#include <amdis/Output.hpp>
#include <amdis/ProblemIterationInterface.hpp>
#include <amdis/ProblemTimeInterface.hpp>

namespace AMDiS
{
  AdaptInstationary::AdaptInstationary(std::string const& name,
                                       ProblemIterationInterface& problemIteration,
                                       AdaptInfo& adaptInfo,
                                       ProblemTimeInterface& problemTime,
                                       AdaptInfo& initialAdaptInfo)
    : AdaptBase(name, &problemIteration, adaptInfo, &problemTime, &initialAdaptInfo)
  {
    Parameters::get(name_ + "->strategy", strategy_);
    Parameters::get(name_ + "->time delta 1", timeDelta1_);
    Parameters::get(name_ + "->time delta 2", timeDelta2_);
    Parameters::get(name_ + "->break when stable", breakWhenStable_);

    fixedTimestep_ = (adaptInfo_.minTimestep() == adaptInfo_.maxTimestep());
  }


  void AdaptInstationary::explicitTimeStrategy()
  {
    AMDIS_FUNCNAME("AdaptInstationary::explicitTimeStrategy()");

    // estimate before first adaption
    if (adaptInfo_.time() <= adaptInfo_.startTime())
      problemIteration_->oneIteration(adaptInfo_, ESTIMATE);


    // increment time
    adaptInfo_.setTime(adaptInfo_.time() + adaptInfo_.timestep());

    problemTime_->setTime(adaptInfo_);

    msg("time = {}, timestep = {}", adaptInfo_.time(), adaptInfo_.timestep());

    adaptInfo_.setSpaceIteration(0);

    // do the iteration
    problemIteration_->beginIteration(adaptInfo_);
    problemIteration_->oneIteration(adaptInfo_, FULL_ITERATION);
    problemIteration_->endIteration(adaptInfo_);
    adaptInfo_.setLastProcessedTimestep(adaptInfo_.timestep());
  }


  void AdaptInstationary::implicitTimeStrategy()
  {
    AMDIS_FUNCNAME("AdaptInstationary::implicitTimeStrategy()");

    do
    {
      adaptInfo_.setTime(adaptInfo_.time() + adaptInfo_.timestep());
      problemTime_->setTime(adaptInfo_);

      msg("time = {}, timestep = {}", adaptInfo_.time(), adaptInfo_.timestep());

      problemIteration_->oneIteration(adaptInfo_, NO_ADAPTION);

      adaptInfo_.incTimestepIteration();

      if (!fixedTimestep_ &&
          !adaptInfo_.timeToleranceReached() &&
          adaptInfo_.timestepIteration() <= adaptInfo_.maxTimestepIteration() &&
          !(adaptInfo_.timestep() <= adaptInfo_.minTimestep()))
      {
        adaptInfo_.setTime(adaptInfo_.time() - adaptInfo_.timestep());
        adaptInfo_.setTimestep(adaptInfo_.timestep() * timeDelta1_);
        continue;
      }


      adaptInfo_.setSpaceIteration(0);


      // === Do space iterations only if the maximum is higher than 0. ===

      if (adaptInfo_.maxSpaceIteration() > 0)
      {

        // === Space iterations. ===
        do
        {
          problemIteration_->beginIteration(adaptInfo_);

          Flag adapted = problemIteration_->oneIteration(adaptInfo_, FULL_ITERATION);
          if (adapted == Flag{0})
          {
            if (!fixedTimestep_ &&
                !adaptInfo_.timeToleranceReached() &&
                !(adaptInfo_.timestep() <= adaptInfo_.minTimestep()))
            {
              adaptInfo_.setTime(adaptInfo_.time() - adaptInfo_.timestep());
              adaptInfo_.setTimestep(adaptInfo_.timestep() * timeDelta2_);
              problemIteration_->endIteration(adaptInfo_);
              adaptInfo_.incSpaceIteration();
              break;
            }
          }

          adaptInfo_.incSpaceIteration();
          problemIteration_->endIteration(adaptInfo_);

        }
        while (!adaptInfo_.spaceToleranceReached() &&
               adaptInfo_.spaceIteration() <= adaptInfo_.maxSpaceIteration());

      }
      else
      {
        problemIteration_->endIteration(adaptInfo_);
      }


    }
    while(!adaptInfo_.timeToleranceReached() &&
          !(adaptInfo_.timestep() <= adaptInfo_.minTimestep()) &&
          adaptInfo_.timestepIteration() <= adaptInfo_.maxTimestepIteration());

    adaptInfo_.setLastProcessedTimestep(adaptInfo_.timestep());

    // After successful iteration/timestep the timestep will be changed according
    // adaption rules for next timestep.
    // First, check for increase of timestep
    if (!fixedTimestep_ && adaptInfo_.timeErrorLow())
      adaptInfo_.setTimestep(adaptInfo_.timestep() * timeDelta2_);

    // Second, check for decrease of timestep
    if (!fixedTimestep_ &&
        !adaptInfo_.timeToleranceReached() &&
        !(adaptInfo_.timestep() <= adaptInfo_.minTimestep()))
      adaptInfo_.setTimestep(adaptInfo_.timestep() * timeDelta1_);
  }


  void AdaptInstationary::simpleAdaptiveTimeStrategy()
  {
    AMDIS_FUNCNAME("AdaptInstationary::simpleAdaptiveTimeStrategy()");

    // estimate before first adaption
    if (adaptInfo_.time() <= adaptInfo_.startTime())
      problemIteration_->oneIteration(adaptInfo_, ESTIMATE);

    adaptInfo_.setTime(adaptInfo_.time() + adaptInfo_.timestep());
    problemTime_->setTime(adaptInfo_);

    msg("time = {}, timestep = {}", adaptInfo_.time(), adaptInfo_.timestep());

    problemIteration_->oneIteration(adaptInfo_, FULL_ITERATION);

    adaptInfo_.setLastProcessedTimestep(adaptInfo_.timestep());

    // First, check for increase of timestep
    if (!fixedTimestep_ && adaptInfo_.timeErrorLow())
      adaptInfo_.setTimestep(adaptInfo_.timestep() * timeDelta2_);

    // Second, check for decrease of timestep
    if (!fixedTimestep_ &&
        !adaptInfo_.timeToleranceReached() &&
        !(adaptInfo_.timestep() <= adaptInfo_.minTimestep()))
      adaptInfo_.setTimestep(adaptInfo_.timestep() * timeDelta1_);
  }


  void AdaptInstationary::oneTimestep()
  {
    AMDIS_FUNCNAME("AdaptInstationary::oneTimestep()");

    adaptInfo_.setTimestepIteration(0);

    switch (strategy_)
    {
    case 0:
      explicitTimeStrategy();
      break;
    case 1:
      implicitTimeStrategy();
      break;
    case 2:
      simpleAdaptiveTimeStrategy();
      break;
    default:
      error_exit("Unknown strategy = {}", strategy_);
    }

    adaptInfo_.incTimestepNumber();
  }


  int AdaptInstationary::adapt()
  {
    AMDIS_FUNCNAME("AdaptInstationary::adapt()");
    int errorCode = 0;

    test_exit(adaptInfo_.timestep() >= adaptInfo_.minTimestep(),
              "timestep < min timestep");
    test_exit(adaptInfo_.timestep() <= adaptInfo_.maxTimestep(),
              "timestep > max timestep");

    test_exit(adaptInfo_.timestep() > 0, "timestep <= 0!");

    if (adaptInfo_.timestepNumber() == 0)
    {
      adaptInfo_.setTime(adaptInfo_.startTime());
      initialAdaptInfo_->setStartTime(adaptInfo_.startTime());
      initialAdaptInfo_->setTime(adaptInfo_.startTime());

      problemTime_->setTime(adaptInfo_);

      // initial adaption
      problemTime_->solveInitialProblem(*initialAdaptInfo_);
      problemTime_->transferInitialSolution(adaptInfo_);
    }

    while (!adaptInfo_.reachedEndTime())
    {
      problemTime_->initTimestep(adaptInfo_);
      oneTimestep();
      problemTime_->closeTimestep(adaptInfo_);

      if (breakWhenStable_ && (adaptInfo_.solverIterations() == 0))
        break;
    }

    return errorCode;
  }

} // end namespace AMDiS
