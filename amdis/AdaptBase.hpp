#pragma once

// std c++ headers
#include <string>

namespace AMDiS
{
  // forward declarations
  class AdaptInfo;
  class ProblemIterationInterface;
  class ProblemTimeInterface;

  /// Interface for adaption loops.
  class AdaptBase
  {
  public:
    /// Constructor
    AdaptBase(std::string const& name,
              ProblemIterationInterface* problemIteration,
              AdaptInfo& adaptInfo,
              ProblemTimeInterface* problemTime = nullptr,
              AdaptInfo* initialAdaptInfo = nullptr)
      : name_(name)
      , problemIteration_(problemIteration)
      , adaptInfo_(adaptInfo)
      , problemTime_(problemTime)
      , initialAdaptInfo_(initialAdaptInfo)
    {}

    /// Destructor
    virtual ~AdaptBase() = default;

    /** \brief
     * Pure virtual method. Must be overloaded by sub classes to perform
     * a concrete adaption loop.
     */
    virtual int adapt() = 0;

    /// Returns \ref name
    std::string const& name() const
    {
      return name_;
    }

    /// Returns \ref problemIteration
    ProblemIterationInterface* problemIteration() const
    {
      return problemIteration_;
    }

    ///
    void setProblemIteration(ProblemIterationInterface* problemIteration)
    {
      problemIteration_ = problemIteration;
    }

    /// Returns \ref adaptInfo
    AdaptInfo& adaptInfo() const
    {
      return adaptInfo_;
    }

    /// Returns \ref problemTime
    ProblemTimeInterface* problemTime() const
    {
      return problemTime_;
    }

    ///
    void setProblemTime(ProblemTimeInterface* problemTime)
    {
      problemTime_ = problemTime;
    }

    /// Returns \ref initialAdaptInfo
    AdaptInfo& initialAdaptInfo() const
    {
      return *initialAdaptInfo_;
    }

  protected:
    /// Name of the adaption loop
    std::string name_;

    /// Problem iteration interface
    ProblemIterationInterface* problemIteration_;

    /// Main adapt info
    AdaptInfo& adaptInfo_;

    /// problem time interface
    ProblemTimeInterface* problemTime_;

    /** \brief
     * Adapt info for initial adapt. Will be given to
     * problemTime->solveInitialProblem().
     */
    AdaptInfo* initialAdaptInfo_;
  };

} // end namespace AMDiS
