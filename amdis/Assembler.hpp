#pragma once

#include <list>
#include <memory>

#include <amdis/ContextGeometry.hpp>
#include <amdis/BoundaryOperator.hpp>
#include <amdis/Operator.hpp>
#include <amdis/algorithm/Map.hpp>

namespace AMDiS
{
  namespace tag
  {
    template <class E> struct element_operator { using type = E; };
    template <class I> struct intersection_operator { using type = I; };
  }

  template <class GridView, class Container, class... Nodes>
  class LocalAssembler
  {
    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ElementTraits = Impl::OperatorTraits<GridView,Element,Container>;
    using IntersectionTraits = Impl::OperatorTraits<GridView,Intersection,Container>;

    /// The type of local operators associated with grid elements
    using ElementLocalOperator = LocalOperator<ElementTraits, Nodes...>;

    /// The type of local operators associated with grid intersections
    using IntersectionLocalOperator = LocalOperator<IntersectionTraits, Nodes...>;

  public:
    /// List of operators to be assembled on grid elements
    LocalAssembler(std::vector<ElementLocalOperator> element,
                   std::vector<IntersectionLocalOperator> intersection,
                   std::vector<BoundaryLocalOperator<IntersectionLocalOperator,Intersection>> boundary)
      : element_(std::move(element))
      , intersection_(std::move(intersection))
      , boundary_(std::move(boundary))
    {}

    /// Return whether any operators are stored on the node
    bool empty() const
    {
      return element_.empty() && boundary_.empty() && intersection_.empty();
    }

    /// Bind all operators to the grid element and geometry
    void bind(Element const& elem)
    {
      for (auto& lop : element_) lop.bind(elem);
      for (auto& lop : intersection_) lop.bind(elem);
      for (auto& lop : boundary_) lop.bind(elem);
    }

    /// Unbind all operators from the element
    void unbind()
    {
      for (auto& lop : element_) lop.unbind();
      for (auto& lop : intersection_) lop.unbind();
      for (auto& lop : boundary_) lop.unbind();
      assembled_ = true;
    }

    /// Assemble all operators on an element
    void assemble(GlobalContext<GridView> const& context, Nodes const&... nodes,
                  Container& matVec) const
    {
      // do not assemble in case nothing to do
      if ((assembled_ && !changing_) || empty())
        return;

      // create a context for the element
      ContextGeometry elementContext{context.element(), context.element(), context.geometry()};

      // assemble element operators
      for (auto const& lop : element_)
        lop.assemble(elementContext, nodes..., matVec);

      if (intersection_.empty() && (boundary_.empty() || !context.element().hasBoundaryIntersections()))
        return;

      // assemble intersection operators
      for (auto const& is : intersections(context.gridView(), context.element()))
      {
        // create a context for the intersection
        ContextGeometry intersectionContext{is, context.element(), context.geometry()};

        if (is.boundary()) {
          // assemble boundary operators
          for (auto& lop : boundary_) {
            lop.assemble(intersectionContext, nodes..., matVec);
          }
        } else {
          // assemble intersection operators
          for (auto& lop : intersection_)
            lop.assemble(intersectionContext, nodes..., matVec);
        }
      }
    }

  private:
    /// List of operators to be assembled on grid elements
    std::vector<ElementLocalOperator> element_;

    /// List of operators to be assembled on interior intersections
    std::vector<IntersectionLocalOperator> intersection_;

    /// List of operators to be assembled on boundary intersections
    std::vector<BoundaryLocalOperator<IntersectionLocalOperator,Intersection>> boundary_;

    /// if false, do reassemble of all operators
    bool assembled_ = false;

    /// if true, or \ref assembled false, do reassemble of all operators
    bool changing_ = true;
  };


  /**
   * \brief An Assembler is the main driver for building the local element matrices and vectors
   * by assembling operators bound to elements, intersections and boundary intersection.
   *
   * \tparam GridView    Type of the grid view the assembler is running on.
   * \tparam Container   The element-matrix or element-vector type.
   * \tparam Nodes...    Tree node types (either row and column node, or just row node)
   */
  template <class GridView, class Container, class... Nodes>
  class Assembler
  {
    using Element = typename GridView::template Codim<0>::Entity;
    using Intersection = typename GridView::Intersection;

    using ElementTraits = Impl::OperatorTraits<GridView,Element,Container>;
    using IntersectionTraits = Impl::OperatorTraits<GridView,Intersection,Container>;

    /// The type of local operators associated with grid elements
    using ElementOperator = Operator<ElementTraits, Nodes...>;

    /// The type of local operators associated with grid intersections
    using IntersectionOperator = Operator<IntersectionTraits, Nodes...>;

  public:
    Assembler () = default;

    void update(GridView const& gridView)
    {
      for (auto& e : element_)
        e.update(gridView);
      for (auto& is : intersection_)
        is.update(gridView);
      for (auto& b : boundary_)
        b.update(gridView);
    }

    template <class Op>
    void push(tag::element_operator<Element>, Op&& op)
    {
      element_.emplace_back(FWD(op));
    }

    template <class Op>
    void push(tag::intersection_operator<Intersection>, Op&& op)
    {
      intersection_.emplace_back(FWD(op));
    }

    template <class Op>
    void push(BoundarySubset<Intersection> b, Op&& op)
    {
      boundary_.push_back({FWD(op), std::move(b)});
    }

    friend LocalAssembler<GridView,Container,Nodes...> localAssembler(Assembler const& a)
    {
      auto toLocalOperator = [](auto const& op) { return localOperator(op); };
      return {Recursive::map(toLocalOperator, a.element_),
              Recursive::map(toLocalOperator, a.intersection_),
              Recursive::map(toLocalOperator, a.boundary_)};
    }

  private:
    /// List of operators to be assembled on grid elements
    std::vector<ElementOperator> element_{};

    /// List of operators to be assembled on interior intersections
    std::vector<IntersectionOperator> intersection_{};

    /// List of operators to be assembled on boundary intersections
    std::vector<BoundaryOperator<IntersectionOperator,Intersection>> boundary_{};
  };


  template <class RowBasis, class ColBasis, class ElementMatrix>
  using MatrixAssembler
    = Assembler<typename RowBasis::GridView,ElementMatrix,
        typename RowBasis::LocalView::TreeCache,
        typename ColBasis::LocalView::TreeCache>;

  template <class Basis, class ElementVector>
  using VectorAssembler
    = Assembler<typename Basis::GridView,ElementVector,
        typename Basis::LocalView::TreeCache>;

} // end namespace AMDiS
