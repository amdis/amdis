#pragma once

// std c++ headers
#include <map>
#include <string>

// AMDiS includes
#include <amdis/CreatorInterface.hpp>

namespace AMDiS
{
  // forward declaration.
  // All classes that need creators must specialize this class and implement
  // a static void init() method.
  template <class BaseClass>
  class DefaultCreators;


  /** \ingroup Common
   * \brief
   * A CreatorMap is used to construct objects, which types depends on key words
   * determined at run time. For example the LinearSolverInterfaceMap can create the different
   * solver types depending on the solver parameter of the init file. The benefit
   * of such creator maps is, that you can extend them only by writing an creator
   * class for your own new class and give the creator together with a key word
   * to the map.
   */
  template <class BaseClass>
  class CreatorMap
  {
  public:
    using CreatorMapType = std::map< std::string, CreatorInterface<BaseClass>* >;

  public:
    /// Adds a new creator together with the given key to the map.
    static void addCreator(std::string key, CreatorInterface<BaseClass>* creator)
    {
      init();
      test_exit(!creatorMap[key], "There is already a creator for key {}", key);
      creatorMap[key] = creator;
    }

    /// Creates a object of the type corresponding to key.
    static CreatorInterface<BaseClass>* get(std::string key, std::string initFileStr)
    {
      init();

      auto it = creatorMap.find(key);
      if (it == creatorMap.end()) {
        warning("No creator for key `{}` defined (used in init-file parameter `{}`). Falling back to `default`.", key, initFileStr);
        key = "default";
      }

      auto creator = creatorMap[key];
      test_exit(creator, "Undefined creator for `{}` (used in init-file parameter `{}`)", key, initFileStr);

      return creator;
    }

  protected:
    static void init()
    {
      if (!initialized) {
        initialized = true;
        DefaultCreators<BaseClass>::init();
      }
    }

  protected:
    /// STL map containing the pairs of keys and creators.
    static CreatorMapType creatorMap;

    static bool initialized;
  };

  template <class BaseClass>
  typename CreatorMap<BaseClass>::CreatorMapType CreatorMap<BaseClass>::creatorMap;

  template <class BaseClass>
  bool CreatorMap<BaseClass>::initialized = false;

} // end namespace AMDiS
