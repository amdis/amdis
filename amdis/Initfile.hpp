#pragma once

#include <cassert>
#include <cmath>
#include <optional>
#include <string>
#include <type_traits>
#include <vector>

#include <dune/common/parametertree.hh>
#include <dune/functions/functionspacebases/concepts.hh>
#include <muParser.h>

namespace AMDiS
{
  class Initfile
  {
  public:
    /// initialize singleton object and global parameters
    static void init(std::string const& in);

    /// \brief Get parameter-values from parameter-tree
    /**
     * Looks for the `key` in the parameter-tree and returns
     * the stored and parsed value if found and parsable.
     *
     * May throw an exception if the value can not be parsed into type T
     **/
    template <class T,
            std::enable_if_t<!std::is_arithmetic_v<T>, int> = 0>
    static std::optional<T> get(std::string const& key)
    {
      if (pt().hasKey(key)) {
        if constexpr (is_arithmetic_container<T>::value)
          return getVector<T>(key);
        else
          return pt().get<T>(key);
      }
      else
        return {};
    }

    template <class T,
            std::enable_if_t<std::is_arithmetic_v<T>, int> = 0>
    static std::optional<T> get(std::string const& key) {
      if (pt().hasKey(key)) {
        std::string aux;
        std::string valStr = pt().get(key, aux);
        return parseExpression<T>(valStr,key);
      } else {
        return {};
      }
    }

    /// \brief Get parameter-values from parameter-tree with default value
    /**
     *  initialized in init()-method.
     *  Cast the value to the desired type.
     *
     *  \param key: The tag to look for
     *  \param value: The default value and result.
     **/
    template <class T>
    static void get(std::string const& key, T& value) {
      value = get<T>(key).value_or(value);
    }

    template <class T>
    static void set(std::string const& key, T const& value)
    {
      pt()[key] = value;
    }

    /// Returns specified info level
    static int getMsgInfo()
    {
      return singlett().msgInfo_;
    }

    /// print all data in singleton to std::cout
    static void printParameters();

    static void clearData() {}

  protected:
    Initfile() = default;

    /// Return the parameter-tree
    static Dune::ParameterTree& pt()
    {
      return singlett().pt_;
    }

    /// Fill an parametr-tree from a file with filename fn
    void read(std::string const& fn, bool force = false);
    void write(std::string const& fn);

    /// read standard values for output and information of parameter-values
    void getInternalParameters();

    /// get Parameter values for vector types
    template <class T>
    static std::optional<T> getVector(const std::string& key) {
      if constexpr (std::is_arithmetic_v<typename T::value_type>) {
        T result;
        std::vector<std::string> aux;
        auto valStr = pt().get(key, aux);
        if constexpr(Dune::models<Dune::Functions::Concept::HasResize, T>())
        result.resize(valStr.size());

        assert(result.size() == valStr.size());
        for (std::size_t i = 0; i < valStr.size(); ++i) {
          result[i] = parseExpression<typename T::value_type,T>(valStr[i],key,i);
        }
        return result;
      } else {
        return pt().get<T>(key);
      }
    }

    /// replace expression with the respective result
    template<typename T>
    static bool parse(const std::string& input, T& result) {
      std::string inputSwap = input;
      std::string allowedChars = "+-*/()0123456789";

      // do not parse strings
      std::string notAllowed = ",abcdfghijklmnopqrstuvwxyzABCDFGHIJKLMNOPQRSTUVWXYZ";
      std::size_t found = inputSwap.find_first_of(allowedChars);
      std::size_t found2 = inputSwap.find_first_of(notAllowed);
      mu::Parser parser;

      if (found != std::string::npos && found2 == std::string::npos) {
        parser.SetExpr(input);
        result = parser.Eval();
        return true;
      }
      return false;
    }

    template<typename T>
    static T parseExpression(const std::string& input,
                             const std::string& key) {
      T result;
      if (parse(input, result)) return result;

      return pt().get<T>(key);
    }

    template<typename T, typename Container>
    static T parseExpression(const std::string& input,
                             const std::string& key,
                             size_t i) {
      T result;
      if (parse(input, result)) return result;

      auto container = pt().get<Container>(key);
      return container[i];
    }

    /// Template for container types with [] operator and existing value_type
    template <typename Container, typename = void>
    struct is_arithmetic_container : std::false_type {};

    template <typename Container>
    struct is_arithmetic_container<Container,
          std::enable_if_t<std::is_arithmetic<typename Container::value_type>::value &&
          !std::is_same<Container, std::string>::value>>
    : std::true_type {};


    /// Return the singleton that contains the data
    static Initfile& singlett()
    {
      static Initfile initfile;
      return initfile;
    }

    int msgInfo_ = 0;
    int paramInfo_ = 1;
    bool breakOnMissingTag_ = false;

    /// ParameterTree to read/store parameter values
    Dune::ParameterTree pt_;
  };

  using Parameters = Initfile;

} // end namespace AMDiS
