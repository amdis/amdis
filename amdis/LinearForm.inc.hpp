#pragma once

#include <utility>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/LocalOperator.hpp>
#include <amdis/OperatorAdapter.hpp>
#include <amdis/functions/EntitySet.hpp>
#include <amdis/operations/Assigner.hpp>
#include <amdis/typetree/Traversal.hpp>
#include <amdis/typetree/TreePath.hpp>

namespace AMDiS {

template <class GB, class T, class Traits>
  template <class ContextTag, class Expr, class TreePath>
void LinearForm<GB,T,Traits>::
addOperator(ContextTag contextTag, Expr const& expr, TreePath path)
{
  static_assert( Concepts::PreTreePath<TreePath>,
      "path must be a valid treepath, or an integer/index-constant");

  using LocalContext = typename Impl::ContextTagType<ContextTag>::type;
  auto op = makeOperator<LocalContext>(expr, this->basis().gridView());
  if constexpr(std::is_same_v<TreePath,RootTreePath>)
    assembler_.push(contextTag, std::move(op));
  else
    assembler_.push(contextTag, OperatorAdapter{std::move(op), makeTreePath(path)});
}


template <class GB, class T, class Traits>
  template <class LocalView,
    REQUIRES_(Concepts::LocalView<LocalView>)>
void LinearForm<GB,T,Traits>::
assemble(LocalView const& localView)
{
  assert(localView.bound());

  elementVector_.resize(localView.size());
  elementVector_ = 0;

  auto const& gv = this->basis().gridView();
  auto const& element = localView.element();
  GlobalContext<TYPEOF(gv)> context{gv, element, element.geometry()};

  auto rhsOp = localAssembler(assembler_);
  rhsOp.bind(element);
  rhsOp.assemble(context, localView.treeCache(), elementVector_);
  rhsOp.unbind();

  this->scatter(localView, elementVector_, Assigner::plus_assign{});
}


template <class GB, class T, class Traits>
void LinearForm<GB,T,Traits>::
assemble()
{
  auto localView = this->basis().localView();

  this->init(this->basis(), true);
  for (auto const& element : entitySet(this->basis())) {
    localView.bind(element);
    this->assemble(localView);
    localView.unbind();
  }
  this->finish();
}

} // end namespace AMDiS
