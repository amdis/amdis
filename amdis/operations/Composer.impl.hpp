#pragma once

#include <tuple>
#include <utility>

#include <amdis/operations/Arithmetic.hpp>
#include <amdis/operations/Composer.hpp>

namespace AMDiS { namespace Operation {

  /// Partial derivative of composed function:
  /// Implements: // sum_i [ d_i(f)[g...] * d_j(g_i) ]
  template <int J, class F, class... Gs>
  auto partial(Composer<F,Gs...> const& c, index_t<J> _j)
  {
    auto index_seq = std::make_index_sequence<sizeof...(Gs)>{};

    // d_i(f)[g...] * d_j(g_i)
    auto term_i = [&](auto const _i)
    {
      auto di_f = std::apply([&](auto const&... gs) {
        return compose(partial(c.f_, _i), gs...);
      }, c.gs_);

      auto const& g_i = std::get<_i>(c.gs_);
      return compose(Multiplies{}, di_f, partial(g_i, _j));
    };

    // sum_i [ d_i(f)[g...] * d_j(g_i) ]
    return std::apply([&](auto const... _i)
    {
      return compose(Plus{}, term_i(_i)...);
    }, index_seq);
  }

}} // end namespace AMDiS::Operation
