#pragma once

#include <algorithm>
#include <utility>

#include <amdis/common/Index.hpp>
#include <amdis/common/ConceptsBase.hpp>

namespace AMDiS
{
  namespace Concepts
  {
    namespace Definition
    {
      struct HasFunctorOrder
      {
        template <class F, int... I>
        auto require(F&& f, std::integer_sequence<int,I...>) -> decltype( order(f, I...) );
      };

      struct HasFunctorVectorOrder
      {
        template <class F>
        auto require(F&& f, std::vector<int> o = {}) -> decltype( order(f, o) );
      };
    }

    template <class F, int N>
    constexpr bool HasFunctorOrder = models<Definition::HasFunctorOrder(F, std::make_integer_sequence<int,N>)>;

    template <class F>
    constexpr bool HasFunctorVectorOrder = models<Definition::HasFunctorVectorOrder(F)>;

    template <class F, int N>
    using HasFunctorOrder_t = bool_t<HasFunctorOrder<F,N>>;
  }

  namespace Operation
  {
    /** \addtogroup operations
     *  @{
     **/

    /// Functor representing a static constant value
    template <class T, T value>
    struct StaticConstant
    {
      template <class... Ts>
      constexpr T operator()(Ts const&... /*args*/) const
      {
        return value;
      }
    };

    using Zero = StaticConstant<int, 0>;
    using One = StaticConstant<int, 1>;

    template <class T, T value, class... Int>
    constexpr int order(StaticConstant<T,value> const&, Int... /*orders*/)
    {
      return 0;
    }

    template <class T, T value, std::size_t J>
    constexpr auto partial(StaticConstant<T,value> const&, index_t<J>)
    {
      return Zero{};
    }

    // -------------------------------------------------------------------------

    /// (Unary-)Functor representing the identity
    struct Id
    {
      template <class T>
      constexpr T const& operator()(T const& x) const
      {
        return x;
      }

      friend constexpr int order(Id const&, int d)
      {
        return d;
      }

      friend auto partial(Id const&, index_t<0>)
      {
        return One{};
      }
    };

    // -------------------------------------------------------------------------

    /// Functor representing a constant value
    template <class T>
    struct Constant
    {
      constexpr Constant(T value)
        : value_(value)
      {}

      template <class... Ts>
      constexpr T const& operator()(Ts const&... /*args*/) const
      {
        return value_;
      }

    private:
      T value_;
    };

    template <class T, class... Int>
    constexpr int order(Constant<T> const&, Int... /*orders*/)
    {
      return 0;
    }

    template <class T, std::size_t J>
    constexpr auto partial(Constant<T> const&, index_t<J>)
    {
      return Zero{};
    }

    // -------------------------------------------------------------------------

    /// Functor representing a constant value
    template <class T>
    struct Reference
    {
      constexpr Reference(T& value)
        : value_(&value)
      {}

      template <class... Ts>
      constexpr T const& operator()(Ts const&... /*args*/) const
      {
        return *value_;
      }

    private:
      T* value_;
    };

    template <class T, class... Int>
    constexpr int order(Reference<T> const&, Int... /*orders*/)
    {
      return 0;
    }

    template <class T, std::size_t J>
    constexpr auto partial(Reference<T> const&, index_t<J>)
    {
      return Zero{};
    }

    // -------------------------------------------------------------------------


    template <class T0, class... Ts>
    inline constexpr decltype(auto) get_element(index_t<0>, T0&& t0, Ts&&... /*ts*/)
    {
      return FWD(t0);
    }

    template <std::size_t J, class T0, class... Ts>
    inline constexpr decltype(auto) get_element(index_t<J>, T0&& /*t0*/, Ts&&... ts)
    {
      static_assert(J <= sizeof...(Ts), "");
      return get_element(index_t<J-1>{}, FWD(ts)...);
    }

    template <std::size_t I>
    struct Arg
    {
      template <class... Ts>
      constexpr auto&& operator()(Ts&&... args) const
      {
        return get_element(index_t<I>{}, FWD(args)...);
      }
    };

    template <std::size_t I, class... Int>
    constexpr int order(Arg<I> const&, Int... orders)
    {
      return get_element(index_t<I>{}, orders...);
    }

    template <std::size_t I, std::size_t J>
    constexpr auto partial(Arg<I> const&, index_t<J>)
    {
      return StaticConstant<int,(I==J ? 1 : 0)>{};
    }

    // -------------------------------------------------------------------------

    template <std::size_t I>
    struct Get
    {
      template <class T, int N>
      constexpr T const& operator()(Dune::FieldVector<T,N> const& vec) const
      {
        return vec[I];
      }

      friend constexpr int order(Get const&, int d)
      {
        return d;
      }
    };

    struct Get_
    {
      explicit constexpr Get_(std::size_t i)
        : i_(i)
      {}

      template <class T, int N>
      constexpr T const& operator()(Dune::FieldVector<T,N> const& vec) const
      {
        return vec[i_];
      }

      friend constexpr int order(Get_ const&, int d)
      {
        return d;
      }

      std::size_t i_;
    };

    /** @} **/

  } // end namespace Operation
} // end namespace AMDiS
