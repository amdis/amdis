#pragma once

namespace AMDiS
{
  namespace Operation
  {
    /// Operation that represents max(A,B)
    struct Max
    {
      template <class T, class S>
      constexpr auto operator()(T const& lhs, S const& rhs) const
      {
        return Math::max(lhs, rhs);
      }
    };

    // -------------------------------------------------------------------------

    /// Operation that represents min(A,B)
    struct Min
    {
      template <class T, class S>
      constexpr auto operator()(T const& lhs, S const& rhs) const
      {
        return Math::min(lhs, rhs);
      }
    };

    // -------------------------------------------------------------------------

    /// Operation that represents max(|A|,|B|)
    struct AbsMax
    {
      template <class T, class S>
      constexpr auto operator()(T const& lhs, S const& rhs) const
      {
        using std::abs;
        return Math::max(abs(lhs), abs(rhs));
      }
    };

    // -------------------------------------------------------------------------

    /// Operation that represents min(|A|,|B|)
    struct AbsMin
    {
      template <class T, class S>
      constexpr auto operator()(T const& lhs, S const& rhs) const
      {
        using std::abs;
        return Math::min(abs(lhs), abs(rhs));
      }
    };

  } // end namespace Operation
} // end namespace AMDiS
