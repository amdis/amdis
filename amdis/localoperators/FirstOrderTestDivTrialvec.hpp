#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct test_divtrialvec {};
  }


  /// first-order operator \f$ \langle\psi, c\,\nabla\cdot\Phi\rangle \f$
  class FirstOrderTestDivTrialvec
  {
  public:
    FirstOrderTestDivTrialvec(tag::test_divtrialvec) {}

    template <class CG, class RN, class CN, class Quad, class LocalFct, class Mat>
    void assemble(CG const& contextGeo, RN const& rowNode, CN const& colNode,
                  Quad const& quad, LocalFct const& localFct, Mat& elementMatrix) const
    {
      static_assert(static_size_v<typename LocalFct::Range> == 1, "Expression must be of scalar type.");
      static_assert(RN::isLeaf && CN::isPower,
        "row-node must be Leaf-Node and col-node must be a Power-Node.");

      assert( colNode.degree() == CG::dow );

      std::size_t rowSize = rowNode.size();
      std::size_t colSize = colNode.child(0).size();

      using RangeFieldType = typename CN::ChildType::LocalBasis::Traits::RangeFieldType;
      using WorldVector = FieldVector<RangeFieldType,CG::dow>;
      std::vector<WorldVector> colGradients;

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.elementGeometry().jacobianInverseTransposed(local);

        // The multiplicative factor in the integral transformation formula
        const auto factor = localFct(local) * contextGeo.integrationElement(qp.position()) * qp.weight();

        // the values of the shape functions on the reference element at the quadrature point
        auto const& shapeValues = rowNode.localBasisValuesAt(local);

        // The gradients of the shape functions on the reference element
        auto const& shapeGradients = colNode.child(0).localBasisJacobiansAt(local);

        // Compute the shape function gradients on the real element
        colGradients.resize(shapeGradients.size());

        for (std::size_t i = 0; i < colGradients.size(); ++i)
          jacobian.mv(shapeGradients[i][0], colGradients[i]);

        for (std::size_t i = 0; i < rowSize; ++i) {
          const auto local_i = rowNode.localIndex(i);
          const auto value = factor * shapeValues[i];
          for (std::size_t j = 0; j < colSize; ++j) {
            for (std::size_t k = 0; k < colNode.degree(); ++k) {
              const auto local_kj = colNode.child(k).localIndex(j);
              elementMatrix[local_i][local_kj] += value * colGradients[j][k];
            }
          }
        }
      }

    }
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::test_divtrialvec, LC>
  {
    static constexpr int degree = 1;
    using type = FirstOrderTestDivTrialvec;
  };

  /** @} **/

} // end namespace AMDiS
