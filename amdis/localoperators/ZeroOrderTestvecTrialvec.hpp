#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/common/ValueCategory.hpp>
#include <amdis/typetree/FiniteElementType.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct testvec_trialvec {};
  }


  /// zero-order operator \f$ \langle\Psi, c\,\Phi\rangle \f$, or \f$ \langle\Psi, A\,\Phi\rangle \f$
  class ZeroOrderTestvecTrialvec
  {
  public:
    ZeroOrderTestvecTrialvec(tag::testvec_trialvec) {}

    template <class CG, class RN, class CN, class Quad, class LocalFct, class Mat>
    void assemble(CG const& contextGeo, RN const& rowNode, CN const& colNode,
                  Quad const& quad, LocalFct const& localFct, Mat& elementMatrix) const
    {
      using expr_value_type = typename LocalFct::Range;
      static_assert(static_size_v<expr_value_type> == 1 ||
                      (static_num_rows_v<expr_value_type> == CG::dow &&
                      static_num_cols_v<expr_value_type> == CG::dow),
        "Expression must be of scalar or matrix type." );
      static_assert(RN::isPower && CN::isPower,
        "Operator can be applied to Power-Nodes only.");
      assert(rowNode.degree() == colNode.degree());

      const bool sameFE = std::is_same_v<FiniteElementType_t<RN>, FiniteElementType_t<CN>>;
      const bool sameNode = rowNode.treeIndex() == colNode.treeIndex();

      using Category = ValueCategory_t<expr_value_type>;

      if (sameFE && sameNode && std::is_same_v<Category,tag::scalar>)
        getElementMatrixOptimized(contextGeo, quad, rowNode, colNode, localFct, elementMatrix, Category{});
      else
        getElementMatrixStandard(contextGeo, quad, rowNode, colNode, localFct, elementMatrix, Category{});
    }

  protected: // specialization for different value-categories

    template <class CG, class QR, class RN, class CN, class LocalFct, class Mat>
    void getElementMatrixStandard(CG const& contextGeo, QR const& quad,
                                  RN const& rowNode, CN const& colNode,
                                  LocalFct const& localFct, Mat& elementMatrix, tag::scalar) const
    {
      std::size_t rowSize = rowNode.child(0).size();
      std::size_t colSize = colNode.child(0).size();

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The multiplicative factor in the integral transformation formula
        const auto factor = localFct(local) * contextGeo.integrationElement(qp.position()) * qp.weight();

        auto const& rowShapeValues = rowNode.child(0).localBasisValuesAt(local);
        auto const& colShapeValues = colNode.child(0).localBasisValuesAt(local);

        for (std::size_t i = 0; i < rowSize; ++i) {
          const auto value = factor * rowShapeValues[i];

          for (std::size_t j = 0; j < colSize; ++j) {
            const auto value0 = value * colShapeValues[j];

            for (std::size_t k = 0; k < rowNode.degree(); ++k) {
              const auto local_ki = rowNode.child(k).localIndex(i);
              const auto local_kj = colNode.child(k).localIndex(j);

              elementMatrix[local_ki][local_kj] += value0;
            }
          }
        }
      }
    }


    template <class CG, class QR, class RN, class CN, class LocalFct, class Mat>
    void getElementMatrixOptimized(CG const& contextGeo, QR const& quad,
                                   RN const& node, CN const& /*colNode*/,
                                   LocalFct const& localFct, Mat& elementMatrix, tag::scalar) const
    {
      std::size_t size = node.child(0).size();

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The multiplicative factor in the integral transformation formula
        const auto factor = localFct(local) * contextGeo.integrationElement(qp.position()) * qp.weight();

        auto const& shapeValues = node.child(0).localBasisValuesAt(local);

        for (std::size_t i = 0; i < size; ++i) {
          const auto value = factor * shapeValues[i];

          for (std::size_t k = 0; k < node.degree(); ++k) {
            const auto local_ki = node.child(k).localIndex(i);
            elementMatrix[local_ki][local_ki] += value * shapeValues[i];
          }

          for (std::size_t j = i+1; j < size; ++j) {
            const auto value0 = value * shapeValues[j];

            for (std::size_t k = 0; k < node.degree(); ++k) {
              const auto local_ki = node.child(k).localIndex(i);
              const auto local_kj = node.child(k).localIndex(j);

              elementMatrix[local_ki][local_kj] += value0;
              elementMatrix[local_kj][local_ki] += value0;
            }
          }
        }
      }
    }

    template <class CG, class QR, class RN, class CN, class LocalFct, class Mat>
    void getElementMatrixStandard(CG const& contextGeo, QR const& quad,
                                  RN const& rowNode, CN const& colNode,
                                  LocalFct const& localFct, Mat& elementMatrix, tag::matrix) const
    {
      using expr_value_type [[maybe_unused]] = typename LocalFct::Range;
      assert(static_num_rows_v<expr_value_type> == rowNode.degree() &&
             static_num_cols_v<expr_value_type> == rowNode.degree());

      std::size_t rowSize = rowNode.child(0).size();
      std::size_t colSize = colNode.child(0).size();

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The multiplicative factor in the integral transformation formula
        const auto factor = contextGeo.integrationElement(qp.position()) * qp.weight();
        const auto exprValue = localFct(local);

        auto const& rowShapeValues = rowNode.child(0).localBasisValuesAt(local);
        auto const& colShapeValues = colNode.child(0).localBasisValuesAt(local);

        for (std::size_t i = 0; i < rowSize; ++i) {
          const auto value0 = factor * rowShapeValues[i];

          for (std::size_t j = 0; j < colSize; ++j) {
            const auto value = value0 * colShapeValues[j];
            const auto mat = exprValue * value;

            for (std::size_t k0 = 0; k0 < rowNode.degree(); ++k0) {
              const auto local_ki = rowNode.child(k0).localIndex(i);
              for (std::size_t k1 = 0; k1 < rowNode.degree(); ++k1) {
                const auto local_kj = colNode.child(k1).localIndex(j);

                elementMatrix[local_ki][local_kj] += mat[k0][k1];
              }
            }
          }
        }
      }
    }

    template <class CG, class QR, class RN, class CN, class LocalFct, class Mat>
    void getElementMatrixOptimized(CG const&, QR const&, RN const&, CN const&,
                                   LocalFct const&, Mat&, tag::matrix) const
    {
      DUNE_THROW(Dune::NotImplemented,
        "Optimized LocalOperator with matrix coefficients not implemented.");
    }
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::testvec_trialvec, LC>
  {
    static constexpr int degree = 0;
    using type = ZeroOrderTestvecTrialvec;
  };

  /** @} **/

} // end namespace AMDiS
