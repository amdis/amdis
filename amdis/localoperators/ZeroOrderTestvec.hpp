#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct testvec {};
  }


  /// zero-order vector-operator \f$ (\mathbf{b}\cdot\Psi) \f$
  class ZeroOrderTestVec
  {
  public:
    ZeroOrderTestVec(tag::testvec) {}

    template <class CG, class Node, class Quad, class LocalFct, class Vec>
    void assemble(CG const& contextGeo, Node const& node, Quad const& quad,
                  LocalFct const& localFct, Vec& elementVector) const
    {
      static_assert(static_size_v<typename LocalFct::Range> == CG::dow,
        "Expression must be of vector type." );
      static_assert(Node::isPower,
        "Operator can be applied to Power-Nodes only.");

      assert(node.degree() == CG::dow);

      std::size_t size = node.child(0).size();

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The multiplicative factor in the integral transformation formula
        const auto factor = contextGeo.integrationElement(qp.position()) * qp.weight();
        const auto exprValue = localFct(local);

        auto const& shapeValues = node.child(0).localBasisValuesAt(local);

        for (std::size_t i = 0; i < size; ++i) {
          const auto value = exprValue * (factor * shapeValues[i]);
          for (std::size_t k = 0; k < node.degree(); ++k) {
            const auto local_ki = node.child(k).localIndex(i);
            elementVector[local_ki] += at(value,k);
          }
        }
      }
    }
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::testvec, LC>
  {
    static constexpr int degree = 0;
    using type = ZeroOrderTestVec;
  };

  /** @} **/

} // end namespace AMDiS
