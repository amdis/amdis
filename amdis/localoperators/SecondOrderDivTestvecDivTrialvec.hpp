#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/typetree/FiniteElementType.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct divtestvec_divtrialvec {};
  }


  /// second-order operator \f$ \langle\nabla\cdot\Psi, c\,\nabla\cdot\Phi\rangle \f$
  class SecondOrderDivTestvecDivTrialvec
  {
  public:
    SecondOrderDivTestvecDivTrialvec(tag::divtestvec_divtrialvec) {}

    template <class CG, class RN, class CN, class Quad, class LocalFct, class Mat>
    void assemble(CG const& contextGeo, RN const& rowNode, CN const& colNode,
                  Quad const& quad, LocalFct const& localFct, Mat& elementMatrix) const
    {
      static_assert(static_size_v<typename LocalFct::Range> == 1,
        "Expression must be of scalar type.");
      static_assert(RN::isPower && CN::isPower,
        "Operator can be applied to Power-Nodes only.");

      const bool sameFE = std::is_same_v<FiniteElementType_t<RN>, FiniteElementType_t<CN>>;
      const bool sameNode = rowNode.treeIndex() == colNode.treeIndex();

      if (sameFE && sameNode)
        getElementMatrixOptimized(contextGeo, quad, rowNode, colNode, localFct, elementMatrix);
      else
        getElementMatrixStandard(contextGeo, quad, rowNode, colNode, localFct, elementMatrix);
    }

  protected:
    template <class CG, class QR, class RN, class CN, class LocalFct, class Mat>
    void getElementMatrixStandard(CG const& contextGeo, QR const& quad,
                                  RN const& rowNode, CN const& colNode,
                                  LocalFct const& localFct, Mat& elementMatrix) const
    {
      assert( rowNode.degree() == colNode.degree() );

      std::size_t rowSize = rowNode.child(0).size();
      std::size_t colSize = colNode.child(0).size();

      using RowFieldType = typename RN::ChildType::LocalBasis::Traits::RangeFieldType;
      using RowWorldVector = FieldVector<RowFieldType,CG::dow>;
      std::vector<RowWorldVector> rowGradients;

      using ColFieldType = typename CN::ChildType::LocalBasis::Traits::RangeFieldType;
      using ColWorldVector = FieldVector<ColFieldType,CG::dow>;
      std::vector<ColWorldVector> colGradients;

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        decltype(auto) local = contextGeo.coordinateInElement(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.elementGeometry().jacobianInverseTransposed(local);

        // The multiplicative factor in the integral transformation formula
        const auto factor = localFct(local) * contextGeo.integrationElement(qp.position()) * qp.weight();

        // The gradients of the shape functions on the reference element
        auto const& rowShapeGradients = rowNode.child(0).localBasisJacobiansAt(local);
        auto const& colShapeGradients = colNode.child(0).localBasisJacobiansAt(local);

        // Compute the shape function gradients on the real element
        rowGradients.resize(rowShapeGradients.size());

        for (std::size_t i = 0; i < rowGradients.size(); ++i)
          jacobian.mv(rowShapeGradients[i][0], rowGradients[i]);

        colGradients.resize(colShapeGradients.size());

        for (std::size_t i = 0; i < colGradients.size(); ++i)
          jacobian.mv(colShapeGradients[i][0], colGradients[i]);

        for (std::size_t i = 0; i < rowSize; ++i) {
          for (std::size_t j = 0; j < colSize; ++j) {
              for (std::size_t k = 0; k < CG::dow; ++k) {
                  for (std::size_t l = 0; l < CG::dow; ++l) {
                      const auto local_ki = rowNode.child(k).localIndex(i);
                      const auto local_kj = colNode.child(l).localIndex(j);
                      elementMatrix[local_ki][local_kj] += factor * rowGradients[i][k] * colGradients[j][l];
                  }
              }
          }
        }
      }
    }


    template <class CG, class QR, class Node, class LocalFct, class Mat>
    void getElementMatrixOptimized(CG const& contextGeo, QR const& quad,
                                   Node const& node, Node const& /*colNode*/,
                                   LocalFct const& localFct, Mat& elementMatrix) const
    {
      std::size_t size = node.child(0).size();

      using RangeFieldType = typename Node::ChildType::LocalBasis::Traits::RangeFieldType;
      using WorldVector = FieldVector<RangeFieldType,CG::dow>;
      std::vector<WorldVector> gradients;

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.elementGeometry().jacobianInverseTransposed(local);

        // The multiplicative factor in the integral transformation formula
        const auto factor = localFct(local) * contextGeo.integrationElement(qp.position()) * qp.weight();

        // The gradients of the shape functions on the reference element
        auto const& shapeGradients = node.child(0).localBasisJacobiansAt(local);

        // Compute the shape function gradients on the real element
        gradients.resize(shapeGradients.size());

        for (std::size_t i = 0; i < gradients.size(); ++i)
          jacobian.mv(shapeGradients[i][0], gradients[i]);

        for (std::size_t k = 0; k < CG::dow; ++k) {
          for (std::size_t l = 0; l < CG::dow; ++l) {
            for (std::size_t i = 0; i < size; ++i) {
              const auto local_ki = node.child(k).localIndex(i);
              const auto value = factor * gradients[i][k];
              elementMatrix[local_ki][local_ki] += value * gradients[i][k];

                for (std::size_t j = i + 1; j < size; ++j) {
                const auto local_kj = node.child(l).localIndex(j);
                elementMatrix[local_ki][local_kj] += value * gradients[j][l];
                elementMatrix[local_kj][local_ki] += value * gradients[j][l];
              }
            }
          }
        }
      }
    }
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::divtestvec_divtrialvec, LC>
  {
    static constexpr int degree = 2;
    using type = SecondOrderDivTestvecDivTrialvec;
  };

  /** @} **/

} // end namespace AMDiS
