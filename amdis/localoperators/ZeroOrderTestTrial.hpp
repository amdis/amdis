#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/typetree/FiniteElementType.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct test_trial {};
  }


  /// zero-order operator \f$ \langle\psi, c\,\phi\rangle \f$
  class ZeroOrderTestTrial
  {
  public:
    ZeroOrderTestTrial(tag::test_trial) {}

    template <class CG, class RN, class CN, class Quad, class LocalFct, class Mat>
    void assemble(CG const& contextGeo, RN const& rowNode, CN const& colNode,
                  Quad const& quad, LocalFct const& localFct, Mat& elementMatrix) const
    {
      static_assert(static_size_v<typename LocalFct::Range> == 1,
        "Expression must be of scalar type." );

      const bool sameFE = std::is_same_v<FiniteElementType_t<RN>, FiniteElementType_t<CN>>;
      const bool sameNode = rowNode.treeIndex() == colNode.treeIndex();

      if (sameFE && sameNode)
        getElementMatrixOptimized(contextGeo, quad, rowNode, colNode, localFct, elementMatrix);
      else
        getElementMatrixStandard(contextGeo, quad, rowNode, colNode, localFct, elementMatrix);
    }

    template <class CG, class Node, class Quad, class LocalFct, class Vec>
    void assemble(CG const& contextGeo, Node const& node,
                  Quad const& quad, LocalFct const& localFct, Vec& elementVector) const
    {
      static_assert(static_size_v<typename LocalFct::Range> == 1,
        "Expression must be of scalar type." );
      static_assert(Node::isLeaf,
        "Operator can be applied to Leaf-Nodes only");

      std::size_t size = node.size();

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The multiplicative factor in the integral transformation formula
        const auto factor = localFct(local) * contextGeo.integrationElement(qp.position()) * qp.weight();

        auto const& shapeValues = node.localBasisValuesAt(local);
        for (std::size_t i = 0; i < size; ++i) {
          const auto local_i = node.localIndex(i);
          elementVector[local_i] += factor * shapeValues[i];
        }
      }
    }


  protected:
    template <class CG, class QR, class RN, class CN, class LocalFct, class Mat>
    void getElementMatrixStandard(CG const& contextGeo, QR const& quad,
                                  RN const& rowNode, CN const& colNode,
                                  LocalFct const& localFct, Mat& elementMatrix) const
    {
      static_assert(RN::isLeaf && CN::isLeaf,
        "Operator can be applied to Leaf-Nodes only.");

      std::size_t rowSize = rowNode.size();
      std::size_t colSize = colNode.size();

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The multiplicative factor in the integral transformation formula
        const auto factor = localFct(local) * contextGeo.integrationElement(qp.position()) * qp.weight();

        auto const& rowShapeValues = rowNode.localBasisValuesAt(local);
        auto const& colShapeValues = colNode.localBasisValuesAt(local);

        for (std::size_t i = 0; i < rowSize; ++i) {
          const auto local_i = rowNode.localIndex(i);
          const auto value = factor * rowShapeValues[i];

          for (std::size_t j = 0; j < colSize; ++j) {
            const auto local_j = colNode.localIndex(j);
            elementMatrix[local_i][local_j] += value * colShapeValues[j];
          }
        }
      }
    }


    template <class CG, class QR, class RN, class CN, class LocalFct, class Mat>
    void getElementMatrixOptimized(CG const& contextGeo, QR const& quad,
                                   RN const& node, CN const& /*colNode*/,
                                   LocalFct const& localFct, Mat& elementMatrix) const
    {
      static_assert(RN::isLeaf && CN::isLeaf,
        "Operator can be applied to Leaf-Nodes only.");

      std::size_t size = node.size();

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The multiplicative factor in the integral transformation formula
        const auto factor = localFct(local) * contextGeo.integrationElement(qp.position()) * qp.weight();

        auto const& shapeValues = node.localBasisValuesAt(local);

        for (std::size_t i = 0; i < size; ++i) {
          const auto local_i = node.localIndex(i);

          const auto value = factor * shapeValues[i];
          elementMatrix[local_i][local_i] += value * shapeValues[i];

          for (std::size_t j = i+1; j < size; ++j) {
            const auto local_j = node.localIndex(j);

            elementMatrix[local_i][local_j] += value * shapeValues[j];
            elementMatrix[local_j][local_i] += value * shapeValues[j];
          }
        }
      }
    }
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::test_trial, LC>
  {
    static constexpr int degree = 0;
    using type = ZeroOrderTestTrial;
  };


  /// Create a zero-order term
  template <class Expr>
  auto zot(Expr&& expr, int quadOrder = -1)
  {
    return makeOperator(tag::test_trial{}, FWD(expr), quadOrder);
  }

  /** @} **/

} // end namespace AMDiS
