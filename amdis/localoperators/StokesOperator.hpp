#pragma once

#include <type_traits>
#include <vector>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct stokes
    {
      bool constViscosity = false;
    };
  }


  /// Stokes operator \f$ \langle\nabla\mathbf{v}, \nu \nabla\mathbf{u}\rangle + \langle\nabla\cdot\mathbf{v}, p\rangle + \langle q, \langle\nabla\cdot\mathbf{u}\rangle \f$
  class StokesOperator
  {
    bool constViscosity_;

  public:
    StokesOperator(tag::stokes t)
      : constViscosity_(t.constViscosity)
    {}

    template <class CG, class Node, class Quad, class LocalFct, class Mat>
    void assemble(CG const& contextGeo, Node const& tree, Node const& /*colNode*/,
                  Quad const& quad, LocalFct const& localFct, Mat& elementMatrix) const
    {
      static_assert(static_size_v<typename LocalFct::Range> == 1,
        "Viscosity must be of scalar type.");

      using VelocityNode = Dune::TypeTree::Child<Node,0>;
      using VelocityCompNode = Dune::TypeTree::Child<Node,0,0>;
      using PressureNode = Dune::TypeTree::Child<Node,1>;

      static_assert(VelocityNode::isPower && PressureNode::isLeaf, "");

      using namespace Dune::Indices;

      std::size_t numVelocityLocalFE = tree.child(_0,0).size();
      std::size_t numPressureLocalFE = tree.child(_1).size();

      using RangeFieldType = typename VelocityNode::ChildType::LocalBasis::Traits::RangeFieldType;
      using WorldVector = Dune::FieldVector<RangeFieldType,CG::dow>;
      std::vector<WorldVector> gradients;

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.elementGeometry().jacobianInverseTransposed(local);

        // The multiplicative factor in the integral transformation formula
        const auto factor = contextGeo.integrationElement(qp.position()) * qp.weight();
        const auto vel_factor = localFct(local) * factor;

        auto const& shapeGradients = tree.child(_0,0).localBasisJacobiansAt(local);

        // Compute the shape function gradients on the real element
        gradients.resize(shapeGradients.size());
        for (std::size_t i = 0; i < gradients.size(); ++i)
          jacobian.mv(shapeGradients[i][0], gradients[i]);

        auto const& pressureValues = tree.child(_1).localBasisValuesAt(local);

        // <viscosity * grad(u), grad(v)>
        for (std::size_t i = 0; i < numVelocityLocalFE; ++i) {
          const auto value_ii = vel_factor * (gradients[i] * gradients[i]);
          for (std::size_t k = 0; k < CG::dow; ++k) {
            const auto local_ki = tree.child(_0,k).localIndex(i);
            elementMatrix[local_ki][local_ki] += value_ii;
          }

          for (std::size_t j = i+1; j < numVelocityLocalFE; ++j) {
            const auto value = vel_factor * (gradients[i] * gradients[j]);
            for (std::size_t k = 0; k < CG::dow; ++k) {
              const auto local_ki = tree.child(_0,k).localIndex(i);
              const auto local_kj = tree.child(_0,k).localIndex(j);
              elementMatrix[local_ki][local_kj] += value;
              elementMatrix[local_kj][local_ki] += value;
            }
          }
        }

        if (!constViscosity_) {
          // <viscosity * d_i u_j, d_j v_i>
          for (std::size_t i = 0; i < numVelocityLocalFE; ++i) {
          for (std::size_t kj = 0; kj < CG::dow; ++kj) {
            const auto value_i_kj = vel_factor * gradients[i][kj];
            for (std::size_t j = 0; j < numVelocityLocalFE; ++j) {
            for (std::size_t ki = 0; ki < CG::dow; ++ki) {
              const auto local_ki = tree.child(_0,ki).localIndex(i);
              const auto local_kj = tree.child(_0,kj).localIndex(j);
              elementMatrix[local_ki][local_kj] += value_i_kj * gradients[j][ki];
            }
            }
          }
          }
        }

        // <p, div(v)> + <div(u), q>
        for (std::size_t i = 0; i < numVelocityLocalFE; ++i) {
          for (std::size_t j = 0; j < numPressureLocalFE; ++j) {
            const auto value = factor * pressureValues[j];
            for (std::size_t k = 0; k < CG::dow; ++k) {
              const auto local_v = tree.child(_0,k).localIndex(i);
              const auto local_p = tree.child(_1).localIndex(j);

              elementMatrix[local_v][local_p] += gradients[i][k] * value;
              elementMatrix[local_p][local_v] += gradients[i][k] * value;
            }
          }
        }
      }

    } // end assemble
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::stokes, LC>
  {
    static constexpr int degree = 2;
    using type = StokesOperator;
  };

  /** @} **/

} // end namespace AMDiS
