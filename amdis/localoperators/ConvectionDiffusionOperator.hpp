#pragma once

#include <type_traits>

#include <dune/common/hybridutilities.hh>

#include <amdis/LocalOperator.hpp>
#include <amdis/common/Order.hpp>
#include <amdis/common/StaticSize.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/typetree/FiniteElementType.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/
  template <class LocalFctA, class LocalFctB, class LocalFctC, class LocalFctF,
            bool conserving = true>
  class ConvectionDiffusionLocalOperator;

  template <class GridFctA, class GridFctB, class GridFctC, class GridFctF,
            bool conserving = true>
  class ConvectionDiffusionOperator
  {
  public:
    /// \brief Constructor. Stores a copy of all passed gridfunctions
    ConvectionDiffusionOperator(GridFctA const& gridFctA, GridFctB const& gridFctB,
                                GridFctC const& gridFctC, GridFctF const& gridFctF,
                                int quadOrder, bool_t<conserving> = {})
      : gridFctA_(gridFctA)
      , gridFctB_(gridFctB)
      , gridFctC_(gridFctC)
      , gridFctF_(gridFctF)
      , quadOrder_(quadOrder)
    {}

    template <class GridView>
    void update(GridView const&) { /* do nothing */ }

    friend auto localOperator(ConvectionDiffusionOperator const& op)
    {
      return ConvectionDiffusionLocalOperator{
        localFunction(op.gridFctA_),
        localFunction(op.gridFctB_),
        localFunction(op.gridFctC_),
        localFunction(op.gridFctF_),
        op.quadOrder_, bool_t<conserving>{}};
    }

  private:
    GridFctA gridFctA_;
    GridFctB gridFctB_;
    GridFctC gridFctC_;
    GridFctF gridFctF_;
    int quadOrder_;
  };

  /// convection-diffusion operator, see \ref convectionDiffusion
  /// <A*grad(u),grad(v)> - <b*u, grad(v)> + <c*u, v> = <f, v> (conserving) or
  /// <A*grad(u),grad(v)> + <b*grad(u), v> + <c*u, v> = <f, v> (non conserving)
  template <class LocalFctA, class LocalFctB, class LocalFctC, class LocalFctF,
            bool conserving>
  class ConvectionDiffusionLocalOperator
  {
  public:
    ConvectionDiffusionLocalOperator(
          LocalFctA const& localFctA, LocalFctB const& localFctB,
          LocalFctC const& localFctC, LocalFctF const& localFctF,
          int quadOrder, bool_t<conserving> = {})
      : localFctA_(localFctA)
      , localFctB_(localFctB)
      , localFctC_(localFctC)
      , localFctF_(localFctF)
      , quadOrder_(quadOrder)
    {}

    template <class Element>
    void bind(Element const& element)
    {
      localFctA_.bind(element);
      localFctB_.bind(element);
      localFctC_.bind(element);
      localFctF_.bind(element);
    }

    /// Unbinds operator from element.
    void unbind()
    {
      localFctA_.unbind();
      localFctB_.unbind();
      localFctC_.unbind();
      localFctF_.unbind();
    }

    template <class CG, class RN, class CN, class Mat>
    void assemble(CG const& contextGeo, RN const& rowNode, CN const& colNode,
                  Mat& elementMatrix) const
    {
      using A_range = typename LocalFctA::Range;
      static_assert(static_size_v<A_range> == 1 || (static_num_rows_v<A_range> == CG::dow && static_num_cols_v<A_range> == CG::dow),
        "Expression A(x) must be of scalar or matrix type.");
      using b_range = typename LocalFctB::Range;
      static_assert(static_size_v<b_range> == 1 || static_size_v<b_range> == CG::dow,
        "Expression b(x) must be of scalar or vector type.");
      using c_range = typename LocalFctC::Range;
      static_assert(static_size_v<c_range> == 1,
        "Expression c(x) must be of scalar type.");
      static_assert(RN::isLeaf && CN::isLeaf,
        "Operator can be applied to Leaf-Nodes only." );
      static_assert(std::is_same_v<FiniteElementType_t<RN>, FiniteElementType_t<CN>>,
        "Galerkin operator requires equal finite elements for test and trial space." );

      using RangeFieldType = typename RN::LocalBasis::Traits::RangeFieldType;
      using WorldVector = FieldVector<RangeFieldType,CG::dow>;
      std::vector<WorldVector> gradients;

      std::size_t numLocalFe = rowNode.size();

      auto contextGeometry = contextGeo.geometry();
      int quadDeg = std::max({
        getQuadratureDegree(contextGeometry,2,coeffOrder(localFctA_),rowNode,colNode),
        getQuadratureDegree(contextGeometry,1,coeffOrder(localFctB_),rowNode,colNode),
        getQuadratureDegree(contextGeometry,1,coeffOrder(localFctC_),rowNode,colNode)});

      using QuadratureRules = Dune::QuadratureRules<typename CG::Geometry::ctype, CG::Geometry::mydimension>;
      auto const& quad = QuadratureRules::rule(contextGeo.type(), quadDeg);

      for (std::size_t iq = 0; iq < quad.size(); ++iq) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(quad[iq].position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.elementGeometry().jacobianInverseTransposed(local);

        // The multiplicative factor in the integral transformation formula
        const auto factor = contextGeo.integrationElement(quad[iq].position()) * quad[iq].weight();

        // the values of the shape functions on the reference element at the quadrature point
        auto const& shapeValues = rowNode.localBasisValuesAt(local);

        // The gradients of the shape functions on the reference element
        auto const& shapeGradients = rowNode.localBasisJacobiansAt(local);

        // Compute the shape function gradients on the real element
        gradients.resize(shapeGradients.size());

        for (std::size_t i = 0; i < gradients.size(); ++i)
          jacobian.mv(shapeGradients[i][0], gradients[i]);

        const auto A = localFctA_(local);
        WorldVector b = makeB<CG::dow>(localFctB_(local));
        const auto c = localFctC_(local);

        if (conserving) {
          WorldVector gradAi;
          for (std::size_t i = 0; i < numLocalFe; ++i) {
            const auto local_i = rowNode.localIndex(i);
            gradAi = A * gradients[i];
            auto gradBi = b * gradients[i];
            for (std::size_t j = 0; j < numLocalFe; ++j) {
              const auto local_j = colNode.localIndex(j);
              elementMatrix[local_i][local_j] += (dot(gradAi, gradients[j]) + (c * shapeValues[i] - gradBi) * shapeValues[j]) * factor;
            }
          }
        } else {
          WorldVector grad_i;
          for (std::size_t i = 0; i < numLocalFe; ++i) {
            const auto local_i = rowNode.localIndex(i);
            grad_i = A * gradients[i];
            grad_i+= b * shapeValues[i];
            for (std::size_t j = 0; j < numLocalFe; ++j) {
              const auto local_j = colNode.localIndex(j);
              elementMatrix[local_i][local_j] += (dot(grad_i, gradients[j]) + c * shapeValues[i] * shapeValues[j]) * factor;
            }
          }
        }
      }
    }


    template <class CG, class Node, class Vec>
    void assemble(CG const& contextGeo, Node const& node, Vec& elementVector) const
    {
      using f_range = typename LocalFctF::Range;
      static_assert( static_size_v<f_range> == 1,
        "Expression f(x) must be of scalar type." );
      static_assert(Node::isLeaf,
        "Operator can be applied to Leaf-Nodes only." );

      std::size_t numLocalFe = node.size();

      auto const& quad = getQuadratureRule(contextGeo.geometry(), 0, coeffOrder(localFctF_), node);

      for (std::size_t iq = 0; iq < quad.size(); ++iq) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(quad[iq].position());

        // the values of the shape functions on the reference element at the quadrature point
        auto const& shapeValues = node.localBasisValuesAt(local);

        // The multiplicative factor in the integral transformation formula
        const auto factor = localFctF_(local) * contextGeo.integrationElement(quad[iq].position()) * quad[iq].weight();

        for (std::size_t i = 0; i < numLocalFe; ++i) {
          const auto local_i = node.localIndex(i);
          elementVector[local_i] += shapeValues[i] * factor;
        }
      }
    }

  private:
    template <class LocalFct>
    int coeffOrder(LocalFct const& localFct) const
    {
      if constexpr (Concepts::Polynomial<LocalFct>)
        return order(localFct);
      else
        return quadOrder_;
    }

    template <int dow, class T, int N>
    static FieldVector<T,dow> makeB(FieldVector<T,N> const& b) { return b; }

    template <int dow, class T, int N>
    static FieldVector<T,dow> makeB(FieldVector<T,N>&& b) { return std::move(b); }

    template <int dow, class T>
    static FieldVector<T,dow> makeB(FieldVector<T,1> const& b) { return FieldVector<T,dow>(T(b)); }

    template <int dow, class T>
    static FieldVector<T,dow> makeB(FieldVector<T,1>&& b) { return FieldVector<T,dow>(T(b)); }

    template <int dow, class T, std::enable_if_t<std::is_arithmetic_v<T>, int> = 0>
    static FieldVector<T,dow> makeB(T b) { return FieldVector<T,dow>(b); }

  private:
    LocalFctA localFctA_;
    LocalFctB localFctB_;
    LocalFctC localFctC_;
    LocalFctF localFctF_;
    int quadOrder_;
  };

#ifndef DOXYGEN
  template <class PreGridFctA, class PreGridFctB, class PreGridFctC, class PreGridFctF, class c>
  struct ConvectionDiffusionOperatorTerm
  {
    static constexpr bool conserving = c::value;
    PreGridFctA gridFctA;
    PreGridFctB gridFctB;
    PreGridFctC gridFctC;
    PreGridFctF gridFctF;
    int quadOrder;
  };

  template <class Context, class... T, class GridView>
  auto makeOperator(ConvectionDiffusionOperatorTerm<T...> const& pre, GridView const& gridView)
  {
    return ConvectionDiffusionOperator{
      makeGridFunction(pre.gridFctA, gridView),
      makeGridFunction(pre.gridFctB, gridView),
      makeGridFunction(pre.gridFctC, gridView),
      makeGridFunction(pre.gridFctF, gridView),
      pre.quadOrder};
  }
#endif

  /// Generator function for constructing a Convection-Diffusion Operator
  template <bool conserving = true,
            class PreGridFctA, class PreGridFctB, class PreGridFctC, class PreGridFctF>
  auto convectionDiffusion(PreGridFctA const& gridFctA, PreGridFctB const& gridFctB,
                           PreGridFctC const& gridFctC, PreGridFctF const& gridFctF,
                           int quadOrder = -1, bool_t<conserving> = {})
  {
    using Pre = ConvectionDiffusionOperatorTerm<PreGridFctA, PreGridFctB, PreGridFctC, PreGridFctF, bool_t<conserving>>;
    return Pre{gridFctA, gridFctB, gridFctC, gridFctF, quadOrder};
  }

  /** @} **/

} // end namespace AMDiS
