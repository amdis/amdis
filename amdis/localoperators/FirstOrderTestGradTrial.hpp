#pragma once

#include <type_traits>

#include <amdis/GridFunctionOperator.hpp>
#include <amdis/common/StaticSize.hpp>

namespace AMDiS
{
  /**
   * \addtogroup operators
   * @{
   **/

  namespace tag
  {
    struct test_gradtrial {};
    struct grad_trial {};
  }


  /// first-order operator \f$ \langle\psi, \mathbf{b}\cdot\nabla\phi\rangle \f$
  class FirstOrderTestGradTrial
  {
  public:
    FirstOrderTestGradTrial(tag::test_gradtrial) {}

    template <class CG, class RN, class CN, class Quad, class LocalFct, class Mat>
    void assemble(CG const& contextGeo, RN const& rowNode, CN const& colNode,
                  Quad const& quad, LocalFct const& localFct, Mat& elementMatrix) const
    {
      static_assert(static_size_v<typename LocalFct::Range> == CG::dow, "Expression must be of vector type.");
      static_assert(RN::isLeaf && CN::isLeaf,
        "Operator can be applied to Leaf-Nodes only.");

      std::size_t rowSize = rowNode.size();
      std::size_t colSize = colNode.size();

      using RangeFieldType = typename CN::LocalBasis::Traits::RangeFieldType;
      using WorldVector = FieldVector<RangeFieldType,CG::dow>;
      std::vector<WorldVector> colGradients;

      for (auto const& qp : quad) {
        // Position of the current quadrature point in the reference element
        auto&& local = contextGeo.coordinateInElement(qp.position());

        // The transposed inverse Jacobian of the map from the reference element to the element
        const auto jacobian = contextGeo.elementGeometry().jacobianInverseTransposed(local);

        // The multiplicative factor in the integral transformation formula
        const auto factor = contextGeo.integrationElement(qp.position()) * qp.weight();
        const auto b = localFct(local);

        // the values of the shape functions on the reference element at the quadrature point
        auto const& shapeValues = rowNode.localBasisValuesAt(local);

        // The gradients of the shape functions on the reference element
        auto const& shapeGradients = colNode.localBasisJacobiansAt(local);

        // Compute the shape function gradients on the real element
        colGradients.resize(shapeGradients.size());

        for (std::size_t i = 0; i < colGradients.size(); ++i)
          jacobian.mv(shapeGradients[i][0], colGradients[i]);

        for (std::size_t j = 0; j < colSize; ++j) {
          const auto local_j = colNode.localIndex(j);
          const auto value = factor * (b * colGradients[j]);
          for (std::size_t i = 0; i < rowSize; ++i) {
            const auto local_i = rowNode.localIndex(i);
            elementMatrix[local_i][local_j] += value * shapeValues[i];
          }
        }
      }

    }
  };

  template <class LC>
  struct GridFunctionOperatorRegistry<tag::test_gradtrial, LC>
  {
    static constexpr int degree = 1;
    using type = FirstOrderTestGradTrial;
  };


  /// Create a first-order term with derivative on test-function
  template <class Expr>
  auto fot(Expr&& expr, tag::grad_trial, int quadOrder = -1)
  {
    return makeOperator(tag::test_gradtrial{}, FWD(expr), quadOrder);
  }

  /** @} **/

} // end namespace AMDiS
