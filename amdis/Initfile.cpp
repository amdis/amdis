#include <config.h>

#include "Initfile.hpp"

#include <amdis/InitfileParser.hpp>

// a parser for arithmetic expressions
// #include <muParser.h>

namespace AMDiS
{
//   namespace detail
//   {
//     double mu_parser_eval(std::string const& valStr)
//     {
//       mu::Parser parser;
//       parser.DefineConst(_T("M_PI"), m_pi);
//       parser.DefineConst(_T("M_E"), m_e);
//
//       parser.SetExpr(valStr);
//       return parser.Eval();
//     }
//   }

void Initfile::init(std::string const& in)
{
  singlett().read(in);
  singlett().getInternalParameters();
}


void Initfile::read(std::string const& fn, bool /*force*/)
{
  InitfileParser::readInitfile(fn, pt_);
}


void Initfile::getInternalParameters()
{
  get("level of information", msgInfo_);
  get("parameter information", paramInfo_);
  get("break on missing tag", breakOnMissingTag_);

  if (msgInfo_ == 0)
    paramInfo_ = 0;
}


void Initfile::printParameters()
{
  // TODO: implement printing of all parameters
}

} // end namespace AMDiS
