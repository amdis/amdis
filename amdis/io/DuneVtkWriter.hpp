#pragma once

#include <string>
#include <memory>

#if HAVE_DUNE_VTK

#include <dune/vtk/pvdwriter.hh>
#include <dune/vtk/vtkwriter.hh>

#include <amdis/Initfile.hpp>
#include <amdis/common/Filesystem.hpp>
#include <amdis/io/FileWriterBase.hpp>

namespace AMDiS
{
  /// Adapter for the dune-vtk writer
  /**
   * \tparam GV  GridView describing the grid to write
   * \tparam GF  GridFunction defined on that GridView
   **/
  template <class GV, class GF>
  class DuneVtkWriter
      : public FileWriterBase
  {
    using GridView = GV;
    using Writer = Dune::Vtk::VtkWriter<GridView>;
    using SeqWriter = Dune::Vtk::PvdWriter<Writer>;
    using GridFunction = GF;

  public:
    /// Constructor.
    DuneVtkWriter(std::string const& name, GridView const& gridView, GridFunction const& gridFunction)
      : FileWriterBase(name)
      , gridFunction_(gridFunction)
    {
      int m = 0, p = 0;
      Parameters::get(name + "->animation", animation_);
      Parameters::get(name + "->mode", m);
      Parameters::get(name + "->precision", p);

      Dune::Vtk::FormatTypes mode =
        m == 0 ? Dune::Vtk::FormatTypes::ASCII :
        m == 1 ? Dune::Vtk::FormatTypes::BINARY :
                 Dune::Vtk::FormatTypes::COMPRESSED;

      Dune::Vtk::DataTypes precision =
        p == 0 ? Dune::Vtk::DataTypes::FLOAT32 :
                 Dune::Vtk::DataTypes::FLOAT64;

      if (animation_) {
        vtkSeqWriter_ = std::make_shared<SeqWriter>(gridView, mode, precision);
        vtkSeqWriter_->addPointData(gridFunction_, this->name());
      } else {
        vtkWriter_ = std::make_shared<Writer>(gridView, mode, precision);
        vtkWriter_->addPointData(gridFunction_, this->name());
      }
    }

    /// Implements \ref FileWriterBase::write
    void write(AdaptInfo& adaptInfo, bool force) override
    {
      std::string data_dir = this->dir() + "/_piecefiles";
      filesystem::create_directories(data_dir);

      std::string filename = filesystem::path({this->dir(), this->filename() + ".vtk"}).string();
      if (this->doWrite(adaptInfo) || force) {
        if (animation_)
          vtkSeqWriter_->writeTimestep(adaptInfo.time(), filename, data_dir, true);
        else
          vtkWriter_->write(filename, data_dir);
      }
    }

  private:
    GridFunction gridFunction_;

    std::shared_ptr<Writer> vtkWriter_;
    std::shared_ptr<SeqWriter> vtkSeqWriter_;

    /// write .pvd if animation=true, otherwise write only .vtu
    bool animation_ = false;
  };

} // end namespace AMDiS

#endif // HAVE_DUNE_VTK
