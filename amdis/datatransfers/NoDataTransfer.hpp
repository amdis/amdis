#pragma once

#include <amdis/DataTransfer.hpp>

namespace AMDiS {

/**
 * \addtogroup Adaption
 * @{
 **/

namespace tag {

  struct no_datatransfer
      : datatransfer
  {};

} // end namespace tag


/**
 * \brief Implementation of \ref DataTransfer that does not interpolate, but
 * just resizes the containers to the dimension of the basis
 **/
class NoDataTransfer
{
public:
  template <class... Args>
  void preAdapt(Args&&...) { /* do nothing */ }

  template <class Basis, class Container>
  void adapt(Basis const& basis, Container& container)
  {
    container.resize(basis);
  }

  template <class... Args>
  void postAdapt(Args&&...) { /* do nothing */ }
};

template <>
struct DataTransferFactory<tag::no_datatransfer>
{
  template <class Basis, class Container,
    REQUIRES(Concepts::GlobalBasis<Basis>)>
  static DataTransfer<Basis,Container> create(Basis const&, Container const&)
  {
    return NoDataTransfer{};
  }
};

/// @}

} // end namespace AMDiS
