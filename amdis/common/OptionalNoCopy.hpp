#pragma once

#include <optional>

namespace AMDiS {

/// \brief Modification of std::optional that does not copy or move its content
template <class T>
class OptionalNoCopy
    : public std::optional<T>
{
  using Self = OptionalNoCopy;
  using Super = std::optional<T>;

public:
  using Super::Super;

  /// Copy constructor discards the stored optional value
  OptionalNoCopy(Self const&)
    : Super(std::nullopt)
  {}

  /// Move constructor discards the stored optional value
  OptionalNoCopy(Self&&)
    : Super(std::nullopt)
  {}

  /// Copy assignment does nothing
  Self& operator=(Self const&)
  {
    return *this;
  }

  /// Move assignment does nothing
  Self& operator=(Self&&)
  {
    return *this;
  }
};

} // end namespace AMDiS
