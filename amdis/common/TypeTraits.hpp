#pragma once

#include <memory>
#include <type_traits>

namespace AMDiS
{
  /// \brief Remove cv and ref qualifiers of type T.
  /**
   * If the type T is a reference type, provides the member typedef type which
   * is the type referred to by T with its topmost cv-qualifiers removed.
   * Otherwise type is T with its topmost cv-qualifiers removed.
   *
   * Note: This is a backport of c++20 std::remove_cvref
   **/
  template <class T>
  struct remove_cvref
  {
    using type = std::remove_cv_t<std::remove_reference_t<T>>;
  };

  /// Helper alias template for \ref remove_cvref
  template <class T>
  using remove_cvref_t = typename remove_cvref<T>::type;

  namespace Impl
  {
    template <class T>
    struct UnderlyingType
    {
      using type = T;
    };

    template <class T>
    struct UnderlyingType<T&>
    {
      using type = typename UnderlyingType<T>::type;
    };

    template <class T>
    struct UnderlyingType<T*>
    {
      using type = typename UnderlyingType<T>::type;
    };

    template <class T>
    struct UnderlyingType<T* const>
    {
      using type = typename UnderlyingType<T>::type;
    };

    template <class T>
    struct UnderlyingType<std::reference_wrapper<T>>
    {
      using type = typename UnderlyingType<T>::type;
    };

    template <class T>
    struct UnderlyingType<std::shared_ptr<T>>
    {
      using type = typename UnderlyingType<T>::type;
    };

    template <class T>
    struct UnderlyingType<std::shared_ptr<T> const>
    {
      using type = typename UnderlyingType<T>::type;
    };

    template <class T>
    struct UnderlyingType<std::unique_ptr<T>>
    {
      using type = typename UnderlyingType<T>::type;
    };

    template <class T>
    struct UnderlyingType<std::unique_ptr<T> const>
    {
      using type = typename UnderlyingType<T>::type;
    };
  }

  /// Strip of top-level const, ref, and pointer types
  template <class T>
  using Underlying_t = typename Impl::UnderlyingType<T>::type;


  // overload for l-values
  template <class T>
  T& underlying(T& value)
  {
    return value;
  }

  // overload for l-value pointers
  template <class T>
  T& underlying(T* value)
  {
    return *value;
  }

  // overload for plain r-values is deleted
  template <class T>
  T underlying(T&& value) = delete;

  // overload for reference-wrappers
  template <class T>
  T& underlying(std::reference_wrapper<T> const& value)
  {
    return value.get();
  }

  // overload for shared_ptr
  template <class T>
  T& underlying(std::shared_ptr<T> const& value)
  {
    return *value;
  }

  // overload for unique_ptr
  template <class T>
  T& underlying(std::unique_ptr<T> const& value)
  {
    return *value;
  }


  /// Macro for forwarding universal references to obj
  #define FWD(...) static_cast<decltype(__VA_ARGS__)>(__VA_ARGS__)

  /// A decay version of decltype, similar to GCCs __typeof__
#ifdef __GCC__
  #define TYPEOF(...) __typeof__(__VA_ARGS__)
#else
  #define TYPEOF(...) AMDiS::remove_cvref_t<decltype(__VA_ARGS__)>
#endif


  /// Extract the static value of an integral_constant variable
  #define VALUE(...) TYPEOF(__VA_ARGS__)::value

  // ---------------------------------------------------------------------------

  /// A variadic type list
  template <class... Ts>
  struct Types {};

  template <class... Ts>
  using Types_t = Types<remove_cvref_t<Ts>...>;


  /// Alias that indicates ownership of resources
  template <class T>
  using owner = T;

  /// A functor with no operation
  struct NoOp
  {
    template <class... T>
    constexpr void operator()(T&&...) const { /* no nothing */ }
  };

  /// Create a unique_ptr by copy/move construction
  template <class Obj>
  auto makeUniquePtr(Obj&& obj)
  {
    return std::make_unique<TYPEOF(obj)>(FWD(obj));
  }

  template <class Obj>
  auto makeSharedPtr(Obj&& obj)
  {
    return std::make_shared<TYPEOF(obj)>(FWD(obj));
  }

  template <bool... b>
  using enable_if_all_t = std::enable_if_t<(b &&...)>;

} // end namespace AMDiS
