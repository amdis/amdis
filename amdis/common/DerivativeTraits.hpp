#pragma once

#include <dune/functions/common/defaultderivativetraits.hh>

namespace Dune
{
  template <class K, int n>
  class FieldVector;

  template <class K, int n, int m>
  class FieldMatrix;
}

namespace AMDiS
{
  namespace tag
  {
    struct value {};
    struct jacobian {};
    using gradient = jacobian;
    struct divergence {};
    struct partial { std::size_t comp = 0; };

    // register possible types for derivative traits
    struct derivative_type : jacobian, divergence, partial {};
  }

  template <class Sig, class Type>
  struct DerivativeTraits;

  template <class R, class D>
  struct DerivativeTraits<R(D), tag::value>
  {
    using Range = R;
  };

  template <class R, class D>
  struct DerivativeTraits<R(D), tag::jacobian>
      : public Dune::Functions::DefaultDerivativeTraits<R(D)>
  {};

  template <class K, int n>
  struct DerivativeTraits<K(Dune::FieldVector<K,n>), tag::jacobian>
  {
    using Range = Dune::FieldVector<K,n>;
  };

  template <class K, int n>
  struct DerivativeTraits<Dune::FieldVector<K,1>(Dune::FieldVector<K,n>), tag::jacobian>
  {
    using Range = Dune::FieldVector<K,n>;
  };

  template <class R, class K, int n>
  struct DerivativeTraits<R(Dune::FieldVector<K,n>), tag::partial>
  {
    using Range = R;
  };

  template <class K, int n>
  struct DerivativeTraits<Dune::FieldVector<K,n>(Dune::FieldVector<K,n>), tag::divergence>
  {
    using Range = K;
  };

  template <class K, int n, int m>
  struct DerivativeTraits<Dune::FieldMatrix<K,n,m>(FieldVector<K,n>), tag::divergence>
  {
    using Range = Dune::FieldVector<K,m>;
  };

} // end namespace AMDiS
