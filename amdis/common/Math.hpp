#pragma once

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <limits>
#include <type_traits>

#include <dune/common/math.hh>

namespace AMDiS
{
  namespace Math
  {
    /// Implementation of the absolute value \f$|a|\f$ of arithmetic types.
    template <class T>
    constexpr T abs(T const& a)
    {
      return  a < 0 ? -a : a;
    }


    /// Implementation of the square \f$ a^2 \f$ of arithmetic types.
    template <class T>
    constexpr auto sqr(T const& a)
    {
      return a*a;
    }

    /// Implementation of the power \f$ v^p \f$ of arithmetic types `T`.
    template <int p, class T>
    constexpr auto pow(T const& v)
    {
      static_assert( p >= 0, "Exponent p in `pow<p>(v)` should be >= 0," );
      return Dune::power(v,p);
    }


    /// Implementation of the minimum of values \f$ min(a,b)\f$ of any type
    /// supporting the `<` relation.
    /// @{

    template <class T0, class T1>
    constexpr auto min(T0 a, T1 b)
    {
      using T = std::common_type_t<T0,T1>;
      return T(a) < T(b) ? a : b;
    }

    template <class T0>
    constexpr T0 min(T0 a)
    {
      return a;
    }

    template <class T0, class... Ts>
    constexpr auto min(T0 a, Ts... ts)
    {
      return min(a, min(ts...));
    }

    /// @}


    /// Implementation of the maximum of values \f$ max(a,b)\f$ of any type
    /// supporting the `<` relation.
    /// @{

    template <class T0, class T1>
    constexpr auto max(T0 a, T1 b)
    {
      using T = std::common_type_t<T0,T1>;
      return T(a) < T(b) ? b : a;
    }

    template <class T0>
    constexpr T0 max(T0 a)
    {
      return a;
    }

    template <class T0, class... Ts>
    constexpr auto max(T0 a, Ts... ts)
    {
      return max(a, max(ts...));
    }

    /// @}

    // sum over empty set is zero
    constexpr double sum()
    {
      return 0.0;
    }

    template <class... Ts>
    constexpr auto sum(Ts const&... ts)
    {
      return (ts + ...);
    }

  } // end namespace Math

  template <class T>
  constexpr T threshold = std::numeric_limits<T>::epsilon();

} // end namespace AMDiS
