#pragma once

#include <functional>
#include <type_traits>

#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

#include <amdis/common/Tags.hpp>

namespace AMDiS
{
  /// Category of type T, e.g. scalar, vector matrix, specified by a tag
  template <class T, class = void>
  struct ValueCategory
  {
    using type = tag::unknown;
  };

  template <class T>
  using ValueCategory_t = typename ValueCategory<remove_cvref_t<T>>::type;

  template <class T>
  struct ValueCategory<T, std::enable_if_t< std::is_arithmetic_v<T> >>
  {
    using type = tag::scalar;
  };

  template <class K, int SIZE>
  struct ValueCategory< Dune::FieldVector<K, SIZE> >
  {
    using type = tag::vector;
  };

  template <class K>
  struct ValueCategory< Dune::FieldVector<K, 1> >
  {
    using type = tag::scalar;
  };

  template <class K, int ROWS, int COLS>
  struct ValueCategory< Dune::FieldMatrix<K, ROWS, COLS> >
  {
    using type = tag::matrix;
  };

  template <class K>
  struct ValueCategory< Dune::FieldMatrix<K, 1, 1> >
  {
    using type = tag::scalar;
  };

  template <class T>
  struct ValueCategory< std::reference_wrapper<T> >
  {
    using type = typename ValueCategory<T>::type;
  };


  namespace Category
  {
    template <class T>
    constexpr bool Scalar = std::is_same_v<ValueCategory_t<T>, tag::scalar>;

    template <class T>
    constexpr bool Vector = std::is_same_v<ValueCategory_t<T>, tag::vector>;

    template <class T>
    constexpr bool Matrix = std::is_same_v<ValueCategory_t<T>, tag::matrix>;

  } // end namespace Category

  template <class V>
  constexpr bool isVector(V const&)
  {
    static_assert(Category::Vector<V>,"");
    return Category::Vector<V>;
  }

  template <class V>
  constexpr bool isNotVector(V const&)
  {
    static_assert(!Category::Vector<V>,"");
    return !Category::Vector<V>;
  }
} // end namespace AMDiS
