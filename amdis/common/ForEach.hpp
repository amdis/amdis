#pragma once

#include <initializer_list>
#include <tuple>
#include <type_traits>

#include <amdis/common/Index.hpp>

namespace AMDiS
{
  namespace Ranges
  {
    template <class Functor, class... Args>
    constexpr void forVariadic(Functor&& f, Args&&... args)
    {
      (f(FWD(args)),...);
    }

    template <std::size_t... I, class Tuple, class Functor>
    constexpr void forEach(std::index_sequence<I...>, Tuple&& tuple, Functor&& f)
    {
      using std::get;
      (f(get<I>(tuple)),...);
    }

    template <class... T, class Functor>
    constexpr void forEach(std::tuple<T...> const& tuple, Functor&& f)
    {
      Ranges::forEach(std::make_index_sequence<sizeof...(T)>{}, tuple, FWD(f));
    }

    template <class... T, class Functor>
    constexpr void forEach(std::tuple<T...>& tuple, Functor&& f)
    {
      Ranges::forEach(std::make_index_sequence<sizeof...(T)>{}, tuple, FWD(f));
    }

    template <class Range, class Functor>
    constexpr auto forEach(Range&& r, Functor&& f)
      -> std::void_t<decltype((std::begin(r),std::end(r)))>
    {
      for (auto& ri : r)
        f(ri);
    }

    template <std::size_t I0 = 0, std::size_t... I, class Functor>
    constexpr void forIndices(std::index_sequence<I...>, Functor&& f)
    {
      (f(index_t<I0+I>{}),...);
    }

    template <std::size_t I0, std::size_t I1, class Functor>
    constexpr void forIndices(index_t<I0>, index_t<I1>, Functor&& f)
    {
      Ranges::forIndices<I0>(std::make_index_sequence<std::size_t(I1-I0)>{}, FWD(f));
    }

    template <std::size_t N, class Functor>
    constexpr void forIndices(index_t<N>, Functor&& f)
    {
      Ranges::forIndices(std::make_index_sequence<N>{}, FWD(f));
    }

    template <std::size_t I0, std::size_t I1, class Functor>
    constexpr void forIndices(Functor&& f)
    {
      Ranges::forIndices<I0>(std::make_index_sequence<std::size_t(I1-I0)>{}, FWD(f));
    }

    template <std::size_t N, class Functor>
    constexpr void forIndices(Functor&& f)
    {
      Ranges::forIndices<0>(std::make_index_sequence<N>{}, FWD(f));
    }

  } // end namespace Ranges
} // end namespace AMDiS
