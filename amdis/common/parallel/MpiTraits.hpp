#pragma once

#if HAVE_MPI

#include <functional>
#include <type_traits>

#include <mpi.h>

#include <dune/common/parallel/mpitraits.hh>

namespace AMDiS {

// forward declarations
namespace Operation {
  struct Plus;
  struct Multiplies;
  struct Max;
  struct Min;
}

namespace Mpi
{
  namespace Impl2
  {
    template <class T>
    struct is_mpi_type : std::false_type {};

    template <> struct is_mpi_type<char> : std::true_type {};
    template <> struct is_mpi_type<short> : std::true_type {};
    template <> struct is_mpi_type<int> : std::true_type {};
    template <> struct is_mpi_type<long> : std::true_type {};
    template <> struct is_mpi_type<long long> : std::true_type {};
    template <> struct is_mpi_type<signed char> : std::true_type {};
    template <> struct is_mpi_type<unsigned char> : std::true_type {};
    template <> struct is_mpi_type<unsigned short> : std::true_type {};
    template <> struct is_mpi_type<unsigned int> : std::true_type {};
    template <> struct is_mpi_type<unsigned long> : std::true_type {};
    template <> struct is_mpi_type<unsigned long long> : std::true_type {};
    template <> struct is_mpi_type<float> : std::true_type {};
    template <> struct is_mpi_type<double> : std::true_type {};
    template <> struct is_mpi_type<long double> : std::true_type {};

    template <class T1, class T2>
    struct is_mpi_type<std::pair<T1,T2>> : std::integral_constant<bool,
      is_mpi_type<T1>::value &&
      is_mpi_type<T2>::value &&
      std::is_standard_layout_v<std::pair<T1,T2>>
      >
    {};

  } // end namespace Impl2

  template <class T>
  using is_mpi_type = Impl2::is_mpi_type<T>;


  template <class T>
  MPI_Datatype type_to_mpi() { return Dune::MPITraits<T>::getType(); }

  namespace Impl2
  {
    template <class T> using always_false = std::false_type;

    template <class Op>
    struct op_to_mpi;

    template <> struct op_to_mpi<Operation::Min> { static MPI_Op value() { return MPI_MIN; } };
    template <> struct op_to_mpi<Operation::Max> { static MPI_Op value() { return MPI_MAX; } };

    template <> struct op_to_mpi<std::plus<>> { static MPI_Op value() { return MPI_SUM; } };
    template <> struct op_to_mpi<Operation::Plus> { static MPI_Op value() { return MPI_SUM; } };

    template <> struct op_to_mpi<std::multiplies<>> { static MPI_Op value() { return MPI_PROD; } };
    template <> struct op_to_mpi<Operation::Multiplies> { static MPI_Op value() { return MPI_PROD; } };

    template <> struct op_to_mpi<std::logical_and<>> { static MPI_Op value() { return MPI_LAND; } };
    template <> struct op_to_mpi<std::logical_or<>> { static MPI_Op value() { return MPI_LOR; } };
    template <> struct op_to_mpi<std::bit_and<>> { static MPI_Op value() { return MPI_BAND; } };
    template <> struct op_to_mpi<std::bit_or<>> { static MPI_Op value() { return MPI_BOR; } };
    template <> struct op_to_mpi<std::bit_xor<>> { static MPI_Op value() { return MPI_BXOR; } };

  } // end namespac Impl2

  template <class T>
  MPI_Op op_to_mpi() { return Impl2::op_to_mpi<T>::value(); }


  template <class T>
  void* to_void_ptr(T* ptr) { return ptr; }

  template <class T>
  void* to_void_ptr(T const* ptr) { return const_cast<T*>(ptr); }

}} // end namespace AMDiS::Mpi

#endif // HAVE_MPI
