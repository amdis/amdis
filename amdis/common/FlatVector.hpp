#pragma once

#include <vector>

namespace AMDiS
{
  /// Flat data vector to be used in assembling as element vector
  template <class T, class Allocator = std::allocator<T>>
  class FlatVector
      : public std::vector<T,Allocator>
  {
  public:
    using std::vector<T,Allocator>::vector;

    /// Assign value `s` to all entries of the vector
    FlatVector& operator=(T s)
    {
      this->assign(this->size(), s);
      return *this;
    }
  };

} // end namespace AMDiS
