#pragma once

#include <amdis/BoundarySubset.hpp>
#include <amdis/ContextGeometry.hpp>

namespace AMDiS
{
  /// The local operator associated to \ref BoundaryOperator.
  template <class LocalOperator, class Intersection>
  class BoundaryLocalOperator
  {
  public:
    /// Constructor, stores all arguments in local variables
    BoundaryLocalOperator(LocalOperator lop, BoundarySubset<Intersection> bs)
      : lop_(std::move(lop))
      , bs_(std::move(bs))
    {}

    /// Bind the local-operators to the element if the element has boundary intersections
    template <class Element>
    void bind(Element const& element)
    {
      if ((bound_ = element.hasBoundaryIntersections(), bound_))
        lop_.bind(element);
    }

    /// Unbind the local-operators from the element
    void unbind()
    {
      if (bound_) {
        lop_.unbind();
        bound_ = false;
      }
    }

    /**
     * \brief Assemble a local element matrix on the given intersection if it belongs to the
     * boundary subset.
     *
     * \param contextGeo  Container for geometry related data, \ref ContextGeometry
     * \param args...     Parameters for the assemble method in the local operator.
     **/
    template <class... Args>
    void assemble(ContextGeometry<Intersection> const& cg, Args&&... args) const
    {
      if (bound_ && bs_(cg.context()))
        lop_.assemble(cg,FWD(args)...);
    }

  private:
    LocalOperator lop_;
    BoundarySubset<Intersection> bs_;
    bool bound_ = false;
  };


  /**
   * \brief An operator to be assembled on a boundary intersection belonging to a given
   * boundary subset.
   *
   * \tparam Operator   The operator to be restricted to the boundary.
   * \tparam Intersection The type of the intersection the operator is restricted to.
   **/
  template <class Operator, class Intersection>
  class BoundaryOperator
  {
  public:
    BoundaryOperator(Operator op, BoundarySubset<Intersection> bs)
      : op_(std::move(op))
      , bs_(std::move(bs))
    {}

    /// Update the operator data on a \ref GridView
    template <class GridView>
    void update(GridView const& gridView)
    {
      op_.update(gridView);
    }

    /// Transform an operator into a local-operator
    friend auto localOperator(BoundaryOperator const& bop)
    {
      return BoundaryLocalOperator{localOperator(bop.op_), bop.bs_};
    }

  private:
    Operator op_;
    BoundarySubset<Intersection> bs_;
  };

} // end namespace AMDiS
