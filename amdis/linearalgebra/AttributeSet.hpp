#pragma once

#include <string>

namespace Dune
{
  // forward declarations
  template <class A>
  class ParallelLocalIndex;

  template <class TG, class TL, int N>
  class ParallelIndexSet;
}

namespace AMDiS
{
  struct DefaultAttributeSet
  {
    enum Type {
      unknown = 0,
      owner = 1,    //< DOF is owned by current processor
      overlap = 2,  //< DOF is shared with another processor who is owner of that DOF
      copy = 3      //< DOF is on front or ghost entity
    };
  };


  /// Default attribute set used to classify DOFs. Can be specialized for own
  /// Communication type `C`.
  template <class C>
  struct AttributeSet
  {
    using type = DefaultAttributeSet::Type;
  };

  // specialization for a Dune::ParallelLocalIndex
  template <class A>
  struct AttributeSet<Dune::ParallelLocalIndex<A>>
  {
    using type = A;
  };

  // specialization for a Dune::ParallelIndexSet
  template <class TG, class TL, int N>
  struct AttributeSet<Dune::ParallelIndexSet<TG,TL,N>>
  {
    using type = typename AttributeSet<TL>::type;
  };

} // end namespace AMDiS
