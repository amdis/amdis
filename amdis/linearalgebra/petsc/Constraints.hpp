#pragma once

#include <vector>

#include <amdis/linearalgebra/Constraints.hpp>
#include <amdis/linearalgebra/petsc/MatrixBackend.hpp>
#include <amdis/linearalgebra/petsc/VectorBackend.hpp>

namespace AMDiS
{
  template <class DofMap>
  struct Constraints<PetscMatrix<DofMap>>
  {
    using Matrix = PetscMatrix<DofMap>;
    using VectorX = PetscVector<DofMap>;
    using VectorB = PetscVector<DofMap>;


    /// Periodic boundary conditions
    /**
     * The periodic constraints are not implemented in a symmetric fashion
     **/
    template <class BitVector, class Associations>
    static void periodicBC(Matrix& mat, VectorX& x, VectorB& b, BitVector const& left, Associations const& left2right, bool setDiagonal = true)
    {
      thread_local std::vector<PetscInt> rows, associated_rows;
      rows.clear();
      associated_rows.clear();
      auto const& dofMap = mat.rowDofMap_;
      for (PetscInt i = 0; i < PetscInt(left.size()); ++i) {
        if (left[i]) {
          // get global row index
          PetscInt row_local[2] = {i, PetscInt(left2right.at(i))};
          PetscInt row[2] = {dofMap.global(row_local[0]), dofMap.global(row_local[1])};

          rows.push_back(row[0]);
          associated_rows.push_back(row[1]);

          // copy row to associated row
          PetscInt ncols;
          PetscInt const* cols;
          PetscScalar const* vals;
          MatGetRow(mat.matrix(), row[0], &ncols, &cols, &vals);
          MatSetValues(mat.matrix(), 1, &row[1], ncols, cols, vals, ADD_VALUES);
          MatRestoreRow(mat.matrix(), row[0], &ncols, &cols, &vals);
        }
      }

      MatAssemblyBegin(mat.matrix(), MAT_FLUSH_ASSEMBLY); // Maybe MAT_FLUSH_ASSEMBLY
      MatAssemblyEnd(mat.matrix(), MAT_FLUSH_ASSEMBLY);

      // clear the periodic rows and add a 1 on the diagonal
      MatSetOption(mat.matrix(), MAT_NO_OFF_PROC_ZERO_ROWS, PETSC_TRUE);
      MatZeroRows(mat.matrix(), rows.size(), rows.data(), setDiagonal ? 1 : 0, PETSC_NULL, PETSC_NULL);

      // Insert the -1 value at the off-diagonal associated columns
      // NOTE: the following is probably very slow
      for (std::size_t i = 0; i < rows.size(); ++i)
        MatSetValue(mat.matrix(), rows[i], associated_rows[i], -1, ADD_VALUES);
      MatAssemblyBegin(mat.matrix(), MAT_FINAL_ASSEMBLY); // Maybe MAT_FLUSH_ASSEMBLY
      MatAssemblyEnd(mat.matrix(), MAT_FINAL_ASSEMBLY);

      std::vector<PetscScalar> data(rows.size());

      // copy periodic rhs values to associated rows
      VecGetValues(b.vector(), rows.size(), rows.data(), data.data());
      VecSetValues(b.vector(), rows.size(), associated_rows.data(), data.data(), ADD_VALUES);
      VecAssemblyBegin(b.vector());
      VecAssemblyEnd(b.vector());

      // set the periodic rhs components to 0
      data.assign(rows.size(), 0);
      VecSetValues(b.vector(), rows.size(), rows.data(), data.data(), INSERT_VALUES);
      VecAssemblyBegin(b.vector());
      VecAssemblyEnd(b.vector());

      // symmetrize the solution vector
      VecGetValues(x.vector(), rows.size(), rows.data(), data.data());
      VecSetValues(x.vector(), rows.size(), associated_rows.data(), data.data(), INSERT_VALUES);
      VecAssemblyBegin(x.vector());
      VecAssemblyEnd(x.vector());
    }
  };

} // end namespace AMDiS
