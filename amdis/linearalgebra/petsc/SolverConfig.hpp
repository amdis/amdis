#pragma once


#include <petscpc.h>
#include <petscksp.h>
#include <string>


namespace AMDiS {
namespace PETSc {

// initialize the KSP solver from the initfile
void configKSP(KSP ksp, std::string prefix, bool initPC = true);

// initialize a direct solver from the initfile
void configDirectSolver(KSP ksp, std::string prefix);

// initialize the preconditioner pc from the initfile
void configPC(PC pc, std::string prefix);

// provide initfile parameters for some PETSc KSP parameters
void setKSPParameters(KSP ksp, char const* ksptype, std::string prefix);

}} // end namespace AMDiS::PETSc
