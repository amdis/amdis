install(FILES
    Access.hpp
    AttributeSet.hpp
    Constraints.hpp
    IndexDistribution.hpp
    LinearSolverInterface.hpp
    MatrixFacade.hpp
    ParallelIndexSet.hpp
    SparsityPattern.hpp
    SymmetryStructure.hpp
    Traits.hpp
    VectorBase.hpp
    VectorFacade.hpp
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/amdis/linearalgebra)

if (BACKEND STREQUAL "MTL")
  add_subdirectory("mtl")
elseif (BACKEND STREQUAL "EIGEN")
  add_subdirectory("eigen")
elseif (BACKEND STREQUAL "PETSC")
  add_subdirectory("petsc")
else ()
  add_subdirectory("istl")
endif ()
