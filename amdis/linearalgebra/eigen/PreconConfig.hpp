#pragma once

#include <string>

#include <Eigen/IterativeLinearSolvers>

#include <amdis/Initfile.hpp>

namespace AMDiS
{
  template <class Precon>
  struct PreconConfig
  {
    static void init(std::string const& prefix, Precon& solver) {}
  };

  template <class T, class I>
  struct PreconConfig<Eigen::IncompleteLUT<T,I>>
  {
    static void init(std::string const& prefix, Eigen::IncompleteLUT<T,I>& solver)
    {
      auto dropTol = Parameters::get<double>(prefix + "->drop tolerance");
      if (dropTol)
        solver.setDroptol(dropTol.value());

      auto fillFactor = Parameters::get<int>(prefix + "->fill factor");
      if (fillFactor)
        solver.setFillfactor(fillFactor.value());
    }
  };

} // end namespace AMDiS
