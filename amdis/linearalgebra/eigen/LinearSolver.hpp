#pragma once

#include <memory>
#include <string>

#include <amdis/CreatorMap.hpp>
#include <amdis/CreatorInterface.hpp>
#include <amdis/linearalgebra/LinearSolverInterface.hpp>

namespace AMDiS
{
  /// Implementation of \ref LinearSolverInterface for EIGEN solvers
  template <class Mat, class VecX, class VecY = VecX>
  class LinearSolver
      : public LinearSolverInterface<Mat,VecX,VecY>
  {
    using M = typename Mat::BaseMatrix;
    using X = typename VecX::BaseVector;
    using Y = typename VecX::BaseVector;

    using Map = CreatorMap<LinearSolverInterface<M,X,Y>>;

  public:
    LinearSolver(std::string const& name, std::string const& prefix)
      : solver_(named(Map::get(name, prefix))->createWithString(prefix))
    {}

    /// Implements \ref LinearSolverInterface::init()
    void init(Mat const& A) override
    {
      solver_->init(A.matrix());
    }

    /// Implements \ref LinearSolverInterface::finish()
    void finish() override
    {
      solver_->finish();
    }

    /// Implements \ref LinearSolverInterface::apply()
    void apply(VecX& x, VecY const& b, Dune::InverseOperatorResult& stat) override
    {
      solver_->apply(x.vector(), b.vector(), stat);
    }

  private:
    std::shared_ptr<LinearSolverInterface<M,X,Y>> solver_;
  };

} // end namespace AMDiS
