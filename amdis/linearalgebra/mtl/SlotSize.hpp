#pragma once

#include <algorithm>
#include <cassert>

#include <dune/common/math.hh>

#include <amdis/Observer.hpp>
#include <amdis/common/Index.hpp>
#include <amdis/common/Math.hpp>
#include <amdis/linearalgebra/SymmetryStructure.hpp>

namespace AMDiS
{
  class SlotSize
  {
  public:
    template <class RowBasis, class ColBasis>
    SlotSize(RowBasis const& rowBasis, ColBasis const& colBasis,
             SymmetryStructure symmetry = SymmetryStructure::unknown)
      : rows_(rowBasis.dimension())
      , cols_(colBasis.dimension())
      , symmetry_(symmetry)
    {
      init(rowBasis, colBasis);
    }

    /// Number of rows in the matrix
    std::size_t rows() const
    {
      return rows_;
    }

    /// Number of columns in the matrix
    std::size_t cols() const
    {
      return cols_;
    }

    /// Estimate of the non-zeros per row
    std::size_t rowSizeEstimate() const
    {
      return estRowSize_;
    }

    /// Symmetry of the matrix entries
    SymmetryStructure symmetry() const
    {
      return symmetry_;
    }

  protected:
    // estimate the number of columns by multiplying the maximal node size with the
    // number of element surrounding a vertex. This number is approximated by the
    // number of simplices surrounding a vertex in a kuhn triangulation
    template <class RowBasis, class ColBasis>
    void init(RowBasis const& rowBasis, ColBasis const& colBasis)
    {
      using GridView = typename RowBasis::GridView;
      static std::size_t surrounding
        = Math::pow<GridView::dimension>(2) * Dune::factorial(int(GridView::dimension));

      estRowSize_ = std::min(cols_, colBasis.localView().maxSize() * surrounding);
    }

  private:
    std::size_t rows_;
    std::size_t cols_;
    std::size_t estRowSize_;
    SymmetryStructure symmetry_;
  };

} // end namespace AMDiS
