#pragma once

#include <memory>
#include <string>
#include <utility>

#include <amdis/CreatorInterface.hpp>
#include <amdis/CreatorMap.hpp>
#include <amdis/Output.hpp>
#include <amdis/linearalgebra/mtl/BlockDiagonalPreconditioner.hpp>
#include <amdis/linearalgebra/mtl/MatrixBackend.hpp>
#include <amdis/linearalgebra/mtl/VectorBackend.hpp>

#include "SIMPLEPreconditioner.hpp"

namespace AMDiS
{
  /// \brief SIMPLER-preconditioner for the Navier-Stokes equation
  template <class M, class V, class Basis>
  class SIMPLERPreconditioner
      : public BlockDiagonalPreconditioner<M, V, Basis>
  {
    using Super = BlockDiagonalPreconditioner<M, V, Basis>;
    using Self = SIMPLERPreconditioner;

    using PreconBase = PreconditionerInterface<M,V,V>;
    using GridView = typename Basis::GridView;
    using Element = typename GridView::template Codim<0>::Entity;

  public:
    /// A creator to be used instead of the constructor.
    struct Creator : CreatorInterfaceName<PreconBase>
    {
      using Interface = PreconBase;

      Creator(std::shared_ptr<Basis> basis)
        : basis_(std::move(basis))
      {}

      std::unique_ptr<Interface> createWithString(std::string prefix) override
      {
        return std::make_unique<Self>(std::move(prefix), basis_);
      }

      std::shared_ptr<Basis> basis_;
    };

  public:
    /// Constructor
    SIMPLERPreconditioner(std::string const& prefix, std::shared_ptr<Basis> basis)
      : Super(prefix, std::move(basis))
      , prefix_(prefix)
    {}

    /// Extract the diagonal blocks of the fullMatrix into new matrices
    void init(M const& fullMatrix) override
    {
      Super::initBlocks(fullMatrix);

      // -D^(-1)
      Dinv_ = diagonal(-diagonal(Super::subMatrix(0,0)));
      invert_diagonal(Dinv_);

      // schur-complement matrix S = -B*D^(-1)*B^T
      Super::subMatrix(1,1) = Super::subMatrix(1,0) * Dinv_ * Super::subMatrix(0,1);

      Super::initSubPrecons();
    }

    void solve(V const& x, V& y) const override
    {
      y.checked_change_resource(x);
      assert(Super::rows() == 2);

      V const x_0(Super::subVector(x, Super::rowIndices(0)));
      V const x_1(Super::subVector(x, Super::rowIndices(1)));
      V y_0(resource(x_0));
      V y_1(resource(x_1));

      auto const& Bt = Super::subMatrix(0,1);
      auto const& B  = Super::subMatrix(1,0);

      V tmp_0(resource(x_0));
      V tmp_1(resource(x_1));
      V dy_1(resource(x_1));

      // y1' = S^(-1) (b1 - B*D^(-1)*b0)
      tmp_1 = x_1 + B * V(Dinv_ * x_0);
      Super::subPrecon_[1]->solve(tmp_1, y_1);

      // y0' = A_00^(-1) (b0 - B^T*y1')
      tmp_0 = x_0 - Bt * y_1;
      Super::subPrecon_[0]->solve(tmp_0, y_0);

      // dy1 = S^(-1) (b1 - B * y0')
      tmp_1 = x_1 - B * y_0;
      Super::subPrecon_[1]->solve(tmp_1, dy_1);

      // y0 = y0' - D^(-1)*B^T*dy1
      y_0 += Dinv_ * V(Bt * dy_1);
      // y1 = y1' + dy1
      y_1 += dy_1;

      Super::subVector(y, Super::colIndices(0)) = y_0;
      Super::subVector(y, Super::colIndices(1)) = y_1;
    }

    void adjoint_solve(V const& x, V& y) const override
    {
      error_exit("Not yet implemented");
    }

  protected:
    std::string prefix_;
    M Dinv_;
  };


  template <class T = double, class Basis>
  void registerSIMPLERPreconditioner(std::shared_ptr<Basis> const& basis, std::string name = "SIMPLER")
  {
    using M = typename MTLSparseMatrix<T>::BaseMatrix;
    using V = typename MTLVector<T>::BaseVector;
    using Creator = typename SIMPLERPreconditioner<M, V, Basis>::Creator;
    CreatorMap<typename Creator::Interface>::addCreator(name, new Creator(basis));
  }

} // end namespace AMDiS
