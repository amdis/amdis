#pragma once

#include <memory>
#include <string>
#include <utility>

#include <amdis/BiLinearForm.hpp>
#include <amdis/CreatorInterface.hpp>
#include <amdis/CreatorMap.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/functions/GlobalBasis.hpp>
#include <amdis/linearalgebra/mtl/BlockDiagonalPreconditioner.hpp>

namespace AMDiS
{
  /// Schur-Complement preconditioner for the Stokes equation
  template <class M, class V, class Basis>
  class StokesPreconditioner
      : public BlockDiagonalPreconditioner<M, V, Basis>
  {
    using Super = BlockDiagonalPreconditioner<M, V, Basis>;
    using Self = StokesPreconditioner;

    using PreconBase = PreconditionerInterface<M,V,V>;
    using GridView = typename Basis::GridView;
    using Element = typename GridView::template Codim<0>::Entity;

  public:
    /// A creator to be used instead of the constructor.
    struct Creator : CreatorInterfaceName<PreconBase>
    {
      using Interface = PreconBase;

      Creator(std::shared_ptr<Basis> basis)
        : basis_(std::move(basis))
      {}

      std::unique_ptr<Interface> createWithString(std::string prefix) override
      {
        return std::make_unique<Self>(std::move(prefix), basis_);
      }

      std::shared_ptr<Basis> basis_;
    };

  public:
    /// Constructor
    StokesPreconditioner(std::string const& prefix, std::shared_ptr<Basis> const& basis)
      : Super(prefix, basis)
      , basis_(basis)
    {}

    /// Extract the diagonal blocks of the fullMatrix `mat` into new matrices
    void init(M const& mat) override
    {
      Super::initBlocks(mat, true);

      auto&& pressurePreBasis = basis_->preBasis().subPreBasis(Dune::Indices::_1);
      GlobalBasis pressureBasis{pressurePreBasis};
      BiLinearForm matM(pressureBasis);

      matM.addOperator(zot(1.0));
      matM.setSymmetryStructure(SymmetryStructure::spd);
      matM.assemble();

      // set mass matrix to pressure diagonal block
      Super::subMatrix(1,1) = matM.impl().matrix();
      Super::initSubPrecons();
    }

  protected:
    std::shared_ptr<Basis> basis_;
  };


  template <class T = double, class Basis>
  void registerStokesPreconditioner(std::shared_ptr<Basis> const& basis, std::string name = "stokes")
  {
    using M = typename MTLSparseMatrix<T>::BaseMatrix;
    using V = typename MTLVector<T>::BaseVector;
    using Creator = typename StokesPreconditioner<M, V, Basis>::Creator;
    CreatorMap<typename Creator::Interface>::addCreator(name, new Creator(basis));
  }

} // end namespace AMDiS
