#pragma once

#include <dune/common/typeutilities.hh>
#include <dune/functions/backends/istlvectorbackend.hh>

namespace AMDiS {
namespace Traits {

/// Unified accessor class
struct Access
{
private:
  // if C implements the vector facade interface
  template <class C, class MI>
  auto operator()(C const& c, MI const& mi, Dune::PriorityTag<3>) const
    -> decltype(c.get(mi))
  {
    return c.get(mi);
  }

  // if C supports multi-index access
  template <class C, class MI>
  auto operator()(C const& c, MI const& mi, Dune::PriorityTag<2>) const
    -> decltype(c[mi])
  {
    return c[mi];
  }

  // if C implements the vector facade interface
  template <class C, class MI>
  auto operator()(C const& c, MI const& mi, Dune::PriorityTag<1>) const
    -> decltype(c.at(mi))
  {
    return c.at(mi);
  }

  // fallback implementation for simple vectors
  template <class C, class MI>
  auto operator()(C const& c, MI const& mi, Dune::PriorityTag<0>) const
    -> decltype(Dune::Functions::istlVectorBackend(c)[mi])
  {
    return Dune::Functions::istlVectorBackend(c)[mi];
  }

public:
  template <class C, class MI>
  auto operator()(C const& c, MI const& mi) const
  {
    return (*this)(c,mi,Dune::PriorityTag<5>{});
  }
};

}} // end namespace AMDiS::Traits
