#pragma once

#include <amdis/Output.hpp>

namespace AMDiS
{
  // forward declaration
  template <class RB, class CB, class T, class Traits>
  class BiLinearForm;


  template <class Matrix>
  struct Constraints
  {
    template <class Mat, class Sol, class Rhs, class BitVec, class Assoc>
    static void periodicBC(Mat& /*matrix*/, Sol& /*solution*/, Rhs& /*rhs*/, BitVec const& /*left*/, Assoc const& /*association*/, bool /*setDiagonal*/ = true)
    {
      /* do nothing */
      warning("periodicBC not implemented for this matrix type.");
    }
  };

  template <class Mat, class Sol, class Rhs, class BitVec, class Assoc>
  void periodicBC(Mat& matrix, Sol& solution, Rhs& rhs, BitVec const& left, Assoc const& association, bool setDiagonal = true)
  {
    Constraints<Mat>::periodicBC(matrix, solution, rhs, left, association, setDiagonal);
  }


  template <class RB, class CB, class T, class Traits>
  struct Constraints<BiLinearForm<RB,CB,T,Traits>>
  {
    using Matrix = BiLinearForm<RB,CB,T,Traits>;

    template <class Sol, class Rhs, class BitVec, class Assoc>
    static void periodicBC(Matrix& matrix, Sol& solution, Rhs& rhs, BitVec const& left, Assoc const& association, bool setDiagonal = true)
    {
      AMDiS::periodicBC(matrix.impl(), solution.impl(), rhs.impl(), left, association, setDiagonal);
    }
  };

} // end namespace AMDiS
