#pragma once

#include <dune/istl/bvector.hh>
#include <dune/functions/backends/istlvectorbackend.hh>

#include <amdis/Output.hpp>
#include <amdis/common/FakeContainer.hpp>
#include <amdis/linearalgebra/VectorBase.hpp>
#include <amdis/typetree/MultiIndex.hpp>

namespace AMDiS
{
  template <class T, class = void>
  struct BlockVectorType
  {
    using type = Dune::FieldVector<T,1>;
  };

  template <class T>
  struct BlockVectorType<T, typename T::field_type>
  {
    using type = T;
  };

	template <class T>
	class ISTLBlockVector
      : public VectorBase<ISTLBlockVector<T>>
	{
    using Self = ISTLBlockVector;

	public:
    /// The vector type of the underlying base vector
	  using BaseVector = Dune::BlockVector<typename BlockVectorType<T>::type>;

    /// The type of the elements of the DOFVector
	  using block_type = typename BaseVector::block_type;

    /// The type of the elements of the DOFVector
    using value_type = T;

    /// The underlying field type
    using field_type = typename block_type::field_type;

    /// The index/size - type
	  using size_type  = typename BaseVector::size_type;

  public:
    /// Constructor. Constructs new BaseVector.
    template <class Comm,
      Dune::disableCopyMove<ISTLBlockVector,Comm> = 0>
    explicit ISTLBlockVector(Comm const&) {}

    ISTLBlockVector() = default;

    /// Return the data-vector \ref vector
    BaseVector const& vector() const
    {
      return vector_;
    }

    /// Return the data-vector \ref vector
    BaseVector& vector()
    {
      return vector_;
    }

    /// Return the current size of the \ref vector_
    std::size_t size() const
    {
      return vector_.size();
    }

    /// Resize the \ref vector_ to the size given by the \p sizeProvider
    // NOTE: this resize works recursively on the hierarchy of the vector
    template <class Basis>
    void init(Basis const& basis, bool clear)
    {
      Dune::Functions::istlVectorBackend(vector_).resize(basis);
      if (clear)
        vector_ = 0;
    }

    /// Access the entry \p idx of the \ref vector_ with write-access.
    template <class MultiIndex>
    value_type& at(MultiIndex const& idx)
    {
      return Dune::Functions::istlVectorBackend(vector_)[idx];
    }

    /// Access the entry \p idx of the \ref vector_ with read-access.
    template <class MultiIndex>
    value_type const& at(MultiIndex const& idx) const
    {
      return Dune::Functions::istlVectorBackend(vector_)[idx];
    }

    /// \name Vector space operations
    /// @{

    /// \brief Implements *this = *this + alpha * x
    Self& axpy (const value_type& alpha, const Self& x)
    {
      vector_.axpy(alpha, x.vector_);
      return *this;
    }

    /// \brief Implements *this = alpha * (*this) + x
    Self& aypx (const value_type& alpha, const Self& x)
    {
      vector_ *= alpha;
      vector_ += x.vector_;
      return *this;
    }

    /// \brief Set all entries to alpha
    void set (const value_type& alpha)
    {
      vector_ = alpha;
    }

    /// \brief scale all entries by the factor alpha
    void scale (const value_type& alpha)
    {
      vector_ *= alpha;
    }

    /// @}

	private:
    BaseVector vector_;
	};


  namespace Recursive
  {
    template <class T>
    struct ForEach<ISTLBlockVector<T>> : ForEach<VectorBase<ISTLBlockVector<T>>> {};

    template <class T>
    struct Transform<ISTLBlockVector<T>> : Transform<VectorBase<ISTLBlockVector<T>>> {};

    template <class T>
    struct InnerProduct<ISTLBlockVector<T>> : InnerProduct<VectorBase<ISTLBlockVector<T>>> {};

  } // end namespace Recursive
} // end namespace AMDiS
