#pragma once

#include <string>

#include <amdis/ProblemInstatBase.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/common/TypeTraits.hpp>

#include <dune/grid/yaspgrid.hh>

namespace AMDiS
{
  // forward declarations
  class AdaptInfo;

  /**
   * \ingroup Problem
   *
   * \brief
   * Standard implementation of ProblemTimeInterface for a time
   * dependent problems.
   */
  template <class Traits>
  class ProblemInstat
      : public ProblemInstatBase
  {
    using Self        = ProblemInstat;
    using ProblemType = ProblemStat<Traits>;

    using SolutionVector = typename ProblemType::SolutionVector;

  public:
    /// Constructs a ProblemInstat with prob as its stationary problem, stored as reference.
    ProblemInstat(std::string const& name, ProblemType& prob)
      : ProblemInstatBase(name)
      , problemStat_(&prob)
    {}

    /// Constructor. Stores a reference to prob and initialProb.
    ProblemInstat(std::string const& name, ProblemType& prob, ProblemStatBase& initialProb)
      : ProblemInstatBase(name, initialProb)
      , problemStat_(&prob)
    {}

    /// Initialisation of the problem.
    void initialize(Flag initFlag = INIT_NOTHING);

    /// Implementation of \ref ProblemTimeInterface::initTimestep().
    void initTimestep(AdaptInfo& adaptInfo) override;

    /// Implementation of \ref ProblemTimeInterface::closeTimestep().
    void closeTimestep(AdaptInfo& adaptInfo) override;

    /// Returns \ref problemStat.
    ProblemType&       problemStat()       { return *problemStat_; }
    ProblemType const& problemStat() const { return *problemStat_; }

    /// Returns const-ref of \ref oldSolution.
    std::shared_ptr<SolutionVector const> oldSolutionVector() const
    {
      test_exit_dbg(bool(oldSolution_),
        "OldSolution need to be created. Call initialize with INIT_UH_OLD.");
      return oldSolution_;
    }

    /// Returns ref of \ref oldSolution.
    std::shared_ptr<SolutionVector> oldSolutionVector()
    {
      test_exit_dbg(bool(oldSolution_),
        "OldSolution need to be created. Call initialize with INIT_UH_OLD.");
      return oldSolution_;
    }

    /// Return a const view to a oldSolution component
    /**
     * \tparam Range  The range type return by evaluating the view in coordinates. If not specified,
     *                it is automatically selected using \ref RangeType_t template.
     **/
    template <class Range = void, class... Indices>
    auto oldSolution(Indices... ii) const
    {
      test_exit_dbg(bool(oldSolution_),
        "OldSolution need to be created. Call initialize with INIT_UH_OLD.");
      return valueOf<Range>(*oldSolution_, ii...);
    }

    /// Implementation of \ref ProblemTimeInterface::transferInitialSolution().
    void transferInitialSolution(AdaptInfo& adaptInfo) override;

  protected:
    /// Used in \ref initialize() to create the \ref oldSolution_.
    void createUhOld();

  protected:
    /// Space problem solved in each timestep. (non-owning pointer)
    ProblemType* problemStat_;

    /// Solution of the last timestep.
    std::shared_ptr<SolutionVector> oldSolution_;
  };


  // Deduction guides
  template <class Traits>
  ProblemInstat(std::string const& name, ProblemStat<Traits>& prob)
    -> ProblemInstat<Traits>;

  template <class Traits>
  ProblemInstat(std::string const& name, ProblemStat<Traits>& prob, ProblemStatBase& initialProb)
    -> ProblemInstat<Traits>;


  // mark template as explicitly instantiated in cpp file
  extern template class ProblemInstat<LagrangeBasis<Dune::YaspGrid<2>,1>>;
  extern template class ProblemInstat<LagrangeBasis<Dune::YaspGrid<2>,1,1>>;

} // end namespace AMDiS

#include "ProblemInstat.inc.hpp"
