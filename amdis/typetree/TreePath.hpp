#pragma once

#include <cassert>
#include <sstream>
#include <string>
#include <type_traits>

#include <dune/common/version.hh>
#include <dune/typetree/treepath.hh>
#include <dune/typetree/typetraits.hh>

#include <amdis/common/Apply.hpp>
#include <amdis/common/Literals.hpp>
#include <amdis/common/Logical.hpp>

namespace AMDiS {

using RootTreePath = Dune::TypeTree::HybridTreePath<>;

namespace Concepts {

/** \addtogroup Concepts
 *  @{
 **/

namespace Definition
{
  template <class Index>
  struct IsPreTreePath
    : std::is_integral<Index>
  {};

  template <class Index, Index I>
  struct IsPreTreePath<std::integral_constant<Index, I>>
    : std::is_integral<Index>
  {};

  template <class Index, Index... I>
  struct IsPreTreePath<std::integer_sequence<Index, I...>>
    : std::is_integral<Index>
  {};

  template <class... Indices>
  struct IsPreTreePath<std::tuple<Indices...>>
    : std::conjunction<std::is_integral<Indices>...>
  {};

} // end namespace Definition

template <class TP>
constexpr bool PreTreePath = Dune::TypeTree::IsTreePath<TP>::value || Definition::IsPreTreePath<TP>::value;

template <class TP>
using PreTreePath_t = bool_t<PreTreePath<TP>>;

/** @} **/

} // end namespace Concepts

namespace Impl
{
  template <class Index,
    std::enable_if_t<std::is_integral_v<Index>, int> = 0>
  constexpr std::size_t treePathIndex(Index i)
  {
    return std::size_t(i);
  }

  template <class Index, Index i,
    std::enable_if_t<std::is_integral_v<Index>, int> = 0>
  constexpr auto treePathIndex(std::integral_constant<Index,i>)
  {
    return std::integral_constant<std::size_t,std::size_t(i)>{};
  }

} // end namespace Impl

#ifdef DOXYGEN

/// \brief Converts a sequence of indices into a HybridTreePath
/**
 * Converts an integer, an integralconstant, a sequence of those, or a TreePath
 * into an \ref Dune::TypeTree::HybridTreePath that is used in GlobalBasis traversal.
 *
 * **Requirements:**
 * The arguments can be one or more of
 * - integer type (`int, std::size_t`)
 * - integral constant (`std::integral_constant<[int|std::size_t],i>, index_t<i>`)
 * or one of dune treepath types, e.g.
 * - any Dune TreePath (`TreePath<std::size_t...>, HybridTreePath<class... T>`)
 * - a `std::tuple` type
 *
 * **Example:**
 * ```
 * makeTreePath(0,1,2),
 * makeTreePath(int_<2>, 0),
 * makeTreePath(1, index_<2>),
 * makeTreePath(hybridTreePath(0,1,2)),
 * makeTreePath(std::tuple{0,index_<2>,2})
 * ```
 **/
template <class PreTreePath>
constexpr auto makeTreePath(PreTreePath tp);

#else // DOXYGEN

template <class... Indices>
constexpr auto makeTreePath(Indices... ii)
  -> decltype( Dune::TypeTree::hybridTreePath(Impl::treePathIndex(ii)...) )
{
  return Dune::TypeTree::hybridTreePath(Impl::treePathIndex(ii)...);
}

constexpr auto makeTreePath()
{
  return Dune::TypeTree::hybridTreePath();
}

template <class Index, Index... I>
constexpr auto makeTreePath(std::integer_sequence<Index, I...>)
{
  return makeTreePath(std::integral_constant<std::size_t, std::size_t(I)>{}...);
}

template <class... T>
constexpr auto makeTreePath(std::tuple<T...> const& tp)
{
  return std::apply([](auto... ii) { return makeTreePath(ii...); }, tp);
}

template <class... T>
constexpr auto const& makeTreePath(Dune::TypeTree::HybridTreePath<T...> const& tp)
{
  return tp;
}

template <std::size_t... I>
constexpr auto makeTreePath(Dune::TypeTree::StaticTreePath<I...>)
{
  return Dune::TypeTree::hybridTreePath(std::integral_constant<std::size_t, I>{}...);
}

#endif // DOXYGEN


/// Literal to create treepath, e.g. 021_tp -> HybridTreePath<index_t<0>,index_t<2>,index_t<1>>
template <char... digits>
constexpr auto operator"" _tp()
{
  return Dune::TypeTree::hybridTreePath(std::integral_constant<std::size_t,Impl::char2digit(digits)>{}...);
}


// convert a treepath into a string, comma-separated
template <class... T>
std::string to_string(Dune::TypeTree::HybridTreePath<T...> const& tp)
{
  auto entry = [&](auto i) { return std::to_string(std::size_t(Dune::TypeTree::treePathEntry<i>(tp))); };
  auto first = [&] { return entry(std::integral_constant<std::size_t,0>()); };

  return Ranges::applyIndices<1,sizeof...(T)>([&](auto... i) -> std::string {
    return (first() +...+ ("," + entry(i)));
  });
}

inline std::string to_string(Dune::TypeTree::HybridTreePath<> const&)
{
  return "";
}

// convert a treepath into an array
template <class... T>
auto to_array(Dune::TypeTree::HybridTreePath<T...> const& tp)
{
  return Ranges::applyIndices<sizeof...(T)>([&](auto... i) {
    return std::array<std::size_t,sizeof...(T)>{std::size_t(Dune::TypeTree::treePathEntry<i>(tp))...};
  });
}

/// Extract the first entry in the treepath
template <class T0, class... T>
auto pop_front(Dune::TypeTree::HybridTreePath<T0,T...> const& tp)
{
  return Ranges::applyIndices<sizeof...(T)>([&](auto... i) {
    return Dune::TypeTree::hybridTreePath(Dune::TypeTree::treePathEntry<i+1>(tp)...);
  });
}

/// Extract the last entry in the treepath
template <class... T, class TN>
auto pop_back(Dune::TypeTree::HybridTreePath<T...,TN> const& tp)
{
  return Ranges::applyIndices<sizeof...(T)>([&](auto... i) {
    return Dune::TypeTree::hybridTreePath(Dune::TypeTree::treePathEntry<i>(tp)...);
  });
}

/// Concatenate two treepaths
template <class... S, class... T>
auto cat(Dune::TypeTree::HybridTreePath<S...> const& tp0, Dune::TypeTree::HybridTreePath<T...> const& tp1)
{
  return Dune::TypeTree::HybridTreePath<S...,T...>(std::tuple_cat(tp0._data,tp1._data));
}

} // end namespace AMDiS
