
install(FILES
    Concepts.hpp
    FiniteElementType.hpp
    MultiIndex.hpp
    RangeType.hpp
    Traversal.hpp
    TreePath.hpp
DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/amdis/typetree)
