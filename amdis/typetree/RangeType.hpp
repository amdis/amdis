#pragma once

#include <type_traits>
#include <utility>

#include <dune/common/fvector.hh>
#include <dune/common/tuplevector.hh>
#include <dune/common/typetraits.hh>

#include <amdis/common/Index.hpp>
#include <amdis/common/TypeTraits.hpp>

namespace Dune
{
  template <class... T>
  struct FieldTraits<TupleVector<T...>>
  {
    using field_type = std::common_type_t<typename FieldTraits<T>::field_type...>;
    using real_type = std::common_type_t<typename FieldTraits<T>::real_type...>;
  };
}

namespace AMDiS
{
  namespace Impl
  {
    template <class Node, class R, class NodeTag, class Default>
    struct RangeTypeImpl
    {
      using type = Default;
      static type create(Node const& node)
      {
        type range{};
        return range;
      }
    };

    template <class Node, class R, class NodeTag>
    struct RangeTypeImpl<Node,R,NodeTag,void>
    {
      static_assert( Dune::AlwaysFalse<NodeTag>::value, "Unknown node-type for range definition" );
    };
  }


  template <class Node, class R = double, class Default = void>
  using RangeType = Impl::RangeTypeImpl<Node, R, typename Node::NodeTag, Default>;

  /// \brief Range type of a node in the basis tree, composed of the leaf basis
  /// range types.
  /**
   * Generate the range type by recursively combining leaf range types to a
   * hybrid node range type. Range types for PowerNodes are thereby constructed
   * as Dune::FieldVector of child range types. CompositeNodes produce a
   * TupleVector of the difference child range types.
   *
   * \tparam Node  Type of a basis-tree node
   * \tparam R     Coefficient type [double]
   * \tparam Default  Fallback type [void]
   **/
  template <class Node, class R = double, class Default = void>
  using RangeType_t =
    typename Impl::RangeTypeImpl<Node, R, typename Node::NodeTag, Default>::type;


  namespace Impl
  {
    // Leaf node
    template <class Node, class R>
    struct RangeTypeImpl<Node, R, Dune::TypeTree::LeafNodeTag, void>
    {
      using LocalBasis = typename Node::FiniteElement::Traits::LocalBasisType;
      using T = typename LocalBasis::Traits::RangeType;
      using type = remove_cvref_t<decltype(std::declval<R>() * std::declval<T>())>;
      static type create(Node const& node)
      {
        return type(0);
      }
    };

    // Power node
    template <class Node, class R>
    struct RangeTypeImpl<Node, R, Dune::TypeTree::PowerNodeTag, void>
    {
      using ChildNode = typename Node::ChildType;

      template <bool childIsLeaf, class ChildRangeType>
      struct FlatType {
        using type = Dune::FieldVector<ChildRangeType, int(Node::degree())>;
      };

      template <class T>
      struct FlatType<true, Dune::FieldVector<T,1>> {
        using type = Dune::FieldVector<T, int(Node::degree())>;
      };

      using type = typename FlatType<ChildNode::isLeaf, RangeType_t<ChildNode,R>>::type;
      static type create(Node const& node)
      {
        return Dune::unpackIntegerSequence([&](auto... indices) {
          return type{RangeType<ChildNode,R>::create(node.child(indices))...};
        }, std::make_index_sequence<std::size_t(Node::degree())>());
      }
    };

    // Power node
    template <class Node, class R>
    struct RangeTypeImpl<Node, R, Dune::TypeTree::DynamicPowerNodeTag, void>
    {
      using ChildNode = typename Node::ChildType;

      template <bool childIsLeaf, class ChildRangeType>
      struct FlatType {
        using type = Dune::DynamicVector<ChildRangeType>;
      };

      template <class T>
      struct FlatType<true, Dune::FieldVector<T,1>> {
        using type = Dune::DynamicVector<T>;
      };

      using type = typename FlatType<ChildNode::isLeaf, RangeType_t<ChildNode,R>>::type;
      static type create(Node const& node)
      {
        type vector(node.degree());
        for (std::size_t i = 0; i < std::size_t(node.degree()); ++i)
          vector[i] = RangeType<ChildNode,R>::create(node.child(i));
        return vector;
      }
    };

    // Composite node
    template <class Node, class R>
    struct RangeTypeImpl<Node, R, Dune::TypeTree::CompositeNodeTag, void>
    {
      template <std::size_t J>
      using ChildNode = typename Node::template Child<J>::type;
      template <std::size_t J>
      using ChildRange = RangeType<ChildNode<J>,R>;

      template <class Idx> struct RangeTypeGenerator;
      template <std::size_t... I>
      struct RangeTypeGenerator<Indices<I...>>
      {
        using type = Dune::TupleVector<typename ChildRange<I>::type...>;
      };

      using type = typename RangeTypeGenerator<std::make_index_sequence<std::size_t(Node::degree())>>::type;
      static type create(Node const& node)
      {
        return Dune::unpackIntegerSequence([&](auto... indices) {
          return type{ChildRange<decltype(indices)::value>::create(node.child(indices))...};
        }, std::make_index_sequence<std::size_t(Node::degree())>());
      }
    };

  } // end namespace Impl

} // end namespace AMDiS
