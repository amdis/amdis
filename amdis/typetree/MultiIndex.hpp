#pragma once

#include <dune/common/reservedvector.hh>
#include <dune/common/version.hh>
#include <dune/functions/common/multiindex.hh>

namespace AMDiS
{
  namespace Traits
  {
    template <class T>
    struct IsFlatIndex
        : std::is_integral<T> {};

    template <class I>
    struct IsFlatIndex<Dune::Functions::StaticMultiIndex<I,1> >
        : std::true_type {};

    template <class I>
    struct IsFlatIndex<std::array<I,1> >
        : std::true_type {};

    template <class I>
    struct IsFlatIndex<Dune::ReservedVector<I,1> >
        : std::true_type {};
  }

  inline std::size_t flatMultiIndex(std::size_t idx)
  {
    return idx;
  }

  inline std::size_t flatMultiIndex(Dune::Functions::StaticMultiIndex<std::size_t,1> const& idx)
  {
    return idx[0];
  }

  inline std::size_t flatMultiIndex(Dune::ReservedVector<std::size_t,1> const& idx)
  {
    return idx[0];
  }

  inline std::size_t flatMultiIndex(std::array<std::size_t,1> const& idx)
  {
    return idx[0];
  }

} // end namespace AMDiS
