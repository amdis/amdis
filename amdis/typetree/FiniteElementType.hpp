#pragma once

#include <dune/typetree/nodetags.hh>

#include <amdis/common/Tags.hpp>

namespace AMDiS
{
  namespace Impl
  {
    template <class Node, class NodeTag>
    struct FiniteElementTypeImpl
    {
      using type = tag::unknown;
    };
  }

  template <class Node>
  using FiniteElementType =
    Impl::FiniteElementTypeImpl<Node, typename Node::NodeTag>;

  template <class Node>
  using FiniteElementType_t = typename FiniteElementType<Node>::type;

  namespace Impl
  {
    // Leaf node
    template <class Node>
    struct FiniteElementTypeImpl<Node, Dune::TypeTree::LeafNodeTag>
    {
      using type = typename Node::FiniteElement;
    };

    // Power node
    template <class Node>
    struct FiniteElementTypeImpl<Node, Dune::TypeTree::PowerNodeTag>
    {
      using ChildNode = typename Node::template Child<0>::type;
      using type = FiniteElementType_t<ChildNode>;
    };

    // Composite node
    template <class Node>
    struct FiniteElementTypeImpl<Node, Dune::TypeTree::CompositeNodeTag>
    {
      using type = tag::unknown; // no common FiniteElement type
    };

  } // end namespace Impl
} // end namespace AMDiS
