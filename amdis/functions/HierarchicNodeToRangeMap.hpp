#pragma once

#include <utility>
#include <type_traits>

#include <dune/common/concept.hh>
#include <dune/functions/common/indexaccess.hh>

#include <amdis/common/Concepts.hpp>
#include <amdis/common/DerivativeTraits.hpp>

namespace AMDiS
{
  /**
  * \brief A simple node to range map using the nested tree indices
  *
  * This map directly uses the tree path entries of the given
  * node to access the nested container.
  *
  * If the container does not provide any operator[] access,
  * it is simply forwarded for all nodes.
  */
  template <class TreePath, class Range>
  decltype(auto) hierarchicNodeToRangeMap(const TreePath& treePath, Range&& y)
  {
    if constexpr(Concepts::HasIndexAccess<Range, Dune::index_constant<0>>)
      // Specialization for ranges with operator[] access
      return Dune::Functions::resolveStaticMultiIndex(y, treePath);
    else
      // Specialization for non-container ranges
      return std::forward<Range>(y);
  }


  /**
   * A wrapper that transforms a given function F(Domain) using the
   * \ref hierarchicNodeToRangeMap restricted to a component given by
   * the TreePath.
   **/
  template <class TreePath, class F>
  struct HierarchicNodeWrapper
  {
    HierarchicNodeWrapper(TreePath const& tp, F const& f)
      : tp_{tp}
      , f_{f}
    {}

    template <class Domain>
    auto operator() (Domain const& x) const
    {
      return hierarchicNodeToRangeMap(tp_, Dune::MatVec::as_vector(f_(x)));
    }

    decltype(auto) friend derivative(HierarchicNodeWrapper<TreePath,F> const& t)
    {
      auto df = derivativeOf(t.f_, tag::jacobian{});
      df.bind(t.f_.localContext());
      return HierarchicNodeWrapper<TreePath,decltype(df)>(t.tp_, df);
    }

    TreePath tp_;
    F f_;
  };

} // end namespace AMDiS
