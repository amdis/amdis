#pragma once

#include <dune/common/indices.hh>
#include <dune/functions/functionspacebases/basistags.hh>
#include <dune/functions/functionspacebases/compositebasis.hh>
#include <dune/functions/functionspacebases/dynamicpowerbasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>

#include <amdis/functions/FlatPreBasis.hpp>

namespace AMDiS
{
  /// \brief Transform a PreBasis into one with an outer-level blocked index-merging strategy
  /// and all inner nodes with flat index-merging strategy.
  /**
   * This utility takes a pre-basis and converts recursively all index-merging strategies
   * into their blocked or flat analog, i.e. `BlockedInterleaved` is converted into
   * `FlatInterleaved` and `BlockedLexicographic` is transformed into `FlatLexicographic` or
   * vice versa. Thereby only the outer node is transformed into blocked, whereas all inner nodes
   * into flat pre-bases.
   *
   * This type-trait needs to be specialized for all PreBasis types that need special handling,
   * like PowerPreBasis or CompositePreBasis.
   *
   * \relates blockedPreBasis()
   * \relates BlockedPreBasis_t
   **/
  template <class PreBasis>
  struct BlockedPreBasis
  {
    using type = PreBasis;

    template <class PB>
    static type create(PB const& preBasis)
    {
      return {preBasis.gridView()};
    }

    /// Do not transform the preBasis if already blocked
    static PreBasis const& create(PreBasis const& preBasis)
    {
      return preBasis;
    }
  };

  /// \brief Type alias for blocked PreBasis
  template <class PreBasis>
  using BlockedPreBasis_t = typename BlockedPreBasis<PreBasis>::type;

  /// \brief Generator function for a blocked PreBasis
  template <class PreBasis>
  decltype(auto) blockedPreBasis(PreBasis const& preBasis)
  {
    return BlockedPreBasis<PreBasis>::create(preBasis);
  }


  /// \brief Define the blocked index-merging strategy for a given strategy `IMS`
  template <class IMS>
  struct BlockedIndexMergingStrategy
  {
    using type = IMS;
  };

  // specialization for FlatInterleaved
  template <>
  struct BlockedIndexMergingStrategy<Dune::Functions::BasisFactory::FlatInterleaved>
  {
    using type = Dune::Functions::BasisFactory::BlockedInterleaved;
  };

  // specialization for FlatLexicographic
  template <>
  struct BlockedIndexMergingStrategy<Dune::Functions::BasisFactory::FlatLexicographic>
  {
    using type = Dune::Functions::BasisFactory::BlockedLexicographic;
  };


  template <class IMS, class... SPB>
  struct BlockedPreBasis<Dune::Functions::CompositePreBasis<IMS, SPB...>>
  {
    using BIMS = Dune::Functions::BasisFactory::BlockedLexicographic;
    using type = Dune::Functions::CompositePreBasis<BIMS, FlatPreBasis_t<SPB>...>;

    template <class PreBasis>
    static type create(PreBasis const& preBasis)
    {
      return create(preBasis, std::index_sequence_for<SPB...>{});
    }

    template <class PreBasis, std::size_t... I>
    static type create(PreBasis const& preBasis, std::index_sequence<I...>)
    {
      return {FlatPreBasis<SPB>::create(preBasis.subPreBasis(Dune::index_constant<I>{}))...};
    }
  };

  template <class IMS, class SPB, std::size_t C>
  struct BlockedPreBasis<Dune::Functions::PowerPreBasis<IMS, SPB, C>>
  {
    using BIMS = typename BlockedIndexMergingStrategy<IMS>::type;
    using type = Dune::Functions::PowerPreBasis<BIMS, FlatPreBasis_t<SPB>, C>;

    template <class PreBasis>
    static type create(PreBasis const& preBasis)
    {
      return {FlatPreBasis<SPB>::create(preBasis.subPreBasis())};
    }
  };

  template <class IMS, class SPB>
  struct BlockedPreBasis<Dune::Functions::DynamicPowerPreBasis<IMS, SPB>>
  {
    using BIMS = typename BlockedIndexMergingStrategy<IMS>::type;
    using type = Dune::Functions::DynamicPowerPreBasis<BIMS, FlatPreBasis_t<SPB>>;

    template <class PreBasis>
    static type create(PreBasis const& preBasis)
    {
      return {FlatPreBasis<SPB>::create(preBasis.subPreBasis())};
    }
  };

} // end namespace AMDiS
