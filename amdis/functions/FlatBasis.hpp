#pragma once

#include <type_traits>
#include <utility>

#include <dune/functions/functionspacebases/defaultglobalbasis.hh>
#include <amdis/functions/FlatPreBasis.hpp>

namespace AMDiS
{
  // forward declaration
  template <class PB>
  class GlobalBasis;

  // define the type of the flat basis
  template <class Basis>
  struct FlatBasis;

  template <class Basis>
  using FlatBasis_t = typename FlatBasis<remove_cvref_t<Basis>>::type;

  template <class PB>
  struct FlatBasis<Dune::Functions::DefaultGlobalBasis<PB>>
  {
    using type = Dune::Functions::DefaultGlobalBasis<typename FlatPreBasis<PB>::type>;

    template <class Basis>
    static type create(Basis const& basis)
    {
      return type{flatPreBasis(basis.preBasis())};
    }
  };

  template <class PB>
  struct FlatBasis<GlobalBasis<PB>>
  {
    using type = GlobalBasis<typename FlatPreBasis<PB>::type>;

    template <class Basis>
    static type create(Basis const& basis)
    {
      return type{flatPreBasis(basis.preBasis())};
    }
  };


  /// \brief Generate a new basis with flat index-merging strategy
  template <class Basis>
  auto flatBasis(Basis const& basis)
  {
    return FlatBasis<Basis>::create(basis);
  }

  /// \brief Generate a new basis with blocked index-merging strategy
  template <class Basis>
  auto flatBasis(Basis&& basis)
  {
    return FlatBasis<remove_cvref_t<Basis>>::create(std::move(basis));
  }

} // end namespace AMDiS
