#pragma once

#include <memory>
#include <type_traits>
#include <vector>

#include <dune/functions/functionspacebases/defaultglobalbasis.hh>
#include <amdis/functions/BasisMapping.hpp>
#include <amdis/functions/BlockedBasis.hpp>
#include <amdis/functions/FlatBasis.hpp>

namespace AMDiS
{
  /// \brief Mapping of a pair of flat bases to the corresponding outer-level blocked bases.
  template <class RowBasis, class ColBasis>
  class BlockedBasisMapping
  {
    // RowBasis and ColBasis must be flat bases
    static_assert(std::is_same_v<FlatBasis_t<RowBasis>,RowBasis>);
    static_assert(std::is_same_v<FlatBasis_t<ColBasis>,ColBasis>);

    // Define a blocked rowbasis
    using RowBlockedBasis = BlockedBasis_t<RowBasis>;

    // Define a blocked colbasis
    using ColBlockedBasis = BlockedBasis_t<ColBasis>;

    // Since the source basis is flat, we can use a vector as storage backend for the mapping
    using RowMapping = BasisMapping<RowBasis, RowBlockedBasis,
      std::vector<typename RowBlockedBasis::MultiIndex>>;
    using ColMapping = BasisMapping<ColBasis, ColBlockedBasis,
      std::vector<typename ColBlockedBasis::MultiIndex>>;

  public:
    BlockedBasisMapping(std::shared_ptr<RowBasis> const& rowBasis,
                        std::shared_ptr<ColBasis> const& colBasis)
      : rowMapping_(rowBasis, std::make_shared<RowBlockedBasis>(blockedBasis(*rowBasis)))
      , colMapping_(colBasis, std::make_shared<ColBlockedBasis>(blockedBasis(*colBasis)))
    {}

    BlockedBasisMapping(std::shared_ptr<RowBasis> const& rowBasis)
      : BlockedBasisMapping(rowBasis, rowBasis)
    {}

    /// \brief Map the index in the flat row-basis into a multi-index in the blocked row-basis
    template <class Index>
    typename RowBlockedBasis::MultiIndex const& row(Index const& index) const
    {
      return rowMapping_(index);
    }

    /// \brief Map the index in the flat col-basis into a multi-index in the blocked col-basis
    template <class Index>
    typename ColBlockedBasis::MultiIndex const& col(Index const& index) const
    {
      return colMapping_(index);
    }

    /// \brief Get the size info (typically the basis itself) for the row-mapping
    decltype(auto) rowSizeInfo() const
    {
      return rowMapping_.sizeInfo();
    }

    /// \brief Get the size info (typically the basis itself) for the col-mapping
    decltype(auto) colSizeInfo() const
    {
      return colMapping_.sizeInfo();
    }

  private:
    RowMapping rowMapping_;
    ColMapping colMapping_;
  };


#ifndef DOXYGEN
  // Specialization for row-basis == col-basis:
  // Construct only one mapping for the indices
  template <class Basis>
  class BlockedBasisMapping<Basis, Basis>
  {
    // Basis must be flat basis
    static_assert(std::is_same_v<FlatBasis_t<Basis>,Basis>);

    // Define a blocked basis
    using BBasis = BlockedBasis_t<Basis>;
    using Mapping = BasisMapping<Basis, BBasis,
      std::vector<typename BBasis::MultiIndex>>;

  public:
    BlockedBasisMapping(std::shared_ptr<Basis> const& basis)
      : mapping_(basis, std::make_shared<BBasis>(blockedBasis(*basis)))
    {}

    BlockedBasisMapping(std::shared_ptr<Basis> const& rowBasis, std::shared_ptr<Basis> const& /*colBasis*/)
      : BlockedBasisMapping(rowBasis)
    {}

    /// \brief Map the index in the flat basis into a multi-index in the blocked basis
    template <class Index>
    typename BBasis::MultiIndex const& row(Index const& index) const
    {
      return mapping_(index);
    }

    /// \brief Map the index in the flat basis into a multi-index in the blocked basis
    template <class Index>
    typename BBasis::MultiIndex const& col(Index const& index) const
    {
      return mapping_(index);
    }

    /// \brief Get the size info (typically the basis itself) for the mapping
    decltype(auto) rowSizeInfo() const
    {
      return mapping_.sizeInfo();
    }

    /// \brief Get the size info (typically the basis itself) for the mapping
    decltype(auto) colSizeInfo() const
    {
      return mapping_.sizeInfo();
    }

  private:
    Mapping mapping_;
  };
#endif

} // end namespace AMDiS
