#pragma once

#include <amdis/functions/LocalView.hpp>

namespace AMDiS
{
  /// \brief The restriction of a finite element basis to a single element
  template <class RB, class CB>
  class LocalViewPair
  {
    using RowLocalView = LocalView<RB>;
    using ColLocalView = LocalView<CB>;

  public:
    /// \brief Construct local view for a given global finite element basis
    LocalViewPair(RB const& rowBasis, CB const& colBasis)
      : rowLocalView_(rowBasis)
      , colLocalView_(colBasis)
    {}

    /// \brief Bind the views to a grid element
    template <class Element>
    void bind (Element const& element)
    {
      rowLocalView_.bind(element);
      colLocalView_.bind(element);
    }

    /// \brief Unbind from the current element
    void unbind ()
    {
      rowLocalView_.unbind();
      colLocalView_.unbind();
    }

    RowLocalView const& row() const
    {
      return rowLocalView_;
    }

    ColLocalView const& col() const
    {
      return colLocalView_;
    }

  protected:
    RowLocalView rowLocalView_;
    ColLocalView colLocalView_;
  };

  /// \brief The restriction of a finite element basis to a single element
  template <class B>
  class LocalViewPair<B,B>
  {
    using LocalView = AMDiS::LocalView<B>;

  public:
    /// \brief Construct local view for a given global finite element basis
    LocalViewPair(B const& rowBasis, B const& colBasis)
      : same_(&rowBasis == &colBasis)
      , rowLocalView_(rowBasis)
      , colLocalView_(same_
          ? Dune::stackobject_to_shared_ptr(rowLocalView_)
          : std::make_shared<LocalView>(colBasis))
    {}

    ~LocalViewPair() = default;
    LocalViewPair(LocalViewPair const&) = delete;
    LocalViewPair& operator=(LocalViewPair const&) = delete;
    LocalViewPair(LocalViewPair&&) = delete;
    LocalViewPair& operator=(LocalViewPair&&) = delete;

    /// \brief Bind the views to a grid element
    template <class Element>
    void bind (Element const& element)
    {
      rowLocalView_.bind(element);
      if (!same_)
        colLocalView_->bind(element);
    }

    /// \brief Unbind from the current element
    void unbind ()
    {
      rowLocalView_.unbind();
      if (!same_)
        colLocalView_->unbind();
    }

    LocalView const& row() const
    {
      return rowLocalView_;
    }

    LocalView const& col() const
    {
      return *colLocalView_;
    }

  protected:
    bool same_ = false;
    LocalView rowLocalView_;
    std::shared_ptr<LocalView> colLocalView_;
  };

} // end namespace AMDiS
