#pragma once

#include <dune/functions/functionspacebases/defaultlocalview.hh>

#include <amdis/common/OptionalNoCopy.hpp>
#include <amdis/functions/NodeCache.hpp>

// NOTE: This is a variant of dune-functions DefaultLocalView

namespace AMDiS
{
  /// \brief The restriction of a finite element basis to a single element
  template <class GB>
  class LocalView
      : public Dune::Functions::DefaultLocalView<GB>
  {
    using Super = Dune::Functions::DefaultLocalView<GB>;

  public:
    /// The global FE basis that this is a view on
    using GlobalBasis = GB;

    /// Tree of local finite elements / local shape function sets
    using Tree = typename Super::Tree;

    /// Cached basis-tree
    using TreeCache = NodeCache_t<Tree>;

  public:
    /// \brief Construct local view for a given global finite element basis
    LocalView (GlobalBasis const& globalBasis)
      : Super(globalBasis)
    {}

    /// \brief Cached version of the local ansatz tree
    TreeCache const& treeCache () const
    {
      if (!treeCache_)
        treeCache_.emplace(makeNodeCache(Super::tree_));

      return *treeCache_;
    }

    /// \brief Return this local-view
    LocalView const& rootLocalView () const
    {
      return *this;
    }

  protected:
    mutable OptionalNoCopy<TreeCache> treeCache_ = std::nullopt;
  };

} // end namespace AMDiS
