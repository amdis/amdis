#pragma once

#include <type_traits>
#include <utility>

#include <dune/functions/functionspacebases/defaultglobalbasis.hh>
#include <amdis/functions/BlockedPreBasis.hpp>

namespace AMDiS
{
  // forward declaration
  template <class PB>
  class GlobalBasis;

  // define the type of the blocked basis
  template <class Basis>
  struct BlockedBasis;

  template <class Basis>
  using BlockedBasis_t = typename BlockedBasis<remove_cvref_t<Basis>>::type;

  template <class PB>
  struct BlockedBasis<Dune::Functions::DefaultGlobalBasis<PB>>
  {
    using type = Dune::Functions::DefaultGlobalBasis<typename BlockedPreBasis<PB>::type>;

    template <class Basis>
    static type create(Basis const& basis)
    {
      return type{blockedPreBasis(basis.preBasis())};
    }
  };

  template <class PB>
  struct BlockedBasis<GlobalBasis<PB>>
  {
    using type = Dune::Functions::DefaultGlobalBasis<typename BlockedPreBasis<PB>::type>;

    template <class Basis>
    static type create(Basis const& basis)
    {
      return type{blockedPreBasis(basis.preBasis())};
    }
  };


  /// \brief Generate a new basis with blocked index-merging strategy on the first level
  template <class Basis>
  auto blockedBasis(Basis const& basis)
  {
    return BlockedBasis<Basis>::create(basis);
  }

  /// \brief Generate a new basis with blocked index-merging strategy on the first level
  template <class Basis>
  auto blockedBasis(Basis&& basis)
  {
    return BlockedBasis<remove_cvref_t<Basis>>::create(std::move(basis));
  }

} // end namespace AMDiS
