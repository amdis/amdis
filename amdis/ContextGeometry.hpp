#pragma once

#include <optional>
#include <type_traits>

#include <dune/common/typetraits.hh>
#include <dune/geometry/referenceelement.hh>
#include <dune/geometry/type.hh>

namespace AMDiS
{
  namespace Impl
  {
    template <class E, class = std::void_t<>>
    struct ContextTypes
    {
      using Entity = E;
    };

    // specialization for intersections
    template <class I>
    struct ContextTypes<I, std::void_t<decltype(std::declval<I>().inside())>>
    {
      using Entity = typename I::Entity;
    };

    template <class Tag>
    struct ContextTagType
    {
      using type = typename Tag::type;
    };

  } // end namespace Impl


  /// \brief Wrapper class for element and geometry
  /**
   * A LocalContext can be either a grid entity of codim 0 (called an element)
   * or an intersection of elements. The element and its geometry may be stored
   * externally and can be passed along with the localContext object.
   * Since an intersection has a geometry (and localGeometry) different from the
   * geometry (and localGeometry) of the entity it belongs to, these objects
   * are provided as well.
   *
   * \tparam LC  LocalContext, either element or intersection
   **/
  template <class ContextType>
  struct ContextGeometry
  {
    template <class I>
    using IsIntersection = std::void_t<typename I::Entity, typename I::Geometry, typename I::LocalGeometry>;

    template <class E>
    using IsElement = std::void_t<typename E::Geometry, typename E::EntitySeed, typename E::LocalGeometry, typename E::HierarchicIterator>;

  public:
    using Context = ContextType;
    using Geometry = typename Context::Geometry;
    using LocalCoordinate = typename Geometry::LocalCoordinate;

    using Element = typename Impl::ContextTypes<Context>::Entity;
    using ElementGeometry = typename Element::Geometry;
    using ElementCoordinate = typename ElementGeometry::LocalCoordinate;

    template <class C>
    static constexpr bool isEntity = Dune::Std::is_detected_v<IsElement,C>;

    enum {
      dim = ElementGeometry::mydimension,    //< the dimension of the grid element
      dow = ElementGeometry::coorddimension  //< the dimension of the world
    };

    /// Constructor. Stores pointer to localContext, element, and geometry.
    ContextGeometry(Context const& context, Element const& element, ElementGeometry const& elementGeo)
      : context_(&context)
      , element_(&element)
      , elementGeometry_(&elementGeo)
    {}

    // Prevent from storing pointer to rvalue-reference
    ContextGeometry(Context const& context, Element const& element, ElementGeometry&& elementGeo) = delete;

  public:
    /// Return the LocalContext, either the element or an intersection.
    Context const& context() const
    {
      return *context_;
    }

    /// Return the bound element (entity of codim 0)
    Element const& element() const
    {
      return *element_;
    }

    /// Return the geometry of the \ref Element
    ElementGeometry const& elementGeometry() const
    {
      return *elementGeometry_;
    }

    /// Return the geometry of the \ref Context
    template <class C = Context>
    decltype(auto) geometry() const
    {
      if constexpr(isEntity<C>)
        return *elementGeometry_;
      else
        return context_->geometry();
    }

    /// Return the geometry relative to the element geometry. This can be either an identity, or geometryInInside of the intersection
    template <class C = Context>
    auto geometryInElement() const
    {
      if constexpr(isEntity<C>)
        return Dune::referenceElement(*element_).geometry();
      else
        return context_->geometryInInside();
    }

    /// Coordinate `p` given in coordinates inside the context, transformed to coordinate in geometry of the Element.
    /// NOTE: This is equivalent to geometryInElement.global(p)
    template <class C = Context>
    auto coordinateInElement(LocalCoordinate const& p) const
    {
      if constexpr(isEntity<C>)
        return p;
      else
        return context_->geometryInInside().global(p);
    }

    /// Coordinate `p` given in coordinates inside the context, transformed to coordinate in world space.
    template <class C = Context>
    auto global(LocalCoordinate const& p) const
    {
      return geometry().global(p);
    }

    /// Return the geometry-type of the local context
    Dune::GeometryType type() const
    {
      return context().type();
    }

    /// The integration element of the context geometry
    template <class C = Context>
    auto integrationElement(LocalCoordinate const& p) const
    {
      return geometry().integrationElement(p);
    }

  private:
    Context const* context_;
    Element const* element_;
    ElementGeometry const* elementGeometry_;
  };


  template <class GV>
  class GlobalContext
  {
  public:
    using GridView = GV;
    using Element = typename GV::template Codim<0>::Entity;
    using Geometry = typename Element::Geometry;

    enum {
      dim = GridView::dimension,      //< the dimension of the grid element
      dow = GridView::dimensionworld  //< the dimension of the world
    };

    /// Constructor. Stores a copy of gridView and a pointer to element and geometry.
    template <class E, class G>
    GlobalContext(GV const& gridView, E&& element, G&& geometry)
      : gridView_(gridView)
      , element_(Dune::wrap_or_move(FWD(element)))
      , geometry_(Dune::wrap_or_move(FWD(geometry)))
    {}

  public:
    /// Return the GridView this context is bound to
    GridView const& gridView() const
    {
      return gridView_;
    }

    /// Return the bound element (entity of codim 0)
    Element const& element() const
    {
      return *element_;
    }

    /// Return the geometry of the \ref Element
    Geometry const& geometry() const
    {
      return *geometry_;
    }

  private:
    GridView gridView_;
    std::shared_ptr<Element const> element_;
    std::shared_ptr<Geometry const> geometry_;
  };

} // end namespace AMDiS
