#pragma once

#include <cmath>

#include <amdis/Assembler.hpp>
#include <amdis/LinearAlgebra.hpp>
#include <amdis/Observer.hpp>
#include <amdis/common/Concepts.hpp>
#include <amdis/common/ConceptsBase.hpp>
#include <amdis/common/FlatMatrix.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/linearalgebra/SymmetryStructure.hpp>
#include <amdis/typetree/TreePath.hpp>

namespace AMDiS
{
  /**
   * Basis implementation of DOFMatrix, i.e. a sparse matrix storing all the
   * assembled Operators indexed with DOF indices. The matrix data is associated
   * to a row and column global basis.
   *
   * \tparam RB  Basis of the matrix rows
   * \tparam CB  Basis of matrix columns
   * \tparam T   Coefficient type to store in the matrix
   * \tparam Traits  Collection of parameter for the linear-algebra backend
   **/
  template <class RB, class CB, class T = double, class Traits = BackendTraits>
  class BiLinearForm
      : public MatrixFacade<T, Traits::template Matrix<RB,CB>::template Impl>
      , private ObserverSequence<event::adapt,2>
  {
    using Self = BiLinearForm;
    using Super = MatrixFacade<T,Traits::template Matrix<RB,CB>::template Impl>;

  public:
    /// The type of the finite element space / basis of the row
    using RowBasis = RB;

    /// The type of the finite element space / basis of the column
    using ColBasis = CB;

    /// The type of the elements of the DOFVector
    using CoefficientType = T;

    /// The type of the matrix filled on an element with local contributions
    using ElementMatrix = FlatMatrix<CoefficientType>;

    /// Type of the sparsity pattern of the backend
    using SparsityPattern = typename Traits::template Matrix<RB,CB>::SparsityPattern;

  public:
    /// Constructor. Stores the row and column basis in a local `shared_ptr` to const
    BiLinearForm(std::shared_ptr<RB> const& rowBasis, std::shared_ptr<CB> const& colBasis)
      : Super(*rowBasis, *colBasis)
      , ObserverSequence<event::adapt,2>(*rowBasis, *colBasis)
      , symmetry_(SymmetryStructure::unknown)
      , rowBasis_(rowBasis)
      , colBasis_(colBasis)
      , updatePattern_(true)
    {
      auto const rowSize = rowBasis_->localView().maxSize();
      auto const colSize = colBasis_->localView().maxSize();
      elementMatrix_.resize(rowSize, colSize);
    }

    /// Wraps the passed global bases into (non-destroying) shared_ptr
    template <class RB_, class CB_,
      REQUIRES(Concepts::Similar<RB_,RB>),
      REQUIRES(Concepts::Similar<CB_,CB>)>
    BiLinearForm(RB_&& rowBasis, CB_&& colBasis)
      : BiLinearForm(Dune::wrap_or_move(FWD(rowBasis)), Dune::wrap_or_move(FWD(colBasis)))
    {}

    /// Constructor for rowBasis == colBasis
    template <class RB_ = RB, class CB_ = CB,
      REQUIRES(std::is_same_v<RB_,CB_>)>
    explicit BiLinearForm(std::shared_ptr<RB> const& rowBasis)
      : BiLinearForm(rowBasis, rowBasis)
    {}

    /// Wraps the passed row-basis into a (non-destroying) shared_ptr
    template <class RB_,
      REQUIRES(Concepts::Similar<RB_,RB>)>
    explicit BiLinearForm(RB_&& rowBasis)
      : BiLinearForm(Dune::wrap_or_move(FWD(rowBasis)))
    {}

    RowBasis const& rowBasis() const { return *rowBasis_; }
    ColBasis const& colBasis() const { return *colBasis_; }

    /// \brief Associate a local operator with this BiLinearForm
    /**
     * Stores an operator in a list that gets assembled during a call to \ref assemble().
     * The operator may be assigned to a specific context, i.e. either an element
     * operator, an intersection operator, or a boundary operator.
     * The \p row and \p col tree paths specify the sub-basis for test and trial
     * functions the operator is applied to.
     *
     * \tparam ContextTag  One of \ref tag::element_operator, \ref tag::intersection_operator
     *                     or \ref BoundarySubset indicating where to assemble
     *                     this operator.
     * \tparam Operator    A (pre-)operator that can be bound to a gridView, or a valid
     *                     GridOperator.
     * \tparam row         A tree-path for the RowBasis
     * \tparam col         A tree-path for the ColBasis
     *
     * [[expects: row is valid tree-path in RowBasis]]
     * [[expects: col is valid tree-path in ColBasis]]
     * @{
     **/
    template <class ContextTag, class Operator, class Row, class Col>
    void addOperator(ContextTag contextTag, Operator const& op, Row row, Col col);

    // Add an operator to be assembled on the elements of the grid
    template <class Operator, class Row = RootTreePath, class Col = RootTreePath>
    void addOperator(Operator const& op, Row row = {}, Col col = {})
    {
      using E = typename RowBasis::LocalView::Element;
      addOperator(tag::element_operator<E>{}, op, row, col);
    }

    // Add an operator to be assembled on the intersections of the grid
    template <class Operator, class Row = RootTreePath, class Col = RootTreePath>
    void addIntersectionOperator(Operator const& op, Row row = {}, Col col = {})
    {
      using I = typename RowBasis::LocalView::GridView::Intersection;
      addOperator(tag::intersection_operator<I>{}, op, row, col);
    }
    /// @}

    /// Provide some additional information about the structure of the matrix pattern
    /**
     * If the symmetry structure is changed, it forces the matrix to
     * rebuild its patterns.
     **/
    void setSymmetryStructure(SymmetryStructure symm)
    {
      updatePattern_ = updatePattern_ || (symmetry_ != symm);
      symmetry_ = symm;
    }

    /// Set the symmetry structure using a string
    void setSymmetryStructure(std::string str)
    {
      setSymmetryStructure(symmetryStructure(str));
    }

    /// Assemble the matrix operators on the bound element.
    template <class RowLocalView, class ColLocalView,
      REQUIRES(Concepts::LocalView<RowLocalView>),
      REQUIRES(Concepts::LocalView<ColLocalView>)>
    void assemble(RowLocalView const& rowLocalView,
                  ColLocalView const& colLocalView);

    /// Assemble all matrix operators, TODO: incorporate boundary conditions
    void assemble();

    /// Prepare the underlying matrix for insertion and rebuild the sparsity pattern
    /// if required.
    /**
     * The pattern is rebuild if either the parameter `forceRebuild` is set to true
     * or if for some other reasons the internal flag `updatePattern_` is set.
     * This might be the case if the symmetry structure is changed or the basis
     * is updated.
     *
     * If the pattern is not updated, just the values are set to zero.
     **/
    void init(bool forcePatternRebuild = false)
    {
      if (updatePattern_ || forcePatternRebuild)
      {
        Super::init(SparsityPattern{*rowBasis_, *colBasis_, symmetry_});
        updatePattern_ = false;
      }
      else
      {
        Super::init();
      }
    }

    /// Set the flag that forces an update of the pattern since the underlying
    /// basis that defines the indexset has been changed
    void updateImpl(event::adapt, index_t<0>) override { updateImpl2(*rowBasis_); }
    void updateImpl(event::adapt, index_t<1>) override { updateImpl2(*colBasis_); }

    auto& assembler() { return assembler_; }

  private:
    template <class Basis>
    void updateImpl2(Basis const& basis)
    {
      if (!updatePattern_)
        assembler_.update(basis.gridView());
      updatePattern_ = true;
    }

  protected:
    /// The symmetry property if the bilinear form
    SymmetryStructure symmetry_;

    /// Dense matrix to store coefficients during \ref assemble()
    ElementMatrix elementMatrix_;

    /// List of operators associated to row/col node
    MatrixAssembler<RowBasis,ColBasis,ElementMatrix> assembler_;

    std::shared_ptr<RowBasis const> rowBasis_;
    std::shared_ptr<ColBasis const> colBasis_;

  private:
    // The pattern is rebuilt on calling init if this flag is set
    bool updatePattern_;
  };


  // deduction guides
  template <class RB, class CB>
  BiLinearForm(RB&&, CB&&)
    -> BiLinearForm<Underlying_t<RB>, Underlying_t<CB>>;

  template <class RB>
  BiLinearForm(RB&&)
    -> BiLinearForm<Underlying_t<RB>, Underlying_t<RB>>;


  template <class T = double, class RB, class CB>
  auto makeBiLinearForm(RB&& rowBasis, CB&& colBasis)
  {
    using BLF = BiLinearForm<Underlying_t<RB>, Underlying_t<CB>, T>;
    return BLF{FWD(rowBasis), FWD(colBasis)};
  }

  template <class T = double, class RB>
  auto makeBiLinearForm(RB&& rowBasis)
  {
    using BLF = BiLinearForm<Underlying_t<RB>, Underlying_t<RB>, T>;
    return BLF{FWD(rowBasis)};
  }

} // end namespace AMDiS

#include <amdis/BiLinearForm.inc.hpp>
