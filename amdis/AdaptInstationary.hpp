#pragma once

// std c++ headers
#include <string>

// AMDiS includes
#include "AdaptBase.hpp"

namespace AMDiS
{
  // forward declarations
  class AdaptInfo;
  class ProblemIterationInterface;
  class ProblemTimeInterface;

  /** \ingroup Adaption
   * \brief
   * AdaptInstationary implements the adaptive procedure for time dependent
   * problems (see ProblemInstat). It contains a pointer to a ProblemInstat
   * object.
   */
  class AdaptInstationary
      : public AdaptBase
  {
  public:
    /// Creates a AdaptInstationary object with the given name for the time
    /// dependent problem problemInstat.
    AdaptInstationary(std::string const& name,
                      ProblemIterationInterface& problemStat,
                      AdaptInfo& info,
                      ProblemTimeInterface& problemInstat,
                      AdaptInfo& initialInfo);

    /// Sets \ref strategy to aStrategy
    void setStrategy(int strategy)
    {
      strategy_ = strategy;
    }

    /// Returns \ref strategy
    int strategy() const
    {
      return strategy_;
    }

    /// Implementation of AdaptBase::adapt()
    int adapt() override;

  protected:
    /** \brief
     * Implements one (maybe adaptive) timestep. Both the explicit and the
     * implicit time strategy are implemented. The semi-implicit strategy
     * is only a special case of the implicit strategy with a limited number of
     * iterations (exactly one).
     * The routine uses the parameter \ref strategy to select the strategy:
     * strategy 0: Explicit strategy,
     * strategy 1: Implicit strategy.
     */
    virtual void oneTimestep();

    /// Implements the explit time strategy. Used by \ref oneTimestep().
    virtual void explicitTimeStrategy();

    /// Implements the implicit time strategy. Used by \ref oneTimestep().
    virtual void implicitTimeStrategy();

    /** \brief
     * This iteration strategy allows the timestep and the mesh to be adapted
     * after each timestep solution. There are no inner loops for mesh adaption and
     * no refused timesteps.
     */
    void simpleAdaptiveTimeStrategy();

  protected:
    /// Strategy for choosing one timestep
    int strategy_ = 0;

    /// Parameter \f$ \delta_1 \f$ used in time step reduction
    double timeDelta1_ = 0.7071;

    /// Parameter \f$ \delta_2 \f$ used in time step enlargement
    double timeDelta2_ = 1.4142;

    /// If this parameter is 1 and the instationary problem is stable, hence the number
    /// of solver iterations to solve the problem is zero, the adaption loop will stop.
    bool breakWhenStable_ = false;

    /// min-timestep == max-timestep
    bool fixedTimestep_;
  };

} // end namespace AMDiS
