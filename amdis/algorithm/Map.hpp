#pragma once

#include <array>
#include <tuple>
#include <vector>

#include <dune/common/tuplevector.hh>
#include <amdis/common/Apply.hpp>

namespace AMDiS {
namespace Recursive {

template <class T>
struct Map;

/// \brief Recursive application of a transformation functor `f` to a hierarchic
/// container of containers, returning the transformed container.
/**
 * This utility function applies the given functor `f` to the "leaf" entries in
 * a hierarchic container that returns a transformed container. Therefore, the
 * container is traversed recursively, using specializations of the `Map<Container>::impl`
 * class method. If no specialization is provided, the function is applied to the
 * whole container or leaf entry, respectively.
 **/
template <class F, class T>
auto map(F&& f, T const& t)
{
  return Map<T>::impl(f,t);
}


// specializations for container types

/// Default implementation of the recursive \ref map function.
template <class T>
struct Map
{
  template <class F>
  static auto impl(F&& f, T const& t)
  {
    return f(t);
  }
};

template <class T, std::size_t n>
struct Map<std::array<T,n>>
{
  template <class F>
  static auto impl(F&& f, std::array<T,n> const& a)
  {
    return Ranges::applyIndices<n>([&](auto... ii) {
      return std::array{Recursive::map(f,a[ii])...}; });
  }
};

template <class... TT>
struct Map<std::tuple<TT...>>
{
  template <class F>
  static auto impl(F&& f, std::tuple<TT...> const& t)
  {
    return Ranges::apply([&](auto const&... ti) {
      return std::tuple{Recursive::map(f,ti)...}; }, t);
  }
};

template <class T1, class T2>
struct Map<std::pair<T1,T2>>
{
  template <class F>
  static auto impl(F&& f, std::pair<T1,T2> const& t)
  {
    return std::pair{Recursive::map(f,t.first), Recursive::map(f,t.second)};
  }
};

template <class... TT>
struct Map<Dune::TupleVector<TT...>>
{
  template <class F>
  static auto impl(F&& f, Dune::TupleVector<TT...> const& t)
  {
    return Ranges::apply([&](auto const&... ti) {
      return Dune::makeTupleVector(Recursive::map(f,ti)...); }, t);
  }
};

template <class T>
struct Map<std::vector<T>>
{
  template <class F>
  static auto impl(F&& f, std::vector<T> const& v)
  {
    using U = TYPEOF(Recursive::map(f,std::declval<T>()));
    std::vector<U> out;
    out.reserve(v.size());
    for (std::size_t i = 0; i < v.size(); ++i)
      out.emplace_back(Recursive::map(f,v[i]));
    return out;
  }
};

}} // end namespace AMDiS::Recursive
