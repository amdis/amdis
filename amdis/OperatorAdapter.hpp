#pragma once

#include <dune/typetree/childextraction.hh>

namespace AMDiS
{
  /// The local operator associated to the \ref OperatorAdapter
  template <class LocalOperator, class RowTreePath, class ColTreePath = RowTreePath>
  class LocalOperatorAdapter
  {
  public:
    /// Constructor, stores all the arguments in local variables.
    LocalOperatorAdapter(LocalOperator const& lop, RowTreePath rowTp, ColTreePath colTp = {})
      : lop_(lop)
      , rowTp_(rowTp)
      , colTp_(colTp)
    {}

    /// \brief Binds the local operator to an `element`.
    /**
     * Binding an operator to the currently visited element in grid traversal.
     **/
    template <class Element>
    void bind(Element const& element)
    {
      lop_.bind(element);
    }

    /// \brief Unbinds operator from element.
    void unbind()
    {
      lop_.unbind();
    }

    /// \brief Assemble a local element matrix on the element that is bound.
    /**
    * \param contextGeo  Container for geometry related data, \ref ContextGeometry
    * \param row         The node of the test-basis
    * \param col         The node of the trial-basis
    * \param mat         Element-matrix
    **/
    template <class ContextGeo, class RowNode, class ColNode, class Mat>
    void assemble(ContextGeo const& cg, RowNode const& row, ColNode const& col, Mat& mat) const
    {
      using Dune::TypeTree::child;
      lop_.assemble(cg,child(row,rowTp_),child(col,colTp_),mat);
    }

    /// \brief Assemble a local element vector on the element that is bound.
    /**
    * \param contextGeo  Container for geometry related data, \ref ContextGeometry
    * \param node        The node of the test-basis
    * \param vec         Element-vector
    **/
    template <class ContextGeo, class Node, class Vec>
    void assemble(ContextGeo const& cg, Node const& node, Vec& vec) const
    {
      using Dune::TypeTree::child;
      lop_.assemble(cg,child(node,rowTp_),vec);
    }

  private:
    LocalOperator lop_;
    RowTreePath rowTp_;
    ColTreePath colTp_;
  };


  /**
   * \brief Wraps an operator that is bound to some tree path (rwo,col) and
   * transforms it into an operator that associated to the root-node of the tree.
   *
   * \tparam Operator     The child-node operator to be wrapped.
   * \tparam RowTreePath  Type of the tree-path of the test-space.
   * \tparam ColTreePath  Type of the tree-path of the trial-space (optional).
   */
  template <class Operator, class RowTreePath, class ColTreePath = RowTreePath>
  class OperatorAdapter
  {
  public:
    /// Constructor, stores all the arguments in local variables.
    OperatorAdapter(Operator op, RowTreePath rowTp, ColTreePath colTp = {})
      : op_(std::move(op))
      , rowTp_(rowTp)
      , colTp_(colTp)
    {}

    /// Update the wrapped operator on a \ref GridView
    template <class GridView>
    void update(GridView const& gv)
    {
      op_.update(gv);
    }

    /// Transform an operator into a local-operator
    friend auto localOperator(OperatorAdapter const& adapter)
    {
      return LocalOperatorAdapter{localOperator(adapter.op_),adapter.rowTp_,adapter.colTp_};
    }

  private:
    Operator op_;
    RowTreePath rowTp_;
    ColTreePath colTp_;
  };

} // end namespace AMDiS
