#pragma once

#include <optional>
#include <vector>

#include <dune/common/ftraits.hh>
#include <dune/common/typeutilities.hh>
#include <dune/functions/common/defaultderivativetraits.hh>
#include <dune/functions/functionspacebases/flatvectorview.hh>
#include <dune/functions/gridfunctions/gridviewentityset.hh>
#include <dune/typetree/childextraction.hh>

#include <amdis/common/DerivativeTraits.hpp>
#include <amdis/common/Tags.hpp>
#include <amdis/functions/NodeCache.hpp>
#include <amdis/interpolators/SimpleInterpolator.hpp>
#include <amdis/linearalgebra/Access.hpp>
#include <amdis/typetree/FiniteElementType.hpp>
#include <amdis/typetree/RangeType.hpp>
#include <amdis/typetree/TreePath.hpp>

namespace AMDiS
{
  /// \class DiscreteFunction
  /// \brief A view on a subspace of a \ref DOFVector
  /**
  * \ingroup GridFunctions
  *
  * \tparam Coeff     Const or mutable coefficient vector
  * \tparam GB        The type of the global basis
  * \tparam TreePath  A realization of \ref Dune::TypeTree::HybridTreePath
  * \tparam Range     The range type for th evaluation of the discrete function
  *
  * **Requirements:**
  * - GB models \ref Concepts::GlobalBasis
  **/
  template <class Coeff, class GB, class TreePath, class Range = void>
  class DiscreteFunction;


  /// A mutable view on the subspace of a DOFVector, \relates DiscreteFunction
  template <class Coeff, class GB, class TreePath, class R>
  class DiscreteFunction
      : public DiscreteFunction<Coeff const,GB,TreePath,R>
  {
    using Self = DiscreteFunction<Coeff,GB,TreePath,R>;
    using Super = DiscreteFunction<Coeff const,GB,TreePath,R>;

    using Coefficients = std::remove_const_t<Coeff>;
    using GlobalBasis = GB;

  public:
    /// Constructor. Stores a pointer to the mutable `coefficients` vector.
    template <class C, class B, class... Path,
      REQUIRES(Concepts::Similar<Underlying_t<C>, Coefficients>),
      REQUIRES(Concepts::Similar<Underlying_t<B>, GlobalBasis>)>
    DiscreteFunction(C&& coefficients, B&& basis, Path... path)
      : Super(FWD(coefficients), FWD(basis), path...)
      , mutableCoeff_(wrap_or_share<Coefficients>(FWD(coefficients)))
    {}

  public:
    /// \brief Interpolation of GridFunction to DOFVector, assuming that there is no
    /// reference to this DOFVector in the expression.
    /**
     * **Example:**
     * ```
     * auto v = valueOf(prob.solutionVector(),0);
     * v.interpolate_noalias([](auto const& x) { return x[0]; });
     * ```
     **/
    template <class Expr, class Tag = tag::assign>
    void interpolate_noalias(Expr&& expr, Tag strategy = {});

    /// \brief Interpolation of GridFunction to DOFVector
    /**
     * **Example:**
     * ```
     * auto v = valueOf(prob.solutionVector(),0);
     * v.interpolate(v + [](auto const& x) { return x[0]; });
     * ```
     * Allows to have a reference to the DOFVector in the expression, e.g. as
     * \ref DiscreteFunction or \ref gradientOf() of a DiscreteFunction.
     **/
    template <class Expr, class Tag = tag::assign>
    void interpolate(Expr&& expr, Tag strategy = {});

    /// \brief Interpolation of GridFunction to DOFVector, alias to \ref interpolate()
    template <class Expr>
    Self& operator<<(Expr&& expr)
    {
      interpolate(FWD(expr));
      return *this;
    }

    /// \brief interpolate `(*this) + expr` to DOFVector
    template <class Expr>
    Self& operator+=(Expr&& expr)
    {
      interpolate((*this) + expr);
      return *this;
    }

    /// \brief interpolate `(*this) - expr` to DOFVector
    template <class Expr>
    Self& operator-=(Expr&& expr)
    {
      interpolate((*this) - expr);
      return *this;
    }

    /// Return the mutable DOFVector
    Coefficients& coefficients()
    {
      return *mutableCoeff_;
    }

    /// Return the const DOFVector
    using Super::coefficients;

    template <class Range = void, class... Indices>
    auto child(Indices... ii)
    {
      auto tp = cat(Super::treePath_, makeTreePath(ii...));
      using Child = DiscreteFunction<Coeff, GB, TYPEOF(makeTreePath(tp)), Range>;
      return Child{mutableCoeff_, Super::basis_, tp};
    }

    using Super::child;

  protected:
    std::shared_ptr<Coefficients> mutableCoeff_;
  };

  /// A Const DiscreteFunction
  template <class Coeff, class GB, class TreePath, class R>
  class DiscreteFunction<Coeff const,GB,TreePath,R>
  {
  private:
    using Coefficients = std::remove_const_t<Coeff>;
    using GlobalBasis = GB;
    using LocalView = typename GlobalBasis::LocalView;
    using GridView = typename GlobalBasis::GridView;

    using Coefficient = TYPEOF(std::declval<Traits::Access>()(std::declval<Coeff const>(), std::declval<typename GB::MultiIndex>()));

    using Tree = typename GlobalBasis::LocalView::Tree;
    using SubTree = typename Dune::TypeTree::ChildForTreePath<Tree, TreePath>;

  public:
    /// Set of entities the DiscreteFunction is defined on
    using EntitySet = Dune::Functions::GridViewEntitySet<GridView, 0>;

    /// Global coordinates of the EntitySet
    using Domain = typename EntitySet::GlobalCoordinate;

    /// Range type of this DiscreteFunction. If R=void deduce the Range automatically, using RangeType_t
    using RangeGenerator = RangeType<SubTree,Coefficient,R>;
    using Range = typename RangeGenerator::type;

    /// Mark this class to not directly implement the derivativeOf function so
    /// that the DerivativeGridFunction can be used.
    enum { hasDerivative = false };

  private:
    /// A LocalFunction representing the localfunction and derivative of the DOFVector on a bound element
    template <class Type>
    class LocalFunction;

  public:
    /// Wrap or move or share the coefficients vector and basis reference or rvalue,
    /// respectively, into shared_ptr.
    /**
     * \param coefficients The vector can be passed either as const ref or as
     *                     shared_ptr. If passed as rvalue-ref it is moved into
     *                     a new shared_ptr.
     * \param basis        A global basis passed either as const ref or shared_ptr.
     * \param path...      The indices of a tree-path or the tree-path directly.
     **/
    template <class C, class B, class... Path,
      REQUIRES(Concepts::Similar<Underlying_t<C>, Coefficients>),
      REQUIRES(Concepts::Similar<Underlying_t<B>, GlobalBasis>)>
    DiscreteFunction(C&& coefficients, B&& basis, Path... path)
      : coefficients_{wrap_or_share(FWD(coefficients))}
      , basis_{wrap_or_share(FWD(basis))}
      , treePath_{makeTreePath(path...)}
      , entitySet_{basis_->gridView()}
    {}

    /// \brief Evaluate DiscreteFunction in global coordinates. NOTE: expensive
    Range operator()(Domain const& x) const;

    /// \brief Create a local function for this view on the DOFVector. \relates LocalFunction
    LocalFunction<tag::value> makeLocalFunction() const
    {
      return {basis().localView(), treePath_, coefficients_, tag::value{}};
    }

    /// \brief Return a \ref Dune::Functions::GridViewEntitySet
    EntitySet const& entitySet() const
    {
      return entitySet_;
    }

    /// \brief Return global basis bound to the DOFVector
    GlobalBasis const& basis() const
    {
      return *basis_;
    }

    /// \brief Return treePath associated with this discrete function
    TreePath const& treePath() const
    {
      return treePath_;
    }

    /// \brief Return const coefficient vector
    Coefficients const& coefficients() const
    {
      return *coefficients_;
    }

    template <class Range = void, class... Indices>
    auto child(Indices... ii) const
    {
      auto tp = cat(this->treePath_, makeTreePath(ii...));
      using Child = DiscreteFunction<Coeff const, GB, TYPEOF(makeTreePath(tp)), Range>;
      return Child{coefficients_, basis_, tp};
    }

  protected:
    std::shared_ptr<Coefficients const> coefficients_;
    std::shared_ptr<GlobalBasis const> basis_;
    TreePath treePath_;
    EntitySet entitySet_;
  };


  // deduction guides

  template <class Coefficients, class Basis, class... Indices,
    class C = Underlying_t<Coefficients>,
    class B = std::remove_const_t<Underlying_t<Basis>>,
    class TP = TYPEOF(makeTreePath(std::declval<Indices>()...)),
    REQUIRES(Concepts::GlobalBasis<B>)>
  DiscreteFunction(Coefficients&&, Basis&&, Indices...)
    -> DiscreteFunction<C,B,TP>;


  // Get the childs of a discrete function by using `valueOf`
  // -------------------------------------------------------

  /// A Generator for the childs of a mutable \ref DiscreteFunction
  template <class Range = void, class C, class GB, class TP, class R, class... Indices>
  auto valueOf(DiscreteFunction<C,GB,TP,R>& df, Indices... ii)
  {
    return df.template child<Range>(ii...);
  }

  /// A Generator for the childs of a const \ref DiscreteFunction
  template <class Range = void, class C, class GB, class TP, class R, class... Indices>
  auto valueOf(DiscreteFunction<C,GB,TP,R> const& df, Indices... ii)
  {
    return df.template child<Range>(ii...);
  }

} // end namespace AMDiS

#include "DiscreteLocalFunction.inc.hpp"
#include "DiscreteFunction.inc.hpp"
