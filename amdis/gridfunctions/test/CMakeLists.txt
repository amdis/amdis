# additional compiler option for CMAKE_BUILD_TYPE=RelWithDebInfo only for gcc
set(NoVarTrackingAssignments $<$<AND:$<CONFIG:RelWithDebInfo>,$<CXX_COMPILER_ID:GNU>>:-fno-var-tracking-assignments>)

dune_add_test(SOURCES CoordGridFunction.cpp
  LINK_LIBRARIES amdis)

dune_add_test(SOURCES ComposerVectorGridFunction.cpp
  LINK_LIBRARIES amdis)
