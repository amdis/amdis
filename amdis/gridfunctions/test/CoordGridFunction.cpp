#include <cassert>
#include <limits>

#include <amdis/AMDiS.hpp>
#include <amdis/gridfunctions/CoordsGridFunction.hpp>

#include <dune/grid/yaspgrid.hh>

template <class T>
bool almost_equal(T a, T b, int ulp = 2)
{
  using std::abs;
  using F = typename Dune::FieldTraits<T>::field_type;
  return abs(a-b) <= abs(a+b) * std::numeric_limits<F>::epsilon() * ulp ||
         abs(a-b) < std::numeric_limits<F>::min();
}

using namespace AMDiS;
int main(int argc, char** argv)
{
  Environment env{argc, argv};

  Dune::YaspGrid<2> grid({1.0,1.0},{4u,4u});
  auto gridView = grid.leafGridView();

  auto pgf0 = X(0);
  auto pgf1 = X(1);
  static_assert(Concepts::AnyGridFunction<TYPEOF(pgf0)>);
  static_assert(Concepts::AnyGridFunction<TYPEOF(pgf1)>);
  static_assert(std::is_same_v<TYPEOF(pgf0),TYPEOF(pgf1)>);

  auto gf0 = makeGridFunction(pgf0, gridView);
  auto gf1 = makeGridFunction(pgf1, gridView);

  [[maybe_unused]] FieldVector<double,2> x = {0.2, 0.6};
  assert(almost_equal(gf0(x), x[0]));
  assert(almost_equal(gf1(x), x[1]));

  auto lf0 = localFunction(gf0);
  auto lf1 = localFunction(gf1);
  for (auto const& e : elements(gridView))
  {
    lf0.bind(e);
    lf1.bind(e);

    auto refElem = referenceElement(e);
    auto geo = e.geometry();
    [[maybe_unused]] auto local = refElem.position(0,0);
    [[maybe_unused]] auto global = geo.global(local);
    assert(almost_equal(lf0(local), global[0]));
    assert(almost_equal(lf1(local), global[1]));

    lf0.unbind();
    lf1.unbind();
  }
}
