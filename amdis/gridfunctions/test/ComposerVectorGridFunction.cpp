#include <cassert>
#include <limits>
#include <vector>

#include <amdis/AMDiS.hpp>
#include <amdis/gridfunctions/ComposerVectorGridFunction.hpp>
#include <amdis/gridfunctions/CoordsGridFunction.hpp>

#include <dune/grid/yaspgrid.hh>

template <class T>
bool almost_equal(T a, T b, int ulp = 2)
{
  using std::abs;
  using F = typename Dune::FieldTraits<T>::field_type;
  return abs(a-b) <= abs(a+b) * std::numeric_limits<F>::epsilon() * ulp ||
         abs(a-b) < std::numeric_limits<F>::min();
}

using namespace AMDiS;
int main(int argc, char** argv)
{
  Environment env{argc, argv};

  Dune::YaspGrid<2> grid({1.0,1.0},{4u,4u});
  auto gridView = grid.leafGridView();

  auto pgf0 = X(0);
  auto pgf1 = X(1);
  std::vector pgfs = {pgf0, pgf1};

  auto f = [](std::vector<double> x) { return x[0]+x[1]; };

  auto vec_pgf = invokeVectorAtQP(f, pgfs);
  static_assert(Concepts::AnyGridFunction<TYPEOF(vec_pgf)>);

  auto vec_pgf_gf = makeGridFunction(vec_pgf, gridView);
  static_assert(Concepts::GridFunction<TYPEOF(vec_pgf_gf)>);

  auto gf0 = makeGridFunction(pgf0, gridView);
  auto gf1 = makeGridFunction(pgf1, gridView);
  auto vec_gf = makeComposerVectorGridFunction(f, gridView, std::vector{gf0,gf1});
  static_assert(Concepts::GridFunction<TYPEOF(vec_gf)>);

  [[maybe_unused]] FieldVector<double,2> x = {0.5, 0.5};
  assert(almost_equal(gf0(x)+gf1(x), vec_gf(x)));

  auto lf0 = localFunction(gf0);
  auto lf1 = localFunction(gf1);
  auto vec_lf = localFunction(vec_gf);
  for (auto const& e : elements(gridView))
  {
    lf0.bind(e);
    lf1.bind(e);
    vec_lf.bind(e);

    [[maybe_unused]] auto refElem = referenceElement(e);
    [[maybe_unused]] auto local = refElem.position(0,0);
    assert(almost_equal(lf0(local)+lf1(local), vec_lf(local)));

    lf0.unbind();
    lf1.unbind();
    vec_lf.unbind();
  }
}
