#pragma once

#include <tuple>
#include <type_traits>

#include <amdis/Operations.hpp>
#include <amdis/common/ForEach.hpp>
#include <amdis/common/Logical.hpp>
#include <amdis/common/Order.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/gridfunctions/Derivative.hpp>
#include <amdis/gridfunctions/GridFunction.hpp>

namespace AMDiS
{
#ifndef DOXYGEN
  template <class Signatur, class Element, class Functor, class... LocalFunctions>
  class ComposerLocalFunction;
#endif

  // implementation
  template <class R, class D, class E, class Functor, class... LocalFunctions>
  class ComposerLocalFunction<R(D), E, Functor, LocalFunctions...>
  {
  public:
    using Range = R;
    using Domain = D;
    using Element = E;

    enum { hasDerivative = true };

  public:
    /// Constructor. Stores copies of the functor and localFunction(gridfunction)s.
    template <class... LocalFcts>
    ComposerLocalFunction(Functor const& fct, LocalFcts&&... localFcts)
      : fct_{fct}
      , localFcts_{FWD(localFcts)...}
    {}

    /// Calls \ref bind for all localFunctions
    void bind(Element const& element)
    {
      std::apply([&](auto &...lf) { (lf.bind(element), ...); }, localFcts_);
    }

    /// Calls \ref unbind for all localFunctions
    void unbind()
    {
      std::apply([](auto&... lf) { (lf.unbind(),...); },
        localFcts_);
    }

    /// \brief Check whether the LocalFunction is bound to an element
    bool bound() const
    {
      return std::get<0>(localFcts_).bound();
    }

    /// Applies the functor \ref fct_ to the evaluated localFunctions
    Range operator()(Domain const& x) const
    {
      return std::apply([&](auto const&... lf) { return fct_(lf(x)...); },
        localFcts_);
    }

    /// Get the element this localfunction (and all inner localfunctions) are bound to
    Element const& localContext() const
    {
      return std::get<0>(localFcts_).localContext();
    }


  public:
    /// Return the stored functor
    Functor const& fct() const
    {
      return fct_;
    }

    auto const& localFunctions() const
    {
      return localFcts_;
    }

  private:
    Functor fct_;
    std::tuple<LocalFunctions...> localFcts_;
  };

  template <class Element, class Functor, class... LocalFunctions>
  auto makeComposerLocalFunction(Functor const& f, LocalFunctions const&... lf)
  {
    using D = typename Element::Geometry::LocalCoordinate;
    using R = TYPEOF(f(lf(std::declval<D>())...));
    return ComposerLocalFunction<R(D), Element, Functor, LocalFunctions...>{f, lf...};
  }



  /// \fn derivative
  /// \brief Derivative of the LocalFunction of a ComposerGridFunction, utilizing
  /// the chain-rule. Only available if the functor provides partial derivatives.
  /**
   * \f$ d_x(f(lf(x)...)) = \sum_i d_i(f)[lf(x)...] * derivativeOf(lf[i]) \f$
   *
   * **Requirements:**
   * - The Functor `F` must model `Concepts::HasPartial`
   **/
  template <class Sig, class E, class F, class... LFs, class Type,
    REQUIRES(Concepts::HasPartial<F>)>
  auto derivativeOf(ComposerLocalFunction<Sig,E,F,LFs...> const& composed, Type const& type)
  {
    // d_i(f)[lgfs...] * lgfs_i
    auto term_i = [&](auto ii)
    {
      auto di_f = std::apply([&](auto const&... lf) {
        return makeComposerLocalFunction<E>(partial(composed.fct(), ii), lf...);
      }, composed.localFunctions());

      auto const& lf_i = std::get<ii>(composed.localFunctions());
      auto df
          = makeComposerLocalFunction<E>(Operation::Multiplies{}, di_f, derivativeOf(lf_i, type));
      if (composed.bound())
        df.bind(composed.localContext());
      return df;
    };

    // sum_i [ d_i(f)[lgfs...] * derivativeOf(lgfs_i)
    auto localFct = Ranges::applyIndices<sizeof...(LFs)>([&](auto... ii)
    {
      return makeComposerLocalFunction<E>(Operation::Plus{}, term_i(ii)...);
    });

    return localFct;
  }


  /// \fn order
  /// \brief Calculate the polynomial order of a functor `F` that provides a free
  /// function order(f, degs...), where degs are the orders of the LocalFunctions.
  /**
   * **Requirements:**
   * - The functor `F` must model `Concepts::HasFunctorOrder`
   * - All localFunctions `LFs...` must model `Concepts::Polynomial`
   **/
  template <class Sig, class E, class F, class... LFs,
    REQUIRES(Concepts::HasFunctorOrder<F,sizeof...(LFs)>
             && (Concepts::Polynomial<LFs> &&...))>
  int order(ComposerLocalFunction<Sig,E,F,LFs...> const& composed)
  {
    return Ranges::apply([&](auto const&... lf) {
      return order(composed.fct(), order(lf)...);
    }, composed.localFunctions());
  }


  /// \class ComposerGridFunction
  /// \brief A Gridfunction that applies a functor to the evaluated Gridfunctions
  /**
   * \ingroup GridFunctions
   * Composition of GridFunctions `g_i` by applying a functor `f` locally, i.e.
   * locally it is evaluated
   * \f$ f(g_0(x), g_1(x), ...) \f$
   *
   * \tparam Sig               Signature of the GridFunction
   * \tparam EntitySet         The EntitySet this GridFunction is defined on
   * \tparam Functor           The type of the outer functor `f`
   * \tparam GridFunctions...  The GridFunction types of `g_i`
   *
   * Requirements:
   * - `arity(f) == sizeof...(GridFunctions)`
   * - `arity(g_i) == arity(g_j) for i != j`
   * - `g_i` models concept \ref GridFunction
   **/
  template <class Sig, class EntitySet, class Functor, class... GridFunctions>
  class ComposerGridFunction;

  template <class R, class D, class ES, class Functor, class... GridFunctions>
  class ComposerGridFunction<R(D), ES, Functor, GridFunctions...>
  {
  public:
    using Range = R;
    using Domain = D;
    using EntitySet = ES;

    enum { hasDerivative = false };

  private:
    template <class GridFct>
    using LocalFct = TYPEOF( localFunction(underlying(std::declval<GridFct const&>())) );

    using LocalDomain = typename EntitySet::LocalCoordinate;
    using Element = typename EntitySet::Element;

  public:
    using LocalFunction
      = ComposerLocalFunction<Range(LocalDomain), Element, Functor, LocalFct<GridFunctions>...>;

    /// \brief Constructor. Stores copies of the functor and gridfunctions.
    template <class... GridFcts>
    ComposerGridFunction(EntitySet const& entitySet, Functor const& fct, GridFcts&&... gridFcts)
      : entitySet_{entitySet}
      , fct_{fct}
      , gridFcts_{FWD(gridFcts)...}
    {}

    /// Applies the functor to the evaluated gridfunctions
    Range operator()(Domain const& x) const
    {
      return std::apply([&](auto const&... gf) { return fct_(underlying(gf)(x)...); },
        gridFcts_);
    }

    /// Return the stored \ref EntitySet of the first GridFunction
    EntitySet const& entitySet() const
    {
      return entitySet_;
    }

    /// Create the localFunction by composition of the inner localFunctions
    LocalFunction makeLocalFunction() const
    {
      return std::apply([&](auto const&... gf) { return LocalFunction{fct_, localFunction(underlying(gf))...}; },
        gridFcts_);
    }

  private:
    EntitySet entitySet_;
    Functor fct_;
    std::tuple<GridFunctions...> gridFcts_;
  };


  // Generator function for ComposerGridFunction expressions
  template <class Functor, class GridView, class... GridFcts>
  auto makeComposerGridFunction(Functor const& f, GridView const& gridView,
                                GridFcts const&... gridFcts)
  {
    static_assert((Concepts::GridFunction<GridFcts> && ...),
      "All passed parameters must be GridFunctions.");
    static_assert(Concepts::Callable<Functor, typename GridFcts::Range...>,
      "Range types of grid functions are not compatible with the functor.");

    using EntitySet = Dune::Functions::GridViewEntitySet<GridView, 0>;
    using Domain = typename EntitySet::GlobalCoordinate;
    using Range = TYPEOF(f(underlying(gridFcts)(std::declval<Domain>())...));

    using FGF = ComposerGridFunction<Range(Domain), EntitySet, Functor, GridFcts...>;
    return FGF{EntitySet{gridView},f, gridFcts...};
  }


#ifndef DOXYGEN
  // PreGridFunction related to ComposerGridFunction.
  template <class Functor, class... PreGridFunctions>
  struct ComposerPreGridFunction
  {
    using Self = ComposerPreGridFunction;

    struct Creator
    {
      template <class GridView>
      static auto create(Self const& self, GridView const& gridView)
      {
        return std::apply([&](auto const&... pgf) {
          return makeComposerGridFunction(self.fct_, gridView,
            makeGridFunction(pgf, gridView)...);
        }, self.preGridFcts_);
      }
    };

    template <class... PreGridFcts>
    explicit ComposerPreGridFunction(Functor const& fct, PreGridFcts&&... pgfs)
      : fct_{fct}
      , preGridFcts_{FWD(pgfs)...}
    {}

  private:
    Functor fct_;
    std::tuple<PreGridFunctions...> preGridFcts_;
  };

  namespace Traits
  {
    template <class Functor, class... PreGridFcts>
    struct IsPreGridFunction<ComposerPreGridFunction<Functor, PreGridFcts...>>
      : std::true_type {};
  }
#endif


  /// \brief Generator function for ComposerGridFunction.
  /// \relates ComposerGridFunction
  /**
   * \ingroup GridFunctions
   * Applies the functor `f` to the grid-functions `gridFcts...`.
   * See \ref ComposerGridFunction.
   *
   * **Examples:**
   * - `invokeAtQP([](Dune::FieldVector<double, 2> const& x) { return two_norm(x); }, X());`
   * - `invokeAtQP([](double u, auto const& x) { return u + x[0]; }, 1.0, X());`
   * - `invokeAtQP(Operation::Plus{}, X(0), X(1));`
   **/
  template <class Functor, class... PreGridFcts>
  auto invokeAtQP(Functor const& f, PreGridFcts&&... gridFcts)
  {
    return ComposerPreGridFunction<Functor, TYPEOF(gridFcts)...>{f, FWD(gridFcts)...};
  }

} // end namespace AMDiS
