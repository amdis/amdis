#pragma once

#include <optional>
#include <type_traits>

#include <dune/functions/common/signature.hh>
#include <dune/functions/gridfunctions/gridviewentityset.hh>

#include <amdis/Operations.hpp>
#include <amdis/common/DerivativeTraits.hpp>
#include <amdis/common/Order.hpp>
#include <amdis/gridfunctions/GridFunction.hpp>

namespace AMDiS
{
#ifndef DOXYGEN
  template <class Signature, class LocalContext, class Function>
  class AnalyticLocalFunction;
#endif

  template <class R, class D, class LC, class Function>
  class AnalyticLocalFunction<R(D), LC, Function>
  {
  public:
    /// The LocalDomain this LocalFunction can be evaluated in
    using Domain = D;

    /// The range type of the LocalFunction
    using Range = R;

    /// The context this localfunction can be bound to
    using LocalContext = LC;

    /// This LocalFunction has its own \ref derivativeOf() function
    enum { hasDerivative = true };

  private:
    using Geometry = typename LC::Geometry;

  public:
    /// \brief Constructor. stores the function `fct`.
    AnalyticLocalFunction(Function const& fct)
      : fct_{fct}
    {}

    /// \brief Create a geometry object from the element
    void bind(LocalContext const& element)
    {
      localContext_.emplace(element);
      geometry_.emplace(element.geometry());
    }

    /// \brief Releases the geometry object
    void unbind()
    {
      geometry_.reset();
      localContext_.reset();
    }
    /// \brief Check whether this localfunction is bound to an element
    bool bound() const
    {
      return !!localContext_;
    }

    /// \brief Return the context this localfunction is bound to
    LocalContext const& localContext() const
    {
      assert( !!localContext_ );
      return *localContext_;
    }

    /// \brief Evaluate the function in global coordinate by a local-to-global
    /// mapping of the local coordinates using the bound geometry object.
    Range operator()(Domain const& local) const
    {
      assert( !!geometry_ );
      return fct_(geometry_.value().global(local));
    }

    Function const& fct() const
    {
      return fct_;
    }

  private:
    Function fct_;
    std::optional<LocalContext> localContext_;
    std::optional<Geometry> geometry_;
  };


  /// \fn order
  /// \brief Return the polynomial order of the LocalFunction.
  /**
   * \relates AnalyticLocalFunction
   * The order is defined as the polynomial order of the functor `fct` and
   * requires a free function `order(fct,1)` to exists.
   *
   * **Requirements:**
   * - The functor `F` must fulfill the concept \ref Concepts::HasFunctorOrder
   **/
  template <class Sig, class LC, class F,
    REQUIRES(Concepts::HasFunctorOrder<F,1>)>
  int order(AnalyticLocalFunction<Sig,LC,F> const& lf)
  {
    return order(lf.fct(),1);
  }


  /// \fn derivative
  /// \brief Create an \ref AnalyticLocalFunction representing the derivative
  /// of the given AnalyticLocalFunction.
  /**
   * In order to differentiate the local function, the functor must be
   * differentiable, i.e. a free function `partial(fct,_0)` must exist.
   *
   * **Requirements:**
   * - The functor `F` must fulfill the concept \ref Concepts::HasDerivative
   **/
  template <class R, class D, class LC, class F, class Type>
  auto derivativeOf(AnalyticLocalFunction<R(D),LC,F> const& lf, Type const& type)
  {
    static_assert(Concepts::HasDerivative<F,Type>,
      "No derivative(F,DerivativeType) defined for Functor F of AnalyticLocalFunction.");

    auto df = derivativeOf(lf.fct(), type);

    using RawSignature = typename Dune::Functions::SignatureTraits<R(D)>::RawSignature;
    using DerivativeSignature = typename DerivativeTraits<RawSignature,Type>::Range(D);
    auto ldf = AnalyticLocalFunction<DerivativeSignature, LC, decltype(df)>{df};
    // bind if lf is bound
    if (lf.bound())
      ldf.bind(lf.localContext());
    return ldf;
  }

  /// \class AnalyticGridFunction
  /// \brief A Gridfunction that evaluates a function with global coordinates.
  /**
   * \ingroup GridFunctions
   * Implements a GridFunction that wraps a functor around global coordinates.
   *
   * \tparam Function The callable `f=f(x)` with `x in Domain == GlobalCoordinates`
   * \tparam GridView A GridView that models `Dune::GridViewConcept`.
   *
   * **Requirements:**
   * - Function models \ref Concepts::Callable<Domain>
   **/
  template <class Function, class GridView>
  class AnalyticGridFunction
  {
  public:
    using EntitySet = Dune::Functions::GridViewEntitySet<GridView, 0>;
    using Domain = typename EntitySet::GlobalCoordinate;
    using Range = remove_cvref_t<std::invoke_result_t<Function,Domain>>;

    enum { hasDerivative = true };

  private:
    using Element = typename EntitySet::Element;
    using LocalDomain = typename EntitySet::LocalCoordinate;
    using LocalFunction = AnalyticLocalFunction<Range(LocalDomain), Element, Function>;

  public:
    /// Constructor. Stores the function `fct` and creates an `EntitySet`.
    AnalyticGridFunction(Function const& fct, GridView const& gridView)
      : fct_{fct}
      , entitySet_{gridView}
    {}

    /// Return the evaluated functor at global coordinates
    Range operator()(Domain const& x) const
    {
      return fct_(x);
    }

    /// Return the \ref AnalyticLocalFunction of the AnalyticGridFunction.
    friend LocalFunction localFunction(AnalyticGridFunction const& gf)
    {
      return {gf.fct_};
    }

    /// Returns \ref entitySet_
    EntitySet const& entitySet() const
    {
      return entitySet_;
    }

    /// Returns \ref fct_
    Function const& fct() const { return fct_; }

  private:
    Function fct_;
    EntitySet entitySet_;
  };


  /// \fn derivative
  /// \brief Return a GridFunction representing the derivative of a functor.
  /**
   * \relates AnalyticGridfunction
   *
   * **Requirements:**
   * - Functor `F` must fulfill the concept \ref Concepts::HasDerivative<Type>
   **/
  template <class F, class GV, class Type>
  auto derivativeOf(AnalyticGridFunction<F,GV> const& gf, Type const& type)
  {
    static_assert(Concepts::HasDerivative<F,Type>,
      "No derivative(F,DerivativeType) defined for Functor of AnalyticLocalFunction.");

    auto df = derivativeOf(gf.fct(), type);
    return AnalyticGridFunction<decltype(df), GV>{df, gf.entitySet().gridView()};
  }


#ifndef DOXYGEN
  // A pre-GridFunction that just stores the function
  template <class Function>
  struct AnalyticPreGridFunction
  {
    using Self = AnalyticPreGridFunction;

    struct Creator
    {
      template <class GridView>
      static auto create(Self const& self, GridView const& gridView)
      {
        using Coordinate = typename GridView::template Codim<0>::Entity::Geometry::GlobalCoordinate;
        static_assert(std::is_invocable_v<Function, Coordinate>);

        return AnalyticGridFunction<Function, GridView>{self.fct_, gridView};
      }
    };

    Function fct_;
  };

  namespace Traits
  {
    template <class Functor>
    struct IsPreGridFunction<AnalyticPreGridFunction<Functor>>
      : std::true_type {};
  }
#endif


  /// \fn evalAtQP
  /// \brief Generator function for AnalyticGridFunction.
  /**
   * \relates AnalyticGridfunction
   * \ingroup GridFunctions
   * Evaluate a functor at global coordinates. See \ref AnalyticGridFunction.
   *
   * **Examples:**
   * - `evalAtQP([](Dune::FieldVector<double, 2> const& x) { return x[0]; })`
   * - `evalAtQP(Operation::TwoNorm{})`
   **/
  template <class Function>
  auto evalAtQP(Function const& f)
  {
    return AnalyticPreGridFunction<Function>{f};
  }

} // end namespace AMDiS
