#pragma once

#include <functional>
#include <type_traits>

#include <dune/common/exceptions.hh>
#include <dune/functions/gridfunctions/gridviewentityset.hh>

#include <amdis/ElementVector.hpp>
#include <amdis/common/DerivativeTraits.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/gridfunctions/ConstantGridFunction.hpp>
#include <amdis/gridfunctions/GridFunction.hpp>

namespace AMDiS
{
#ifndef DOXYGEN
  template <class Signature, class IndexSet, class Container>
  class ElementLocalFunction;
#endif

  /// \brief LocalFunction of a Gridfunction returning a constant value per element.
  template <class R, class D, class IndexSet, class Container>
  class ElementLocalFunction<R(D), IndexSet, Container>
  {
  public:
    /// The LocalDomain this LocalFunction can be evaluated in
    using Domain = D;

    /// The range type of the LocalFunction
    using Range = R;

    /// This LocalFunction has its own \ref derivativeOf() function
    enum { hasDerivative = true };

    /// Type of the entity to bind
    using Element = typename IndexSet::template Codim<0>::Entity;

  public:
    /// \brief Constructor. Stores the constant value.
    ElementLocalFunction(IndexSet const& indexSet, Container const& container)
      : indexSet_(&indexSet)
      , container_(&container)
    {}

    /// Return the bound element
    Element const& localContext() const
    {
      assert(!!element_);
      return *element_;
    }

    /// Extract the constant data associated to the element
    void bind(Element const& element)
    {
      element_.emplace(element);
      value_ = (*container_)[indexSet_->index(element)];
    }

    void unbind() {}

    /// \brief Check whether this localfunction is bound to an element
    bool bound() const
    {
      return !!element_;
    }

    /// Return the constant `value_`.
    Range const& operator()(Domain const& /*local*/) const
    {
      return value_;
    }

    /// \brief Create a \ref ConstantLocalFunction representing the derivative
    /// of a constant function, that ist, the value 0.
    template <class Type>
    auto makeDerivative(Type const& /*type*/) const
    {
      using RawSignature = typename Dune::Functions::SignatureTraits<R(D)>::RawSignature;
      using DerivativeRange = typename DerivativeTraits<RawSignature,Type>::Range;
      DerivativeRange diff(0);
      ConstantLocalFunction<DerivativeRange(D), Element, DerivativeRange> df{diff};
      if (bound())
        df.bind(*element_);
      return df;
    }

    /// Return the constant polynomial order 0.
    int order() const
    {
      return 0;
    }

  private:
    IndexSet const* indexSet_;
    Container const* container_;
    std::optional<Element> element_;
    Range value_ = {};
  };


  /// \brief Gridfunction returning a constant value per element.
  /**
   * \ingroup GridFunctions
   *
   * The GridFunction represents for each element the constant value
   * that is stored in the `Container`. This container is indexed by
   * the IndexSet of the given `GridView` for the element it is bound to.
   **/
  template <class GridView, class Container>
  class ElementGridFunction
  {
  public:
    using EntitySet = Dune::Functions::GridViewEntitySet<GridView, 0>;
    using Domain = typename EntitySet::GlobalCoordinate;
    using Range = typename Container::value_type;

    enum { hasDerivative = false };

  private:
    using LocalDomain = typename EntitySet::LocalCoordinate;
    using LocalFunction = ElementLocalFunction<Range(LocalDomain), typename GridView::IndexSet, Container>;

  public:
    /// Constructor. Stores the function `fct` and creates an `EntitySet`.
    ElementGridFunction(GridView const& gridView, Container const& container)
      : entitySet_(gridView)
      , container_(&container)
    {}

    template <class G, class T,
      std::enable_if_t<std::is_same_v<typename G::LeafGridView,GridView>, int> = 0,
      std::enable_if_t<std::is_same_v<typename ElementVector<G,T>::Data, Container>, int> = 0>
    ElementGridFunction(ElementVector<G,T> const& elementVector)
      : entitySet_(elementVector.gridView())
      , container_(&elementVector.data())
    {}

    /// Return the constant `value_`
    Range operator()(Domain const& /*x*/) const
    {
      DUNE_THROW(Dune::NotImplemented, "Global evaluation not yet implemented");
      return Range(0);
    }

    EntitySet const& entitySet() const
    {
      return entitySet_;
    }

    /// \brief Create an \ref ElementLocalFunction
    LocalFunction makeLocalFunction() const
    {
      return {entitySet_.gridView().indexSet(), *container_};
    }

  private:
    EntitySet entitySet_;
    Container const* container_;
  };

  // deduction guide
  template <class G, class T>
  ElementGridFunction(ElementVector<G,T> const&)
    -> ElementGridFunction<typename G::LeafGridView, typename ElementVector<G,T>::Data>;

  /// A Generator for an \ref ElementGridFunction
  template <class G, class T>
  auto valueOf(ElementVector<G,T> const& ev)
  {
    return ElementGridFunction{ev.gridView(), ev.data()};
  }

} // end namespace AMDiS
