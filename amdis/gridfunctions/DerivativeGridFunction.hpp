#pragma once

#include <type_traits>

#include <dune/grid/utility/hierarchicsearch.hh>

#include <amdis/common/Concepts.hpp>
#include <amdis/common/DerivativeTraits.hpp>
#include <amdis/gridfunctions/Derivative.hpp>
#include <amdis/gridfunctions/GridFunction.hpp>

namespace AMDiS
{
  namespace Impl
  {
    template <class LF, class Sig>
    struct CheckFunctorConcept
    {
      static_assert(Concepts::Functor<LF, Sig>, "Derivative of LocalFunction can not be called as a functor.");
    };

    template <class Traits>
    struct CheckValidRange
    {
      static_assert(!std::is_same_v<typename Traits::Range, Dune::Functions::InvalidRange>, "Invalid Range.");
    };
  }

  /// \class DerivativeGridFunction
  /// \brief A Gridfunction that returns the derivative when calling localFunction.
  /**
   * \ingroup GridFunctions
   * Wraps the GridFunction so that \ref localFunction returns a LocalFunction
   * representing the derivative of the LocalFunction of the GridFunction.
   *
   * \tparam GridFunction The GridFunction that is wrapped.
   * \tparam The type of derivative, i.e. one of tag::gradient, tag::partial{i}, or tag::divergence
   *
   * **Requirements:**
   * - `GridFunction` models \ref Concepts::GridFunction and the LocalFunction
   *   has a derivative.
   **/
  template <class GridFunction, class Type>
  class DerivativeGridFunction
  {
    using GridFctRange = typename GridFunction::Range;
    using GridFctDomain = typename GridFunction::Domain;
    using RawSignature = typename Dune::Functions::SignatureTraits<GridFctRange(GridFctDomain)>::RawSignature;

    using Traits = DerivativeTraits<RawSignature, Type>;
    using LocalFunction = TYPEOF( derivativeOf(localFunction(std::declval<GridFunction>()), std::declval<Type>()) ) ;

    using LocalFctRange = typename Traits::Range;
    using LocalFctDomain = typename GridFunction::EntitySet::LocalCoordinate;

    CHECK_CONCEPT(Impl::CheckValidRange<Traits>);
    CHECK_CONCEPT(Impl::CheckFunctorConcept<LocalFunction, LocalFctRange(LocalFctDomain)>);

    enum { hasDerivative = false };

  public:
    /// The Range of the derivative of the GridFunction
    using Range = typename Traits::Range;

    /// The domain of the GridFunction
    using Domain = GridFctDomain;

    /// The EntitySet of the GridFunction
    using EntitySet = typename GridFunction::EntitySet;

  public:
    /// Constructor. Stores a copy of gridFct.
    DerivativeGridFunction(GridFunction const& gridFct, Type const& type)
      : gridFct_{gridFct}
      , type_{type}
    {}

    /// Constructor. Moves the gridFct into an instance variable.
    DerivativeGridFunction(GridFunction&& gridFct, Type const& type)
      : gridFct_{std::move(gridFct)}
      , type_{type}
    {}

    /// Evaluate derivative in global coordinates. NOTE: expensive
    Range operator()(Domain const& x) const
    {
      auto gv = entitySet().gridView();

      using GridView = decltype(gv);
      using Grid = typename GridView::Grid;
      using IS = typename GridView::IndexSet;

      Dune::HierarchicSearch<Grid,IS> hsearch{gv.grid(), gv.indexSet()};

      auto element = hsearch.findEntity(x);
      auto geometry = element.geometry();
      auto localFct = makeLocalFunction();
      localFct.bind(element);
      return localFct(geometry.local(x));
    }

    /// Return the derivative-localFunction of the GridFunction.
    LocalFunction makeLocalFunction() const
    {
      return derivativeOf(localFunction(gridFct_), type_);
    }

    /// Return the \ref EntitySet of the \ref GridFunction.
    EntitySet const& entitySet() const
    {
      return gridFct_.entitySet();
    }

  private:
    GridFunction gridFct_;
    Type type_;
  };


  /// \fn derivativeOf
  /// \brief Create a GridFunction representing the derivative of the given
  /// Gridfunction.
  /**
   * A GridFunction can be differentiated if the corresponding LocalFunction
   * provides a free function `derivativeOf()`
   *
   * **Requirements:**
   * - The type `GridFct` models the concept of a GridFunction
   * - The `GridFct` has no own `derivativeOf()` function, i.e. it holds
   *   `GridFct::hasDerivative == false`.
   * - The localFunction of the `GridFct` models `Concepts::HasDerivative`.
   **/
  template <class GridFct, class Type,
            class LocalFct = decltype( localFunction(std::declval<GridFct>()) ),
    REQUIRES(not GridFct::hasDerivative)>
  auto derivativeOf(GridFct const& gridFct, Type const& type)
  {
    static_assert(Concepts::HasDerivative<LocalFct,Type>,
      "derivativeOf(LocalFunction,type) not defined!");
    return DerivativeGridFunction<GridFct,Type>{gridFct, type};
  }


#ifndef DOXYGEN
  template <class Expr, class Type>
  struct DerivativePreGridFunction
  {
    using Self = DerivativePreGridFunction;

    struct Creator
    {
      template <class GridView>
      static auto create(Self const& self, GridView const& gridView)
      {
        return derivativeOf(makeGridFunction(self.expr_, gridView), self.type_);
      }
    };

    Expr expr_;
    Type type_ = {};
  };

  namespace Traits
  {
    template <class Expr,class Type>
    struct IsPreGridFunction<DerivativePreGridFunction<Expr,Type>>
      : std::true_type {};
  }
#endif


  /// \fn gradientOf
  /// \brief Generator function for DerivativeGridFunction expressions.
  /// \relates DerivativeGridFunction
  /**
   * \ingroup GridFunctions
   * Generates a Gridfunction representing the gradient of a GridFunction.
   * See \ref DerivativeGridFunction.
   *
   * **Examples:**
   * - `gradientOf(prob.solution(_0))`
   * - `gradientOf(X(0) + X(1) + prob.solution(_0))`
   **/
  template <class Expr>
  auto gradientOf(Expr const& expr)
  {
    return DerivativePreGridFunction<Expr, tag::gradient>{expr};
  }

  /// Generates a Gridfunction representing the divergence of a vector-valued GridFunction.
  template <class Expr>
  auto divergenceOf(Expr const& expr)
  {
    return DerivativePreGridFunction<Expr, tag::divergence>{expr};
  }

  /// Generates a Gridfunction representing the partial derivative of a GridFunction.
  template <class Expr>
  auto partialDerivativeOf(Expr const& expr, std::size_t i)
  {
    return DerivativePreGridFunction<Expr, tag::partial>{expr, tag::partial{i}};
  }

  /** @} **/

} // end namespace AMDiS
