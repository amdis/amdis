#pragma once

#include <type_traits>

#include <dune/grid/utility/hierarchicsearch.hh>

#include <amdis/algorithm/Transform.hpp>
#include <amdis/gridfunctions/GridFunction.hpp>
#include <amdis/linearalgebra/VectorFacade.hpp>

namespace AMDiS {

// Evaluate DiscreteFunction in global coordinates
template <class C, class GB, class TP, class R>
typename DiscreteFunction<C const,GB,TP,R>::Range DiscreteFunction<C const,GB,TP,R>::
  operator()(Domain const& x) const
{
  using Grid = typename GlobalBasis::GridView::Grid;
  using IS = typename GlobalBasis::GridView::IndexSet;

  auto const& gv = this->basis().gridView();
  Dune::HierarchicSearch<Grid,IS> hsearch{gv.grid(), gv.indexSet()};

  auto element = hsearch.findEntity(x);
  auto geometry = element.geometry();
  auto localFct = localFunction(*this);
  localFct.bind(element);
  return localFct(geometry.local(x));
}


// Interpolation of GridFunction to DOFVector
template <class C, class GB, class TP, class R>
  template <class Expr, class Tag>
void DiscreteFunction<C,GB,TP,R>::
  interpolate_noalias(Expr&& expr, Tag strategy)
{
  auto const& basis = this->basis();
  auto const& treePath = this->treePath();

  auto&& gf = makeGridFunction(FWD(expr), basis.gridView());

  auto interpolate = InterpolatorFactory<Tag>::create(basis, treePath);
  interpolate(this->coefficients(), gf);
}


// Interpolation of GridFunction to DOFVector
template <class C, class GB, class TP, class R>
  template <class Expr, class Tag>
void DiscreteFunction<C,GB,TP,R>::
  interpolate(Expr&& expr, Tag strategy)
{
  // create temporary copy of data
  Coefficients tmp(coefficients());

  Self tmpView{tmp, this->basis(), this->treePath()};
  tmpView.interpolate_noalias(FWD(expr), strategy);

  // move data from temporary vector into stored DOFVector
  coefficients() = std::move(tmp);
}

} // end namespace AMDiS
