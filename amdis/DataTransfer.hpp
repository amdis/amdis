#pragma once

#include <map>
#include <memory>

#include <dune/common/concept.hh>
#include <dune/functions/common/typeerasure.hh>
#include <amdis/Output.hpp>

namespace AMDiS {
namespace Impl {

template <class B, class C>
struct DataTransferDefinition
{
  // Definition of the interface a DataTransfer class must fulfill
  struct Interface
  {
    virtual ~Interface() = default;
    virtual void preAdapt(B const& basis, C const& container, bool mightCoarsen) = 0;
    virtual void adapt(B const& basis, C& container) = 0;
    virtual void postAdapt(C& container) = 0;
  };

  // Templatized implementation of the interface
  template <class Impl>
  struct Model : public Impl
  {
    using Impl::Impl;
    void preAdapt(B const& b, C const& c, bool mc) final { this->get().preAdapt(b,c,mc); }
    void adapt(B const& b, C& c) final { this->get().adapt(b,c); }
    void postAdapt(C& c) final { this->get().postAdapt(c); }
  };

  // The Concept definition of a DataTransfer
  struct Concept
  {
    template <class DT>
    auto require(DT&& dt) -> decltype
    (
      dt.preAdapt(std::declval<B const&>(), std::declval<C const&>(), true),
      dt.adapt(std::declval<B const&>(), std::declval<C&>()),
      dt.postAdapt(std::declval<C&>())
    );
  };

  using Base = Dune::Functions::TypeErasureBase<Interface,Model>;
};

} // end namespace Impl


/**
 * \addtogroup Adaption
 * @{
 **/

/// \brief The base class for data transfer classes.
/**
 * Type-erasure base class for data-transfer.
 *
 * \tparam Basis      The global basis, the data to interpolate is defined on.
 * \tparam Container  A vector type for storign the data to interpolate.
 **/
template <class Basis, class Container>
class DataTransfer
    : public Impl::DataTransferDefinition<Basis,Container>::Base
{
  using Definition = Impl::DataTransferDefinition<Basis,Container>;
  using Super = typename Definition::Base;

public:
  /// \brief Constructor from a type supporting the DataTransferInterface.
  template <class Impl, Dune::disableCopyMove<DataTransfer,Impl> = 0>
  DataTransfer(Impl&& impl)
    : Super{FWD(impl)}
  {
    static_assert(Concepts::models<typename Definition::Concept(Impl)>,
      "Implementation does not model the DataTransfer concept.");
  }

  /// \brief Default Constructor.
  DataTransfer() = default;

  /// \brief Collect data that is needed before grid adaption.
  /**
   * \param basis         The global basis before the grid is updated.
   * \param container     The original data before grid adaption, can be used to create a
   *                      persistent storage.
   * \param mightCoarsen  Flag to indicate whether there are elements marked for coarsening.
   **/
  void preAdapt(Basis const& basis, Container const& container, bool mightCoarsen)
  {
    this->asInterface().preAdapt(basis, container, mightCoarsen);
  }

  /// \brief Interpolate data to new grid after grid adaption.
  /**
   * \param basis      The global basis after the grid is updated
   * \param container  The original data vector not yet updated. Should be adapted in this method.
   **/
  void adapt(Basis const& basis, Container& container)
  {
    this->asInterface().adapt(basis, container);
  }

  /// \brief Perform cleanup after grid adaption
  /**
   * \param container  The data vector after any adaption and data transfer
   **/
  void postAdapt(Container& container)
  {
    this->asInterface().postAdapt(container);
  }
};


namespace tag {

  /// \brief Base tag type for all data transfer tags.
  struct datatransfer {};

} // end namespace tag


//// \brief Factory class to create DataTransfer based on a tag.
/**
 * Specializations of this factory can be used to generically construct a DataTransfer
 * based on a given <tag>. The <tag> should be derived from `tag::datatransfer`.
 **/
template <class Tag>
struct DataTransferFactory
{
  template <class Basis, class Container>
  static DataTransfer<Basis,Container> create(Basis const&, Container const&)
  {
    error_exit("Unknown <tag> passed to the DataTransferFactory.");
    return DataTransfer<Basis,Container>{};
  }
};

/// @}

} // end namespace AMDiS
