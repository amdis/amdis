#pragma once

#include <type_traits>

#include <dune/functions/common/typeerasure.hh>

#include <amdis/ContextGeometry.hpp>
#include <amdis/common/ConceptsBase.hpp>
#include <amdis/common/TypeTraits.hpp>

namespace AMDiS {
namespace Impl {

template <class Traits, class... Nodes>
struct LocalOperatorDefinition
{
  using MatVec = typename Traits::ElementContainer;
  using CG = ContextGeometry<typename Traits::LocalContext>;
  using Element = typename CG::Element;

  // Definition of the interface a LocalOperator must fulfill
  struct Interface
  {
    virtual ~Interface() = default;
    virtual void bind(Element const&) = 0;
    virtual void unbind() = 0;
    virtual void assemble(CG const&, Nodes const&..., MatVec&) const = 0;
  };

  // Templatized implementation of the interface
  template <class Impl>
  struct Model : public Impl
  {
    using Impl::Impl;
    void bind(Element const& e) final { this->get().bind(e); }
    void unbind() final { this->get().unbind(); }
    void assemble(CG const& cg, Nodes const&... ns, MatVec& mv) const final {
      this->get().assemble(cg,ns...,mv); }
  };

  // The Concept definition of a LocalOperator
  struct Concept
  {
    template <class LOp>
    auto require(LOp&& lop) -> decltype
    (
      lop.bind(std::declval<Element>()),
      lop.unbind(),
      lop.assemble(std::declval<CG>(), std::declval<Nodes>()..., std::declval<MatVec&>())
    );
  };

  using Base = Dune::Functions::TypeErasureBase<Interface, Model>;
};

} // end namespace Impl


/**
 * \addtogroup operators
 * @{
 **/

/// \brief The base class for a local operator to be used in a \ref Assembler.
/**
 * Type-erasure base class for local operators.
 *
 * \tparam Traits    Parameters for the LocalOperator collected in a class.
 *                   See \ref Impl::OperatorTraits.
 * \tparam Nodes...  Basis node (pair) the operator is associated to.
 **/
template <class Traits, class... Nodes>
class LocalOperator
    : public Impl::LocalOperatorDefinition<Traits,Nodes...>::Base
{
  using Definition = Impl::LocalOperatorDefinition<Traits,Nodes...>;
  using Super = typename Definition::Base;

public:
  /// Type of the element matrix or element vector
  using MatVec = typename Traits::ElementContainer;

  /// The geometry of the local context, see \ref ContextGeometry
  using ContextGeo = ContextGeometry<typename Traits::LocalContext>;

  /// The grid entity of codim=0
  using Element = typename ContextGeo::Element;

public:
  /// \brief Constructor. Pass any type supporting the \ref LocalOperatorInterface
  template <class Impl, Dune::disableCopyMove<LocalOperator,Impl> = 0>
  LocalOperator(Impl&& impl)
    : Super{FWD(impl)}
  {
    static_assert(Concepts::models<typename Definition::Concept(Impl)>,
      "Implementation does not model the LocalOperator concept.");
  }

  /// \brief Default Constructor.
  LocalOperator() = default;

  /// \brief Binds the local operator to an `element`.
  /**
   * Binding an operator to the currently visited element in grid traversal.
   **/
  void bind(Element const& element)
  {
    this->asInterface().bind(element);
  }

  /// \brief Unbinds operator from element.
  void unbind()
  {
    this->asInterface().unbind();
  }

  /// \brief Assemble a local element matrix or vector on the element that is bound.
  /**
   * \param contextGeo  Container for geometry related data, \ref ContextGeometry
   * \param nodes...    The node(s) of the test-basis (and trial-basis)
   * \param mv          Element-matrix or vector
   **/
  void assemble(ContextGeo const& cg, Nodes const&... ns, MatVec& mv) const
  {
    this->asInterface().assemble(cg,ns...,mv);
  }
};

/// @}

} // end namespace AMDiS
