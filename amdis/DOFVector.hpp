#pragma once

#include <memory>
#include <utility>

#include <dune/common/concept.hh>
#include <dune/common/shared_ptr.hh>
#include <dune/functions/functionspacebases/concepts.hh>

#include <amdis/DataTransfer.hpp>
#include <amdis/LinearAlgebra.hpp>
#include <amdis/Observer.hpp>
#include <amdis/common/Concepts.hpp>
#include <amdis/common/DerivativeTraits.hpp>
#include <amdis/common/TypeTraits.hpp>
#include <amdis/datatransfers/InterpolationDataTransfer.hpp>
#include <amdis/functions/GlobalBasis.hpp>
#include <amdis/typetree/TreePath.hpp>

namespace AMDiS
{
  // forward declaration
  template <class, class, class, class>
  class DiscreteFunction;

  // forward declaration
  template <class, class>
  class DerivativeGridFunction;


  /// \brief The basic container that stores a base vector and a corresponding basis
  /**
   * \tparam GB  Basis of the vector
   * \tparam T   Type of the coefficients
   * \tparam Traits  Collection of parameter for the linear-algebra backend
   **/
  template <class GB, class T = double, class Traits = BackendTraits>
  class DOFVector
      : public VectorFacade<T, Traits::template Vector<GB>::template Impl>
      , private Observer<event::preAdapt>
      , private Observer<event::adapt>
      , private Observer<event::postAdapt>
  {
    using Self = DOFVector;

    template <class, class, class, class>
    friend class DiscreteFunction;

  public:
    /// A global basis associated to the coefficients
    using GlobalBasis = GB;

    /// The internal coefficient vector storage
    using Coefficients = VectorFacade<T, Traits::template Vector<GB>::template Impl>;

    /// The index/size - type
    using size_type  = typename GlobalBasis::size_type;

    /// The type of the elements of the DOFVector
    using value_type = T;

    template <class Self>
    static auto& coefficients(Self& self)
    {
      if constexpr(std::is_const_v<Self>)
        return static_cast<Coefficients const&>(self);
      else
        return static_cast<Coefficients&>(self);
    }

  public:
    /// (1) Constructor. Stores the shared_ptr of the basis and creates a new
    /// DataTransfer.
    template <class DataTransferTag = tag::interpolation_datatransfer>
    DOFVector(std::shared_ptr<GB> const& basis, DataTransferTag = {})
      : Coefficients(*basis)
      , Observer<event::preAdapt>(basis->gridView().grid())
      , Observer<event::adapt>(*basis)
      , Observer<event::postAdapt>(basis->gridView().grid())
      , dataTransfer_(DataTransferFactory<DataTransferTag>::create(*basis, coefficients(*this)))
      , basis_(basis)
    {
      resizeZero();
    }

    /// (2) Constructor. Forwards to (1) by wrapping reference into non-destroying
    /// shared_ptr, see \ref Dune::wrap_or_move.
    template <class GB_, class DataTransferTag = tag::interpolation_datatransfer,
      REQUIRES(Concepts::Similar<GB_,GB>)>
    DOFVector(GB_&& basis, DataTransferTag tag = {})
      : DOFVector(Dune::wrap_or_move(FWD(basis)), tag)
    {}

    /// (3) Constructor. Forwards to (1) by creating a new basis from a dune-functions
    /// pre-basis factory.
    template <class GV, class PBF, class DataTransferTag = tag::interpolation_datatransfer,
      REQUIRES(Concepts::PreBasisFactory<PBF, GV>)>
    DOFVector(GV const& gridView, PBF const& preBasisFactory, DataTransferTag tag = {})
      : DOFVector(std::make_shared<GB>(gridView, preBasisFactory), tag)
    {}

    /// Return the global basis
    GlobalBasis const& basis() const
    {
      return *basis_;
    }

    Coefficients const& coefficients() const
    {
      return coefficients(*this);
    }

    Coefficients& coefficients()
    {
      return coefficients(*this);
    }

    /// Write DOFVector to file
    void backup(std::string const& filename);

    /// Read backup data from file
    void restore(std::string const& filename);

    void resize()
    {
      Coefficients::resize(*basis_);
    }

    using Coefficients::resize;

    void resizeZero()
    {
      Coefficients::resizeZero(*basis_);
    }

    using Coefficients::resizeZero;

    /// Return the associated DataTransfer object
    auto const& dataTransfer() const
    {
      return dataTransfer_;
    }

    /// Return the associated DataTransfer object
    auto& dataTransfer()
    {
      return dataTransfer_;
    }

    /// Assign the DataTransfer object
    void setDataTransfer(DataTransfer<GB,Coefficients> dataTransfer)
    {
      dataTransfer_ = std::move(dataTransfer);
    }

    /// Create a new DataTransfer object based on the operation type
    template <class DataTransferTag>
    void setDataTransfer(DataTransferTag tag)
    {
      setDataTransfer(DataTransferFactory<DataTransferTag>::create(basis(), coefficients()));
    }


  public: // functions to transform the DOFVector into a grid-function

    /// A Generator to transform a DOFVector into a \ref DiscreteFunction
    template <class Range = void, class... Indices>
    friend auto valueOf(DOFVector& dofVec, Indices... ii)
    {
      using TP = TYPEOF(makeTreePath(ii...));
      using DF = DiscreteFunction<Coefficients,GlobalBasis,TP,Range>;
      return DF{dofVec.coefficients(), dofVec.basis_, makeTreePath(ii...)};
    }

    /// A Generator to transform a const DOFVector into a \ref DiscreteFunction
    template <class Range = void, class... Indices>
    friend auto valueOf(DOFVector const& dofVec, Indices... ii)
    {
      using TP = TYPEOF(makeTreePath(ii...));
      using DF = DiscreteFunction<Coefficients const,GlobalBasis,TP,Range>;
      return DF{dofVec.coefficients(), dofVec.basis_, makeTreePath(ii...)};
    }

    template <class Type>
    auto makeDerivative(Type type) const
    {
      using TP = TYPEOF(makeTreePath());
      using DF = DiscreteFunction<Coefficients const,GlobalBasis,TP,void>;
      return DerivativeGridFunction<DF,Type>{
        DF{coefficients(), basis_, makeTreePath()}, type};
    }

    /// Return a derivative grid-function of the given type, e.g., `tag::jacobian`, `tag::divergence`,
    /// or `tag::partialDerivative`
    template <class Type>
    friend auto derivativeOf(DOFVector const& dofVec, Type type)
    {
      return dofVec.makeDerivative(type);
    }

    /// Return a derivative grid-function representing the gradient (jacobian) of the DOFVector
    friend auto gradientOf(DOFVector const& dofVec)
    {
      return dofVec.makeDerivative(tag::gradient{});
    }

    /// Return a derivative grid-function representing the divergence of the DOFVector
    friend auto divergenceOf(DOFVector const& dofVec)
    {
      return dofVec.makeDerivative(tag::divergence{});
    }

    /// Return a derivative grid-function representing the partial derivative wrt. the components `comp` of the DOFVector
    friend auto partialDerivativeOf(DOFVector const& dofVec, std::size_t comp)
    {
      return dofVec.makeDerivative(tag::partial{comp});
    }

  protected:

    /// Override of Observer update(event::preAdapt) method. Redirects to preAdapt
    /// method of the \ref DataTransfer object.
    void updateImpl(event::preAdapt e) override
    {
      dataTransfer_.preAdapt(*basis_, coefficients(), e.value);
    }

    /// Override of Observer update(event::adapt) method. Redirects to adapt method
    /// of the \ref DataTransfer object.
    void updateImpl(event::adapt e) override
    {
      assert(e.value);
      resize();
      dataTransfer_.adapt(*basis_, coefficients());
    }

    /// Override of Observer update(event::postAdapt) method. Redirects to postAdapt
    /// method of the \ref DataTransfer object.
    void updateImpl(event::postAdapt) override
    {
      dataTransfer_.postAdapt(coefficients());
    }

  private:
    /// Data interpolation when the grid changes, set by default
    /// to \ref DataTransferOperation::INTERPOLATE.
    DataTransfer<GB,Coefficients> dataTransfer_;

    std::shared_ptr<GlobalBasis const> basis_;
  };


  // deduction guides
  template <class GB>
  DOFVector(GB&& basis)
    -> DOFVector<Underlying_t<GB>>;

  template <class GB, class DataTransferTag,
    REQUIRES(std::is_base_of_v<tag::datatransfer, DataTransferTag>)>
  DOFVector(GB&& basis, DataTransferTag)
    -> DOFVector<Underlying_t<GB>>;

  template <class GV, class PBF,
    REQUIRES(Concepts::PreBasisFactory<PBF, GV>)>
  DOFVector(GV const& gridView, PBF const& pbf)
    -> DOFVector<decltype(GlobalBasis{gridView,pbf})>;

  template <class GV, class PBF, class DataTransferTag,
    REQUIRES(Concepts::PreBasisFactory<PBF, GV>),
    REQUIRES(std::is_base_of_v<tag::datatransfer, DataTransferTag>)>
  DOFVector(GV const& gridView, PBF const& pbf, DataTransferTag)
    -> DOFVector<decltype(GlobalBasis{gridView,pbf})>;


  /// \brief Create a DOFVector from a basis.
  /**
   * This generator function accepts the basis as reference, temporary, or
   * shared_ptr. Internally the reference is wrapped into a non-destroying
   * shared_ptr and the temporary is moved into a new shared_ptr.
   *
   * The DataTransferOperation controls what is done during grid changes with the
   * DOFVector. The default is interpolation of the data to the new grid. See
   * \ref DataTransferOperation for more options.
   **/
  template <class ValueType = double, class GB,
            class DataTransferTag = tag::interpolation_datatransfer>
  DOFVector<Underlying_t<GB>, ValueType>
  makeDOFVector(GB&& basis, DataTransferTag tag = {})
  {
    return {FWD(basis), tag};
  }

} // end namespace AMDiS

#include <amdis/DOFVector.inc.hpp>
