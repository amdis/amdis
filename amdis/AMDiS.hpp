#pragma once

#include <config.h>

#include <amdis/AdaptInfo.hpp>
#include <amdis/Environment.hpp>
#include <amdis/GridFunctions.hpp>
#include <amdis/Initfile.hpp>
#include <amdis/Integrate.hpp>
#include <amdis/LinearAlgebra.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/Output.hpp>
#include <amdis/ProblemStat.hpp>

namespace AMDiS
{
  /// Initialized the Environment for MPI.
  /**
   * \deprecated
   *
   * Use \ref Environment at the beginning of the main() instead:
   *
   *   int main(int argc, char** argv) {
   *      Environment env(argc, argv);
   *      // do somthing.
   *   }
   *
   **/
  [[deprecated("Use Environment at the beginning of main() instead")]]
  inline void init(int& argc, char**& argv, std::string const& initFileName = "")
  {
    static Environment env(argc, argv, initFileName);
  }

  /// Closes the MPI environment
  /**
   * \deprecated
   *
   * The \ref Environment is closed automatically on destruction.
   **/
  [[deprecated("Use Environment at the beginning of main() instead")]]
  inline void finalize() { /* no nothing */ }

} // end namespace AMDiS
