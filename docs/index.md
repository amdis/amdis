# AMDiS

The *Adaptive Multi-Dimensional Simulation Toolbox* (AMDiS) is a Finite-Element
discretization module allowing for fast prototyping of PDEs on adaptively refined
grids. It is implemented on top of the [Dune](https://dune-project.org) framework.

1. [Getting Started](getting-started.md):
For information about how to install AMDiS and dependencies, see the
Getting Started guide.

2. [Tutorials](tutorials/tutorials.md):
An overview about how to discretize a PDE, how to work with the grid and the
data and how to control the simulation workflow is given in the Tutorials.

3. [Reference](reference/DOFVector.md):
A more detailed explanation of the interface of some selected classes and functions.
This can be seen as an extension to the automatically generated Doxygen documentation

4. [API Documentation](api/index.html):
Automatically generated documentation by [doxygen](http://www.doxygen.nl/) from the source
code files in the AMDiS repository.
