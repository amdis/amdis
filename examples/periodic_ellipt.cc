#include <amdis/AMDiS.hpp>
#include <amdis/AdaptStationary.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/ProblemStat.hpp>

#include <dune/grid/yaspgrid.hh>

using namespace AMDiS;
using namespace Dune::Functions::BasisFactory;

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  // Define the problem setup
  auto g = [](auto const& x){ return std::sin(2*dot(x,x)*M_PI); };

  // Construct a grid
  using Grid = Dune::YaspGrid<2>;
  Grid grid({1.0, 1.0}, {10, 10});

  ProblemStat prob("ellipt", grid, lagrange<2>());
  prob.initialize(INIT_ALL);

  auto opL = makeOperator(tag::gradtest_gradtrial{}, 1.0);
  prob.addMatrixOperator(opL);

  auto opForce = makeOperator(tag::test{}, 1.0);
  prob.addVectorOperator(opForce);

  // Set boundary condition: left-right periodic, bottom-top dirichlet
  prob.boundaryManager()->setBoxBoundary({-1,-1,1,1});
  prob.addDirichletBC(1, g);
  prob.addPeriodicBC(-1, {{1.0,0.0},{0.0,1.0}}, {1.0,0.0});

  AdaptInfo adaptInfo("adapt");
  AdaptStationary runner{"adapt", prob, adaptInfo};
  runner.adapt();
  return 0;
}
