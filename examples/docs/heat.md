Time dependant scalar problem {#heat}
=====
[TOC]

# Introduction {#intro}
In this example we will solve the heat equation with Dirichlet boundary condition
\f{eqnarray*}{
  - \partial_t u - \Delta u &=& f &\text{ in } \Omega \times (t_{begin}, t_{end}) \\
  u &=& g &\text{ on } \Gamma \times (t_{begin}, t_{end}) \\
  u &=& u_0 &\text{ on } \Omega \times t_{begin}
\f}
whereby \f$ \Omega \subset \mathbb{R}^d, \quad 1 \le d \le 3 \f$ denotes the domain and \f$ \Gamma = \{ x \in \partial \Omega | \exists i \in \{1, ..., d\}: x_i = 0 \}  \f$ a subset of its boundary. We solve the problem in the time interval \f$ (t_{begin}, t_{end}) \f$ with Dirichlet boundary conditions on \f$ \Gamma \f$ and set

\f{eqnarray*}{
  f(x,t) &=& -1 \\
  g(x,t) &=& 0 \\
  u_0(x) &=& 0.
\f}

## Discretization {#disc}

In this example we use the implicit Euler scheme
\f[
  \frac{u^{new} - u^{old}}{\tau} - \Delta u^{new} = f(\cdot, t^{old} + \tau)
\f]
where \f$ \tau = t^{new} - t^{old} \f$ is the timestep size between the old and new problem time. \f$ u^{new} \f$ is the searched solution at \f$ t = t^{new} \f$, \f$ u^{old} \f$ is the solution at \f$ t = t^{old} \f$, which is already known from the last timestep. If we bring all terms that depend on \f$ u^{old} \f$ to the right hand side, the equation reads
\f[
  \frac{u^{new}}{\tau} - \Delta u^{new} =  - \frac{u^{old}}{\tau} + f(\cdot, t^{old} + \tau).
\f]

## Implementation {#impl}

We will now look at the source code of the example in detail. You can also find the complete source code for all files [below](#see-also).

### Source code description {#source}

First we include the required AMDiS headers. `AMDiS.hpp` is a collection of headers that are commonly used. In this example we also use the [`ProblemStat`](../reference/Problem#class-problemstat) and [`ProblemInstat`](../reference/Problem#class-probleminstat) classes, which we explicitly include. We will also use the predefined operators in `LocalOperators.hpp`.
\snippetlineno heat.cc includes

On the following lines we define the grid manager and finite element space used in this example - a structured axis-aligned cube grid with nodal (lagrange) basis functions of order 2. We set name aliases for ease of notation for the basis and problem types. Note that `GRIDDIM` is a constant passed by the buildsystem. By default we build the example for \f$ GRIDDIM = 2 \f$.
\snippetlineno heat.cc typedefs

Most AMDiS classes and functions are found in the *AMDiS namespace*. To make life easier for us we therefore tell the compiler to look for our classes in this namespace.
\snippetlineno heat.cc namespace amdis

Our example begins with a call to the `main` function. Any command line argument we pass can be used with the `argc` and `argv` arguments. As every AMDiS program requires us to set up an `Environment` at the start we do that here.
\snippetlineno heat.cc main begin

Now we create the stationary base problem using the `ProblemStat` class, as well as the instationary problem using the `ProblemInstat` class and initialize both. Note that we name both problem instances *heat* and initialize everything. We also set up an `AdaptInfo` object that controls the adaptation procedure later on.
\snippetlineno heat.cc problem definition

Next we add the operators that define our problem. Note that we store references to the inverse of the time step size `invTau` and the last solution `prob.solution()` so we use the correct values every timestep. See the [operator reference page](../reference/Operators) for descriptions of the operators used here.
\snippetlineno heat.cc operators

Lastly we set the constraints: We have dirichlet boundary conditions on the left and lower part of the boundary, as indicated by the predicate function and prescribe a value of zero.
\snippetlineno heat.cc constraints

With everything set up we can now create a helper object `AdaptInstationary` that performs the time-stepping scheme automatically using the parameters given by the `AdaptInfo` object we created earlier. We only need to call the `adapt()` function.
\snippetlineno heat.cc adapt

With that the example ends. We return 0 to indicate a successful run.
\snippetlineno heat.cc main end

### Parameter file {#params}

We describe the options used by the example's parameter file `heat.dat.2d`. For a full description of all options see [the reference page](../reference/Initfile#list-of-parameters-and-values).

\dontinclude init/heat.dat.2d
Parameters for the grid "heatMesh":
- global refinements: number of global refinement steps done as part of initialization
- min corner: lower left corner of the domain
- max corner: upper right corner of the domain
- num cells: number of grid cells in x- and y-direction

\skip heatMesh
\until num cells

`heat->mesh` specifies the name of the grid used by the problem named "heat".
\skipline heat->mesh

Parameters for the solver employed by the problem "heat":
- solver: type of the solver, pcg means preconditioned conjugate gradient method
- info: verbosity of the output generated by the solver
- absolute tolerance: error tolerance that needs to be reached before the solver ends the iteration
- max iteration: maximum number of iterations until the solver aborts
- precon: preconditioner used in the computation, ilu means incomplete LU-deconposition

\skip heat->solver
\until precon

Parameters for the output generated by the problem "heat":
- format: output format, here we generate a backup file
- filename: name of the output file
- name: name of the solution within the output file
- output directory: directory where the output files are generated
- animation: write an animated file opposed to just a single timestep

\skip heat->output
\until animation

Parameters for the time stepping scheme:
- timestep: timestep size
- start time: \f$ t_{begin}\f$
- end time: \f$ t_{end}\f$

\skip adapt
\until end time

### Running the example {#running}
Using a console we navigate to the examples directory where the `heat.2d` file resides and run the file while also passing the [necessary arguments](#args).

Running `heat.2d` in bash could look like this:
```
<amdis-root>/build-cmake/examples $ ./heat.2d "<amdis-root>/examples/init/heat.dat.2d"
```

#### Command line arguments {#args}

- Initfile, <amdis-root>/examples/init/heat.dat.#d

#### Output {#output}

In this section we show some sample output of `heat.2d` using the default arguments.

The example produces a series of output blocks like the one below for each time step. In each of them the current simulation time and time step size is printed before the inner iteration begins. Some performance information is printed while the iteration is in progress. After one step the inner iteration finishes and an output file is generated.
\verbatim
time = 0.1, timestep = 0.1

begin of iteration number: 1
=============================
markElements needed 7.7e-08 seconds
4225 local DOFs
fill-in of assembled matrix: 66049
assemble needed 0.0135029 seconds
=== GeneralizedPCGSolver
=== rate=0.395649, T=0.00769608, TIT=0.000481005, IT=17
solution of discrete system needed 0.0489367 seconds

end of iteration number: 1
=============================
writeFiles needed 0.00137441 seconds
...
\endverbatim

Using [ParaView](http://www.paraview.org) we can inspect the output file and obtain a visualization that looks like this.
![Solution of heat.2d at t = 0.2](/img/heat2d_02.svg)
![Solution of heat.2d at t = 0.6](/img/heat2d_06.svg)
![Solution of heat.2d at t = 1.0](/img/heat2d_10.svg)

### See also {#see-also}
The complete source code for the example and the parameter file can be found here:
- \subpage heat_source "heat.cc"
- \subpage heat_init1 "heat.dat.2d"

\page heat_source heat.cc
\includelineno heat.cc

\page heat_init1 heat.dat.2d
\includelineno init/heat.dat.2d
