Stationary problem with Dirichlet boundary condition {#ellipt}
=====
[TOC]

# Introduction {#intro}
In this example we will solve Poisson's equation with Dirichlet boundary condition
\f{eqnarray*}{
  - \Delta u &=& f &\text{ in } \Omega \\
  u &=& g &\text{ on } \Gamma
\f}
whereby \f$ \Omega \subset \mathbb{R}^d, \quad 1 \le d \le 3 \f$ denotes the domain and \f$ \Gamma = \partial \Omega \f$ its boundary.

We set the right hand side functions to
\f{eqnarray*}{
  f &=& -(400 \| x \|^2 - 20 d) e^{-10 \|x\|^2} \\
  g &=& e^{-10.0 \|x\|^2}.
\f}

The example code will solve the problem on a series of coarse to fine grids and compute the errors in \f$ L_2 \f$ and \f$ H_1 \f$ norm as well as experimental order of convergence (EOC).

## Discretization {#disc}

For using a Finite Element Method we derive a weak formulation
\f[
  \int_\Omega \nabla u \cdot \nabla v = \int_\Omega f v  \quad \forall v \in V.
\f]
with solutions \f$ u \in \{ w \in H^1(\Omega) : tr_{\Gamma} w = g \} \f$ and trial space \f$ V = H^1_0(\Omega) = \{ w \in H^1(\Omega) : tr_{\Gamma} w = 0 \} \f$.

## Implementation{#impl}

We will now look at the source code of the example in detail. You can also find the complete source code for all files [below](#see-also).

### Source code description{#source}

First we include the required AMDiS headers. `AMDiS.hpp` is a collection of headers that are commonly used. In this example we also use the [`ProblemStat`](../reference/Problem#class-problemstat) class, which we explicitly include. We will also use the predefined operators in `LocalOperators.hpp` as well as the integrate function from `Integrate.hpp`.
\snippetlineno ellipt.cc includes

On the following lines we include the grid manager used in this example - a structured axis-aligned cube grid. We set a name alias `Grid` for the type. Note that `GRIDDIM` is a constant passed by the buildsystem. By default we build the example for the values 2 and 3.
\snippetlineno ellipt.cc gridinclude

Most AMDiS classes and functions are found in the *AMDiS namespace*. To make life easier for us we therefore tell the compiler to look for our classes in this namespace.
\snippetlineno ellipt.cc namespace amdis

Our example begins with a call to the `main` function. Any command line argument we pass can be used with the `argc` and `argv` arguments.
\snippetlineno ellipt.cc main begin

Every AMDiS program requires us to set up an `Environment`, so we do that here. We also start a timer to measure the execution time. Using the second command line argument we set the number of grid refinement steps.
The next step is setting up our right hand side and boundary condition functors. We do that using a lambda function that takes a world coordinate as an argument.
\snippetlineno ellipt.cc parameter definition

Now we define the Finite Element space we want to work with - in this case using second order lagrange elements - and create and initialize the `ProblemStat` instance with the name *ellipt*. This class will contain all information about our problem as well as the means to solve it.
\snippetlineno ellipt.cc problem definition

Next we tell the problem class what our PDE looks like. We add an operator of the form \f$ \int c \nabla u \cdot \nabla v\f$ with \f$ c = 1.0 \f$ as well as one of the form \f$ \int f v \f$ with a specified quadrature order of 6.
\snippetlineno ellipt.cc operators

Lastly we set the constraints: We have dirichlet boundary conditions on the complete boundary (since we did not specify any boundary segments the identifier 1 includes the complete boundary).
\snippetlineno ellipt.cc constraints

Before we start with the adapt-solve-loop we set up a helper object `AdaptInfo` that controls how the adaptation process is handled. We also initialize containers to store the error norms and element sizes in.
\snippetlineno ellipt.cc adapt parameter definition

We now start with the adaptation loop by globally refining the domain.
\snippetlineno ellipt.cc main loop begin

In this block we loop over all elements and calculate the maximum of the element widths.
\snippetlineno ellipt.cc main loop element widths calc

Here we tell the problem to assemble the operators we defined above and solve the resulting linear system of equations.
\snippetlineno ellipt.cc main loop assemble solve

At the end of the loop we calculate the error norms. Note that *g* is the exact solution of Poisson's equation.
\snippetlineno ellipt.cc main loop end

After we finished the adaptation loop for the last time we write the final solution to an output file defined in the initfile. We also print a table of the error norms and the EOC as well as the total execution time.
\snippetlineno ellipt.cc output

With that the example ends. We return 0 to indicate a successful run.
\snippetlineno ellipt.cc main end

### Parameter file{#params}

We describe the options used by the example's parameter files. For a full description of all options see [the reference page](../reference/Initfile#list-of-parameters-and-values).

#### ellipt.dat.2d {#params2d}
\dontinclude init/ellipt.dat.2d
Parameters for the grid "elliptMesh":
- global refinements: number of global refinement steps done as part of initialization
- num cells: number of grid cells in x- and y-direction
- overlap: size of the interface between neighbouring subdomains in parallel computations

\skip elliptMesh
\until overlap

`ellipt->mesh` specifies the name of the grid used by the problem named "ellipt".
\skipline ellipt->mesh

This parameter specifies the symmetry structure of the assembled matrix.
\skipline ellipt->symmetry

Parameters for the solver employed by the problem "ellipt":
- solver: type of the solver, pcg means preconditioned conjugate gradient method
- info: verbosity of the output generated by the solver
- max iteration: maximum number of iterations until the solver aborts
- relative tolerance: factor by which the initial defect is reduced before the solver ends the iteration
- precon: preconditioner used in the computation, ilu means incomplete LU-deconposition

\skip ellipt->solver
\until precon

Parameters for the output generated by the problem "ellipt":
- format: output format
- filename: name of the output file
- name: name of the solution within the output file
- output directory: directory where the output files are generated

\skip ellipt->output
\until directory

#### ellipt.dat.3d{#params3d}

See [ellipt.dat.2d](#params2d), except `num cells` has three entries since the grid is 3-dimensional.

### Running the example{#running}
Using a console we navigate to the examples directory where the `ellipt.#d` files reside (# can be either 2 or 3) and run the file while also passing the [necessary arguments](#args).

Running `ellipt.2d` in bash could look like this:
```
<amdis-root>/build-cmake/examples $ ./ellipt.2d "<amdis-root>/examples/init/ellipt.dat.2d" 4
```

#### Command line arguments {#args}

- Initfile, <amdis-root>/examples/init/ellipt.dat.#d
- (optional) Number grid levels used for calculation of errors and EOC

#### Output{#output}

In this section we show some sample output of `ellipt.2d` using the default arguments. The text output of `ellipt.3d` is similar.

Every time the adapt loop is run the time for a global refinement step, the number of DOFs, the number of nonzero entries inserted into the matrix and the assembly time is printed.
\verbatim
globalRefine needed 0.000197335 seconds
289 local DOFs
fill-in of assembled matrix: 4225
assemble needed 0.000797755 seconds
\endverbatim

After each assemble step the solver runs and prints convergence rate, elapsed time (total and per iteration) and total iterations until the tolerance is reached. Lastly the total time for the solve step is printed.
\verbatim
=== GeneralizedPCGSolver
=== rate=0.0718531, T=0.000249728, TIT=2.77476e-05, IT=10
solution of discrete system needed 0.0034118 seconds
\endverbatim

After the last loop iteration finishes the output file is generated an a table with error norms and EOC as well as the total execution time is printed.
\verbatim
writeFiles needed 0.195444 seconds

level | h            | |u - u*|_L2  | |u - u*|_H1  | eoc_L2       | eoc_H1
------+--------------+--------------+--------------+--------------+-------------
1     | 0.125        | 0.000378951  | 0.0197187    | 0            | 0
2     | 0.0625       | 4.79736e-05  | 0.00497982   | 2.9817       | 1.9854
3     | 0.03125      | 6.01667e-06  | 0.00124812   | 2.9952       | 1.99634
4     | 0.015625     | 7.52723e-07  | 0.000312228  | 2.99877      | 1.99908
5     | 0.0078125    | 9.41105e-08  | 7.80695e-05  | 2.99969      | 1.99977
6     | 0.00390625   | 1.17645e-08  | 1.95181e-05  | 2.99992      | 1.99994
elapsed time: 12.9328 seconds
\endverbatim

Using [ParaView](http://www.paraview.org) we can inspect the output file and obtain a visualization that looks like this.

![Solution of ellipt.2d](/img/ellipt2d.svg)
![Solution of ellipt.3d](/img/ellipt3d.svg)

### See also {#see-also}
The complete source code for the example and the parameter files can be found here:
- \subpage ellipt_source "ellipt.cc"
- \subpage ellipt_init1 "ellipt.dat.2d"
- \subpage ellipt_init2 "ellipt.dat.3d"

\page ellipt_source ellipt.cc
\includelineno ellipt.cc

\page ellipt_init1 ellipt.dat.2d
\includelineno init/ellipt.dat.2d

\page ellipt_init2 ellipt.dat.3d
\includelineno init/ellipt.dat.3d
