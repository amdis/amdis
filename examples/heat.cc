/// [includes]_begin
#include <config.h>

#include <iostream>

#include <amdis/AMDiS.hpp>
#include <amdis/AdaptInstationary.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/ProblemInstat.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/GridFunctions.hpp>
/// [includes]_end

/// [namespace amdis]_begin
using namespace AMDiS;
/// [namespace amdis]_end

/// [typedefs]_begin
// 1 component with polynomial degree 1
//using Grid = Dune::AlbertaGrid<GRIDDIM, WORLDDIM>;
using HeatParam   = YaspGridBasis<GRIDDIM, 2>;
using HeatProblem = ProblemStat<HeatParam>;
using HeatProblemInstat = ProblemInstat<HeatParam>;
/// [typedefs]_end

/// [main begin]_begin
int main(int argc, char** argv)
{
  Environment env(argc, argv);
/// [main begin]_end

  /// [problem definition]_begin
  HeatProblem prob("heat");
  prob.initialize(INIT_ALL);

  HeatProblemInstat probInstat("heat", prob);
  probInstat.initialize(INIT_UH_OLD);

  AdaptInfo adaptInfo("adapt");
  /// [problem definition]_end

  /// [operators]_begin
  auto invTau = std::ref(probInstat.invTau());

  // 1/τ*⟨u,v⟩
  auto opTimeLhs = makeOperator(tag::test_trial{}, invTau);
  prob.addMatrixOperator(opTimeLhs);

  // ⟨∇u,∇v⟩
  auto opL = makeOperator(tag::gradtest_gradtrial{}, 1.0);
  prob.addMatrixOperator(opL);

  // 1/τ*⟨u_old,v⟩
  auto opTimeRhs = makeOperator(tag::test{},
    invokeAtQP([invTau](double u) { return u * invTau.get(); }, prob.solution()), 2);
  prob.addVectorOperator(opTimeRhs);

  // (-1.0,v)
  auto opForce = makeOperator(tag::test{}, [](auto const& x) { return -1.0; }, 0);
  prob.addVectorOperator(opForce);
  /// [operators]_end


  /// [constraints]_begin
  // set boundary condition
  auto predicate = [](auto const& p){ return p[0] < 1.e-8 || p[1] < 1.e-8; };
  auto dbcValues = [](auto const& p){ return 0.0; };
  prob.addDirichletBC(predicate, dbcValues);
  /// [constraints]_end

  /// [adapt]_begin
  AdaptInstationary adapt("adapt", prob, adaptInfo, probInstat, adaptInfo);
  adapt.adapt();
  /// [adapt]_end

/// [main end]_begin
  return 0;
}
/// [main end]_end
