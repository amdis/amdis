#include <config.h>
#include <amdis/AMDiS.hpp>
#include <amdis/LocalOperators.hpp>

#include <dune/grid/onedgrid.hh>
#include <dune/functions/functionspacebases/defaultglobalbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/periodicbasis.hh>

using namespace AMDiS;
int main(int argc, char** argv)
{
  Environment env(argc, argv);

  double left = 0.0, right = 1.0;
  Dune::OneDGrid grid(10, left, right);

  // Define a periodic association for the vertices
  std::pair<std::size_t,std::size_t> association;
  auto const& indexSet = grid.leafGridView().indexSet();
  double tol = 1.e-10;
  for (auto const& v : vertices(grid.leafGridView())) {
    if (v.geometry().center() < left+tol)
      association.first = indexSet.index(v);
    else if (v.geometry().center() > right-tol)
      association.second = indexSet.index(v);
  }

  // Define a periodic basis by using the constructed associations
  using namespace Dune::Functions::BasisFactory;
  Experimental::PeriodicIndexSet periodicIndexSet;
  periodicIndexSet.unifyIndexPair(association.first, association.second);

  ProblemStat prob("prob", grid, Experimental::periodic(lagrange<1>(), periodicIndexSet));
  prob.initialize(INIT_ALL);

  prob.addMatrixOperator(sot(1.0)); // <u', v'>
  prob.addMatrixOperator(zot(1.0)); // <u, v>
  prob.addVectorOperator(zot([](auto const& x) { return x[0] + std::sin(2*x[0]*M_PI); }, 4)); // <f, v>

  AdaptInfo adaptInfo("adapt");
  prob.assemble(adaptInfo);
  prob.solve(adaptInfo);
  prob.writeFiles(adaptInfo);
}