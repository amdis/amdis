#include <amdis/AMDiS.hpp>
#include <amdis/Integrate.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/ProblemStat.hpp>

#include <dune/grid/albertagrid/albertareader.hh>
#include <dune/foamgrid/foamgrid.hh>

#include "FactoryParametrization.hpp"
#include "SphereMapping.hpp"

using namespace AMDiS;

using Grid = Dune::FoamGrid<GRIDDIM, WORLDDIM>;
using Basis = LagrangeBasis<Grid, 1>;

struct UnitRadius
{
  template <class T>
  static double eval(T&&) { return 1.0; }
};

#if !HAVE_ALBERTA
#error "Need Alberta!!"
#endif

// solve the equation -laplace(u) - k^2 u = f on the sphere
int main(int argc, char** argv)
{
  Environment env(argc, argv);

  std::string gridName = Parameters::get<std::string>("surface->mesh").value();
  std::string gridFileName = Parameters::get<std::string>(gridName + "->macro file name").value();

  using Param = Parametrization<Grid, SphereMapping<GRIDDIM, WORLDDIM, UnitRadius>>;
  Dune::GridFactory<Param> gridFactory;
  Dune::AlbertaReader<Param>().readGrid(gridFileName, gridFactory);
  std::unique_ptr<Grid> grid(gridFactory.create());

  ProblemStat<Basis> prob("surface", *grid);
  prob.initialize(INIT_ALL);

  auto opL = makeOperator(tag::gradtest_gradtrial{}, 1.0);
  prob.addMatrixOperator(opL);

  double kappa = Parameters::get<double>("surface->kappa").value_or(1.0);
  auto opM = makeOperator(tag::test_trial{}, -kappa*kappa);
  prob.addMatrixOperator(opM);

  auto opForce = makeOperator(tag::test{}, X(0) + X(1) + X(2));
  prob.addVectorOperator(opForce);

  AdaptInfo adaptInfo("adapt");
  prob.assemble(adaptInfo);
  prob.solve(adaptInfo);

  prob.writeFiles(adaptInfo);

  return 0;
}
