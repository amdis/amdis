#pragma once

#include <type_traits>
#include <vector>

#include <dune/grid/common/gridfactory.hh>
#include <amdis/Output.hpp>

namespace AMDiS
{
  template <class GridType, class Projection>
  struct Parametrization
  {
    using Grid = GridType;
    using ctype = typename Grid::ctype;

    static constexpr int dimension = Grid::dimension;
    static constexpr int dimensionworld = Grid::dimensionworld;

    using LeafIntersection = typename Grid::LeafIntersection;

    template <int cd>
    using Codim = typename Grid::template Codim<cd>;
  };

} // end namespace AMDiS


namespace Dune
{
  /// \brief A factory class for \ref SimpleGrid.
  template <class Grid, class Projection>
  class GridFactory<AMDiS::Parametrization<Grid, Projection>>
      : public GridFactoryInterface<AMDiS::Parametrization<Grid, Projection>>
  {
    using GridWrapper = AMDiS::Parametrization<Grid, Projection>;

    /// Type used by the grid for coordinates
    using ctype = typename Grid::ctype;

    static constexpr int dim = Grid::dimension;
    static constexpr int dow = Grid::dimensionworld;

    using LocalCoordinate = FieldVector<ctype, dim>;
    using GlobalCoordinate = FieldVector<ctype, dow>;

  public:

    /// Default constructor
    GridFactory() = default;

    /// Insert a vertex into the coarse grid
    virtual void insertVertex(GlobalCoordinate const& pos) override
    {
      GlobalCoordinate new_pos(pos);
      Projection::project(new_pos);

      coordinates.push_back(new_pos);
      factory.insertVertex(new_pos);
    }

    /// Insert an element into the coarse grid.
    virtual void insertElement(GeometryType const& type,
                               std::vector<unsigned int> const& vertices) override
    {
      assert( type.isSimplex() );

      std::vector<GlobalCoordinate> corners(vertices.size());
      for (std::size_t i = 0; i < vertices.size(); ++i)
        corners[i] = coordinates[vertices[i]];

      factory.insertElement(type, vertices, Projection{std::move(corners)} );
    }

    virtual void insertBoundarySegment(std::vector<unsigned int> const& /*vertices*/) override
    {
      AMDiS::warning("Not implemented!");
    }

    /// Finalize grid creation and hand over the grid.
    std::unique_ptr<Grid> create()
    {
      return std::unique_ptr<Grid>(factory.createGrid());
    }

    std::unique_ptr<GridWrapper> createGrid() override
    {
      AMDiS::warning("Should not be created. Use non-virtual method `create()` instead, to create the underlying grid!");
      return nullptr;
    }

  private:
    // buffers for the mesh data
    std::vector<GlobalCoordinate> coordinates;
    GridFactory<Grid> factory;
  };

} // end namespace Dune
