#include <amdis/AMDiS.hpp>
#include <amdis/AdaptInstationary.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/ProblemInstat.hpp>
#include <amdis/linearalgebra/mtl/precons/SIMPLEPreconditioner.hpp>
#include <amdis/linearalgebra/mtl/precons/SIMPLERPreconditioner.hpp>
#include <amdis/localoperators/StokesOperator.hpp>

using namespace AMDiS;

struct NavierStokesBasis
    : public TaylorHoodBasis<Dune::YaspGrid<GRIDDIM>>
{
  using CoefficientType = double;
};

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  ProblemStat<NavierStokesBasis> prob("stokes");
  prob.initialize(INIT_ALL-INIT_SOLVER);

  registerSIMPLEPreconditioner(prob.globalBasis());
  registerSIMPLERPreconditioner(prob.globalBasis());
  prob.initialize(INIT_SOLVER);

  ProblemInstat<NavierStokesBasis> probInstat("stokes", prob);
  probInstat.initialize(INIT_UH_OLD);

  double viscosity = 1.0;
  double density = 1.0;
  double vel = 1.0;
  Parameters::get("stokes->viscosity", viscosity);
  Parameters::get("stokes->density", density);
  Parameters::get("stokes->boundary velocity", vel);

  AdaptInfo adaptInfo("adapt");

  // tree-paths for components
  auto _v = Dune::Indices::_0;
  auto _p = Dune::Indices::_1;

  auto invTau = std::ref(probInstat.invTau());

  // <1/tau * u, v>
  auto opTime = makeOperator(tag::testvec_trialvec{},
    density * invTau);
  prob.addMatrixOperator(opTime, _v, _v);

  // <1/tau * u^old, v>
  auto opTimeOld = makeOperator(tag::testvec{},
    density * invTau * prob.solution(_v));
  prob.addVectorOperator(opTimeOld, _v);


  // sum_i <grad(u_i),grad(v_i)> + <p, div(v)> + <div(u), q>
  auto opStokes = makeOperator(tag::stokes{}, viscosity);
  prob.addMatrixOperator(opStokes, makeTreePath(), makeTreePath());

  // <(u * nabla)u_i^old, v_i>
  auto opNonlin1 = makeOperator(tag::testvec_trialvec{},
    density * trans(gradientOf(prob.solution(_v))));
  prob.addMatrixOperator(opNonlin1, _v, _v);

  for (std::size_t i = 0; i < WORLDDIM; ++i) {
    // <(u^old * nabla)u_i, v_i>
    auto opNonlin2 = makeOperator(tag::test_gradtrial{},
      density * prob.solution(_v));
    prob.addMatrixOperator(opNonlin2, makeTreePath(_v,i), makeTreePath(_v,i));
  }

  // <(u^old * grad(u_i^old)), v_i>
  auto opNonlin3 = makeOperator(tag::testvec{},
    trans(gradientOf(prob.solution(_v))) * prob.solution(_v));
  prob.addVectorOperator(opNonlin3, _v);

  // define boundary values
  auto parabolic_y = [](auto const& x)
  {
    return FieldVector<double,2>{0.0, x[1]*(1.0 - x[1])};
  };

  FieldVector<double,2> zero(0);

  // set boundary conditions for velocity
  prob.boundaryManager()->setBoxBoundary({1,2,2,2});
  prob.addDirichletBC(1, _v, _v, parabolic_y);
  prob.addDirichletBC(2, _v, _v, zero);

  // set initial conditions
  prob.solution(_v).interpolate(parabolic_y);

  AdaptInstationary adapt("adapt", prob, adaptInfo, probInstat, adaptInfo);
  adapt.adapt();

  // output solution
  prob.writeFiles(adaptInfo);

  return 0;
}
