#pragma once

#include <algorithm>
#include <array>

#include <dune/common/fvector.hh>

namespace AMDiS
{
  /**
  * \brief Mapping class mapping from a triangle with points on the unit sphere onto the sphere
    *       with theta in [0, pi] and phi in [0, 2*pi)
  */
  template <int dim, int dow, class Radius, class ct = double>
  class SphereMapping
  {
    using LocalCoordinate = Dune::FieldVector<ct, dim>;
    using GlobalCoordinate = Dune::FieldVector<ct, dow>;

  public:
    template <class VertexContainer>
    SphereMapping(VertexContainer&& vertices)
    {
      std::copy_n(vertices.begin(), dim+1, vertices_.begin());
    }

    void evaluate(LocalCoordinate const& x, GlobalCoordinate& y) const
    {
      // calculate global coordinate
      auto shapeFunctions = evaluateShapeFunctions(x);
      y = 0.0;
      for(size_t i = 0; i < dim+1; i++)
        for(size_t j = 0; j < dow; j++)
          y[j] += vertices_[i][j]*shapeFunctions[i];
      project(y);
    }

    GlobalCoordinate operator() (LocalCoordinate const& x) const
    {
      GlobalCoordinate y;
      evaluate(x,y);
      return y;
    }

    GlobalCoordinate evaluateShapeFunctions(LocalCoordinate const& x) const
    {
      GlobalCoordinate out;
      out[0] = 1.0;
      for (size_t i=0; i<2; i++)
      {
        out[0]  -= x[i];
        out[i+1] = x[i];
      }
      return out;
    }

    static void project(GlobalCoordinate& y)
    {
      // project it on the unit sphere
      y /= y.two_norm() / Radius::eval(y);
    }


  private:
    std::array<GlobalCoordinate, dim+1> vertices_;
  };

} // end namespace AMDiS
