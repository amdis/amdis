#include <amdis/AMDiS.hpp>
#include <amdis/AdaptInstationary.hpp>
#include <amdis/ProblemInstat.hpp>
#include <amdis/GridFunctions.hpp>
#include <amdis/Marker.hpp>

#include <dune/grid/albertagrid.hh>
using Grid = Dune::AlbertaGrid<2, 2>;

using namespace AMDiS;
using namespace Dune::Functions::BasisFactory;


/// NOTE: Only tested with Backend MTL. Periodic BCs are not implemented for all backends in the moment.

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  // To use periodic boundary conditions the grid should be periodic as well.
  // Therefore, we have to give the AlbertaGrid the same face transformation
  // as we use for the periodic boundary conditions. To add this to the grid,
  // we parametrize the grid reader with a grid factory and add to this grid
  // factory the face transformation in form of an affine mapping x -> A*x + b.

  // Construct grid factory
  Dune::GridFactory<Grid> gfp;
  Dune::FieldMatrix<double,2,2> A_per{{1.0,0.0}, {0.0,1.0}};
  Dune::FieldVector<double,2> b_per{2.0, 0.0};
  gfp.insertFaceTransformation(A_per, b_per);

  // Construct grid reader
  Dune::AlbertaReader<Grid> rg;

  // If you change the mesh for testing, you probably also need to change the transformations!
  std::string mesh_name = Parameters::get<std::string>("chMesh->macro file name").value_or("./macro/macro.square.2d");

  // Parametrize grid reader with name of the macro file and grid factory
  rg.readGrid(mesh_name, gfp);

  // Create grid
  auto gptr = std::unique_ptr<Grid>{gfp.createGrid()};
  auto& grid = *gptr;

  ProblemStat prob("ch", grid, power<2>(lagrange<1>()));
  prob.initialize(INIT_ALL);

  ProblemInstat probInstat("ch", prob);
  probInstat.initialize(INIT_UH_OLD);

  AdaptInfo adaptInfo("adapt");

  auto invTau = std::ref(probInstat.invTau());
  auto phi = prob.solution(0);
  auto phiOld = probInstat.oldSolution(0);

  prob.addMatrixOperator(zot(invTau), 0, 0);
  prob.addVectorOperator(zot(phiOld * invTau), 0);

  double M = Parameters::get<double>("parameters->mobility").value_or(1.0);
  prob.addMatrixOperator(sot(M), 0, 1);
  prob.addMatrixOperator(zot(1.0), 1, 1);

  // Add advection, so that the circles moves and we can see the periodic boundary
  Dune::FieldVector<double,2> v{1.0,0.0};
  auto adv = makeOperator(tag::test_gradtrial(), 5.0*v, 5);
  prob.addMatrixOperator(adv, 0, 0);


  double a = Parameters::get<double>("parameters->a").value_or(1.0);
  double b = Parameters::get<double>("parameters->b").value_or(1.0/4.0);

  double eps = Parameters::get<double>("parameters->epsilon").value_or(0.02);
  prob.addMatrixOperator(sot(-a*eps), 1, 0);

  auto opFimpl = zot(-b/eps * (2 + 12*phi*(phi - 1)));
  prob.addMatrixOperator(opFimpl, 1, 0);

  auto opFexpl = zot(b/eps * pow<2>(phi)*(6 - 8*phi));
  prob.addVectorOperator(opFexpl, 1);


  // We have a periodic boundary on the right/left boundary with ID -1.
  prob.boundaryManager()->setBoxBoundary({-1,-1,2,2});

  // Add Boundary identifier and Face transformation that maps the right to the left boundary
  prob.addPeriodicBC(-1, A_per, b_per);

  // Define an indicator for local adaptive refinement
  int ref_int  = Parameters::get<int>("refinement->interface").value_or(10);
  int ref_bulk = Parameters::get<int>("refinement->bulk").value_or(2);
  auto indicator = [ref_int, ref_bulk](double phi) {
    return phi > 0.05 && phi < 0.95 ? ref_int : ref_bulk;
  };

  // Construct a grid marker for the automatic refinement
  GridFunctionMarker marker("interface", prob.grid(), invokeAtQP(indicator,phi));
  prob.addMarker(marker);

  // Define an initial condition for the phase field
  double radius = Parameters::get<double>("parameters->radius").value_or(0.2);
  auto dist = [radius](auto const& x) { return x.two_norm() - radius; };
  auto phase = [eps,dist](auto const& x) {
    return 0.5*(1 - std::tanh(dist(x)/(2*std::sqrt(2.0)*eps)));
  };

  // Perform an initial refinement
  for (int i = 0; i < (ref_int - ref_bulk)+2; ++i) {
    phi << phase;
    prob.markElements(adaptInfo);
    prob.adaptGrid(adaptInfo);
  }

  AdaptInstationary adapt("adapt", prob, adaptInfo, probInstat, adaptInfo);
  adapt.adapt();

  return 0;
}
