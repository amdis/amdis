/// [includes]_begin
#include <config.h>

#include <iostream>

#include <amdis/AMDiS.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/ProblemStat.hpp>
/// [includes]_end

/// [namespace amdis]_begin
using namespace AMDiS;
/// [namespace amdis]_end

/// [main begin]_begin
int main(int argc, char** argv)
{
  Environment env(argc, argv);
/// [main begin]_end

  /// [grid construction]_begin
  using Grid = Dune::YaspGrid<GRIDDIM>;
  auto grid = MeshCreator<Grid>{"elliptMesh"}.create();
  /// [grid construction]_end

  /// [problem definition]_begin
  using namespace Dune::Functions::BasisFactory;
  ProblemStat prob("ellipt", *grid, power<2>(lagrange<2>()));
  prob.initialize(INIT_ALL);

  AdaptInfo adaptInfo("adapt");
  /// [problem definition]_end

  /// [operators]_begin
  // ⟨∇u₁,∇v₁⟩
  auto opL = makeOperator(tag::gradtest_gradtrial{}, 1.0);
  prob.addMatrixOperator(opL, 1, 1);

  // ⟨u₀,v₀⟩ and ⟨u₁,v₀⟩
  auto opM = makeOperator(tag::test_trial{}, 1.0);
  prob.addMatrixOperator(opM, 0, 0);
  auto opM2 = makeOperator(tag::test_trial{}, -1.0);
  prob.addMatrixOperator(opM2, 0, 1);

  // (f,v₁) with f ≡ -1
  auto opForce = makeOperator(tag::test{}, [](auto const& x) { return -1.0; }, 0);
  prob.addVectorOperator(opForce, 1);
  /// [operators]_end


  /// [constraints]_begin
  // set boundary condition
  auto predicate = [](auto const& x){ return x[0] < 1.e-8 || x[1] < 1.e-8; }; // define boundary
  auto dbcValues = [](auto const& x){ return 0.0; }; // set value
  prob.addDirichletBC(predicate, 1, 1, dbcValues);
  /// [constraints]_end

  /// [adapt]_begin
  prob.buildAfterAdapt(adaptInfo, Flag(0));
  prob.solve(adaptInfo);
  prob.writeFiles(adaptInfo);
  /// [adapt]_end

/// [main end]_begin
  return 0;
}
/// [main end]_end
