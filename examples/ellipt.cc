/// [includes]_begin
#include <amdis/AMDiS.hpp>
#include <amdis/Integrate.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/ProblemStat.hpp>
/// [includes]_end

/// [gridinclude]_begin
#include <dune/grid/yaspgrid.hh>
using Grid = Dune::YaspGrid<GRIDDIM>;
/// [gridinclude]_end

/// [namespace amdis]_begin
using namespace AMDiS;
/// [namespace amdis]_end

/// [main begin]_begin
int main(int argc, char** argv)
{
/// [main begin]_end
  /// [parameter definition]_begin
  Environment env(argc, argv);
  Dune::Timer t;

  int numLevels = GRIDDIM == 2 ? 6 : 4;
  if (argc > 2)
    numLevels = std::atoi(argv[2]);

  auto f = [](auto const& x) {
    double r2 = dot(x,x);
    double ux = std::exp(-10.0 * r2);
    return -(400.0 * r2 - 20.0 * x.size()) * ux;
  };
  auto g = [](auto const& x){ return std::exp(-10.0 * dot(x,x)); };
  auto grad_g = [](auto const& x) -> FieldMatrix<double,1,GRIDDIM> {
    return {-20.0 * std::exp(-10.0 * dot(x,x)) * x};
  };
  /// [parameter definition]_end

  /// [problem definition]_begin
  using Param = LagrangeBasis<Grid, 2>;
  ProblemStat<Param> prob("ellipt");
  prob.initialize(INIT_ALL);
  /// [problem definition]_end

  /// [operators]_begin
  // ⟨∇u,∇v⟩
  auto opL = makeOperator(tag::gradtest_gradtrial{}, 1.0);
  prob.addMatrixOperator(opL);

  // (f,v)
  auto opForce = makeOperator(tag::test{}, f, 6);
  prob.addVectorOperator(opForce);
  /// [operators]_end

  /// [constraints]_begin
  // set boundary condition
  prob.addDirichletBC(1, g);
  /// [constraints]_end

  /// [adapt parameter definition]_begin
  AdaptInfo adaptInfo("adapt");

  std::vector<double> errL2; errL2.reserve(numLevels);
  std::vector<double> errH1; errH1.reserve(numLevels);
  std::vector<double> widths; widths.reserve(numLevels);
  /// [adapt parameter definition]_end

  /// [main loop begin]_begin
  for (int l = 0; l < numLevels; ++l) {
    prob.globalRefine(1);
  /// [main loop begin]_end

  /// [main loop element widths calc]_begin
    double h = 0;
    for (auto const& e : elements(prob.gridView(), Dune::Partitions::interior)) {
      auto refElem = Dune::referenceElement<double,GRIDDIM>(e.type());
      auto geo = e.geometry();
      for (int i = 0; i < refElem.size(GRIDDIM-1); ++i) { // edges
        auto v0 = geo.global(refElem.position(refElem.subEntity(i,GRIDDIM-1,0,GRIDDIM), GRIDDIM));
        auto v1 = geo.global(refElem.position(refElem.subEntity(i,GRIDDIM-1,1,GRIDDIM), GRIDDIM));
        h = std::max(h, (v0 - v1).two_norm());
      }
    }
    widths.push_back(h);
  /// [main loop element widths calc]_end

  /// [main loop assemble solve]_begin
    prob.assemble(adaptInfo);
    prob.solve(adaptInfo);
  /// [main loop assemble solve]_end

  /// [main loop end]_begin
    double errorL2 = integrate(sqr(g - prob.solution()), prob.gridView(), 6);
    errL2.push_back(std::sqrt(errorL2));
    double errorH1 = errorL2 + integrate(unary_dot(grad_g - gradientOf(prob.solution())), prob.gridView(), 6);
    errH1.push_back(std::sqrt(errorH1));
  }
  /// [main loop end]_end

  /// [output]_begin
  // write last solution to file
  prob.writeFiles(adaptInfo);

  msg("");
  msg("{:5} | {:12} | {:12} | {:12} | {:12} | {:12}",
      "level", "h", "|u - u*|_L2","|u - u*|_H1","eoc_L2","eoc_H1");
  msg_("{0:->7}{0:->15}{0:->15}{0:->15}{0:->15}{1:->14}","+","\n");

  std::vector<double> eocL2(numLevels, 0.0), eocH1(numLevels, 0.0);
  for (int i = 1; i < numLevels; ++i) {
    eocL2[i] = std::log(errL2[i]/errL2[i-1]) / std::log(widths[i]/widths[i-1]);
    eocH1[i] = std::log(errH1[i]/errH1[i-1]) / std::log(widths[i]/widths[i-1]);
  }

  for (int i = 0; i < numLevels; ++i)
    msg("{:<5} | {:<12} | {:<12} | {:<12} | {:<12} | {:<12}",
        i+1, widths[i], errL2[i], errH1[i], eocL2[i], eocH1[i]);

  msg("elapsed time: {} seconds", t.elapsed());
  /// [output]_end

/// [main end]_begin
  return 0;
}
/// [main end]_end
