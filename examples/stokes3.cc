#include <amdis/AMDiS.hpp>
#include <amdis/LocalOperators.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/localoperators/StokesOperator.hpp>

using namespace AMDiS;

using Grid = Dune::YaspGrid<2>;
using StokesParam = TaylorHoodBasis<Grid>;
using StokesProblem = ProblemStat<StokesParam>;

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  StokesProblem prob("stokes");
  prob.initialize(INIT_ALL);

  double viscosity = 1.0;
  Parameters::get("stokes->viscosity", viscosity);

  // tree-paths for components
  auto _v = Dune::Indices::_0;
  auto _p = Dune::Indices::_1;

  auto opStokes = makeOperator(tag::stokes{}, viscosity);
  prob.addMatrixOperator(opStokes);

  // define boundary values
  auto parabolic_y = [](auto const& x)
  {
    return FieldVector<double,2>{0.0, x[1]*(1.0 - x[1])};
  };

  FieldVector<double,2> zero(0);

  // set boundary conditions for velocity
  prob.boundaryManager()->setBoxBoundary({1,2,2,2});
  prob.addDirichletBC(1, _v, _v, parabolic_y);
  prob.addDirichletBC(2, _v, _v, zero);

  AdaptInfo adaptInfo("adapt");

  // assemble and solve system
  prob.assemble(adaptInfo);
  prob.solve(adaptInfo);

  // output solution
  prob.writeFiles(adaptInfo);

  return 0;
}
