#include <config.h>

#include <iostream>

#include <amdis/AMDiS.hpp>
#include <amdis/ProblemStat.hpp>
#include <amdis/localoperators/ConvectionDiffusionOperator.hpp>

using namespace AMDiS;

int main(int argc, char** argv)
{
  Environment env(argc, argv);

  // create a grid by inspecting the initfile parameters
  Dune::YaspGrid<2> grid({1.0, 1.0}, {4u, 4u});

  using namespace Dune::Functions::BasisFactory;
  ProblemStat prob("ellipt", grid, lagrange<1>());
  prob.initialize(INIT_ALL);

  // -div(A*grad(u)) + div(b*u) + c*u = f
  auto opCD = convectionDiffusion(/*A=*/1.0, /*b=*/0.0, /*c=*/1.0, /*f=*/1.0);
  prob.addMatrixOperator(opCD);
  prob.addVectorOperator(opCD);

  // set boundary condition
  auto predicate = [](auto const& x){ return x[0] < 1.e-8 || x[1] < 1.e-8; }; // define boundary
  auto dbcValues = [](auto const& x){ return 0.0; }; // set value
  prob.addDirichletBC(predicate, dbcValues);

  AdaptInfo adaptInfo("adapt");
  prob.assemble(adaptInfo);
  prob.solve(adaptInfo);
  prob.writeFiles(adaptInfo);

  return 0;
}
